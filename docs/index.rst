.. Fleeting Forms documentation master file, created by
   sphinx-quickstart on Sat Apr 25 18:16:42 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Fleeting Forms's documentation!
==========================================

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Contents:

   src/hosting/client_onboarding
   api/index.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
