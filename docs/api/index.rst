Fleeting Forms API Documentation
--------------------------------

Welcome to the Fleeting Forms API documentation.  You're either here 
because you're curious, need to maintain something, or want to
contribute.  Welcome.

The Fleeting Forms project is build on `Django`_ using the
`Django ReST Framework`_.  Familiarize yourself with them before 
diving in.

.. _django: https://djangoproject.com
.. _django rest framework: https://www.django-rest-framework.org/

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Contents:

   fleetingform/models
   fleetingform/views
   fleetingform/forms
   fleetingform/serializers
   fleetingform/errors
   fleetingform/lib
   fleetingform/authentication
   fleetingform/permissions
   fleetingform/middleware



