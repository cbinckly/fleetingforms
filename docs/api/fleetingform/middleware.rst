Middleware
-----------------------

Middleware manipulates the request before it is passed to the view
and the response before it is returned to the client.  

To learn more about Django's middleware implementation and how the
class has been implemented, have a look at the `middleware docs`.

.. _middleware docs: https://docs.djangoproject.com/en/3.0/topics/http/middleware/

.. automodule:: fleetingform.middleware
    :members:
    :show-inheritance:

