Views
-----------------------

The views respond to browser requests and control the response that
is rendered.

.. automodule:: fleetingform.views
    :members:
    :undoc-members:
    :show-inheritance:

