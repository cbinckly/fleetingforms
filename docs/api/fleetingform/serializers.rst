Serializers
---------------------------

Serializers transform models to and from JSON representations for the
ReST API.

This is where you'll find inbound data validation.

.. automodule:: fleetingform.serializers
    :members:
    :undoc-members:
    :show-inheritance:

