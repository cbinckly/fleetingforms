Errors
-----------------------

Custom error classes used in the application.

.. automodule:: fleetingform.errors
    :members:
