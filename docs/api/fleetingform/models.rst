Models
-----------------------

Models define the core data structures that support the application
and are represented in the database.

With Fleeting Forms, the Form is at the heart of the application.
They are grouped together in a :class:`~fleetingform.models.FleetingNamespace`
and contain the :class:`~fleetingform.models.FleetingTemplate` and 
:class:`~fleetingform.models.FleetingAuth` that define the form.

.. automodule:: fleetingform.models
    :members:
    :undoc-members:
    :show-inheritance:

