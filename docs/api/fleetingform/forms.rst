Forms
-----------------------

There are two types of forms in this application: relatively static login forms
and dynamic FleetingForms.  This stuiff is all for the former.

.. automodule:: fleetingform.forms
    :members:
    :show-inheritance:

