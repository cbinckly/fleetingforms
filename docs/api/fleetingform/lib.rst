Libraries and Utilities
-------------------------------------

A collection of things that are used throughout the application.
This includes helpful functions for sending OTPs, the form
generator, and other sundry.

.. automodule:: fleetingform.lib
    :members:
    :show-inheritance:

.. automodule:: fleetingform.lib.form_generator
    :members:
    :show-inheritance:
