ReST Framwork Permissions
-----------------------------------------

Permissions are used to check whether a particular user can see
an object or write to a namespace.  Fleeting Form's API permissions
are implemented using the ReST Framework's `permissions`_, so have a look
there for more background.

.. _permissions: https://www.django-rest-framework.org/api-guide/permissions/

.. automodule:: fleetingform.permissions
    :members:
    :undoc-members:
    :show-inheritance:

