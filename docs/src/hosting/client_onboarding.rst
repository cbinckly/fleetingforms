Client Onboarding
=================

aka Creating a New Namespace
----------------------------

The fleetingforms service can be bundled and sold as part of a managed service.
In this case, the service provider has a user account but the clients do not.

There are two approaches a service provider can take:

#. Maintain one namespace that all clients use.
#. Maintain a per-client namespace.

The first approach minimizes the administration overhead as only one namespace
needs to be maintained.  In the event that the namespace token (shared between
all clients) is compromised, the overhead of reassigning the keys for all 
deployments could be very high.

The second approach requires the creation of a new namespace for each customer
but enables per client usage tracking as well as a per-client token.  Although
an extra step is required to onboard a new client, it is very straightforward
and easily automated.

Once a client has been provisioned, they need never be changed, save for a 
token reset if required.

Creating a New Namespace
------------------------

There are two ways to create a namespace: through the administration panel;
using the API. The administration panel allows humans to administer namespaces
while the API provides a way to automate namespace provisioning and billing.

Using the Admin Panel
~~~~~~~~~~~~~~~~~~~~~

Visit https://fleetingforms.io/admin/ and login with your username and 
password.

.. image:: /images/create_namespace/01_login.png
   :width: 600
   :alt: Fleeting forms administration login screen.

On the Admin Landing Page, select the :guilabel:`Fleeting Namespace` section.

.. image:: /images/create_namespace/02_model_list.png
   :width: 600
   :alt: Fleeting forms administration model list.

Select the :guilabel:`+ Add Fleeting Namespace` button in the top right corner.

.. image:: /images/create_namespace/03_namespace_list.png
   :width: 600
   :alt: Fleeting forms administration namespace list.

Complete the form, setting the client's preferences, and save it.

.. image:: /images/create_namespace/04_enter_details.png
   :width: 600
   :alt: Fleeting namespace detail data entry.

The namespace as now been assigned a token that can be shared it the user.

Using the API
~~~~~~~~~~~~~

Any user of the service can access the namespaces API to create or manage
their namespaces.  This allows managed service providers to automate the
onboarding of new clients and the monitoring of per-client usage.

To create a new namespace over the API, start by posting a new namespace
definition to https://fleetingforms.io/namespaces/ path::

    {
        "subdomain": "company-name",
        "retention": 180,
        "logo": "https://company-name.com/images/logo.png",
        "support_email": "accounting-support@company.com",
        "soft_limit": 1000,
        "hard_limit": 1500
    }

This will return the details of the new namespace, including the client token::

    {
        "id": 1567,
        "user": "myuser",
        "token": "6544d16d-5ef6-4924-9eee-54e3752b0640",
        "subdomain": "company-name",
        "retention": 180,
        "logo": "https://company-name.com/images/logo.png",
        "style": "",
        "support_email": "accounting-support@company.com",
        "soft_limit": 1000,
        "hard_limit": 1500
        "usage": {
            "this month": 0,
            "last month": 0
        }
    }

The namespace has now been created along with associated counters and the token
can be provided to the user.


