#!/usr/bin/env python
"""Deploy an instance of the fleeting forms webservice in AWS.

This deployment script is intended to illustrate the process of deploying
the fleeting forms webservice as an ECS cluster in its own VPC.

This script can be used to roll out new environments.  The default
configuration is suitable for dev/test only. For a production deployment,
consider:

- Avoid a serverless database configuration
- Expand task sizes to accomodate higher load
- Add a NAT Gateway to all public subnets, not just the first

That having been said, the deployment created here is isolated and secure.  The
webservices are well protected and the only point of ingress is HTTPS traffic
through the Load Balancer.  Data is encrypted at rest and in transit.

Preconditions
-------------

Before running the script, two things need to have been done:

    - Register a domain to host the service (any domain will do)
    - Get a certificate from Amazon ACM with:

      - Common Name: <your domain>
      - Common Name: *.<your domain>


Network Topology
----------------

In addition, you must have identified your network topology.  Note that
the following constraints are imposed by the availbility requirements
for the Application Load Balancer and RDS DB:

#. The ALB requires two public subnets in different availability zones.
#. The RDS DB requires three private subnets in different availbility zones.

To make the design consistent across all Availbility Zones, it is recommended
that you configure both a public and private subnet in at least three
availbility zones within your region.

What does the script do?
------------------------

The script takes all the actions necessary to get a fully functioning VPC
with an ECS cluster running the fleeting forms service up from scratch.

It looks after the following tasks:

- Creating the new VPC
- Configuring the internal and external security groups
- Setting up routing within the VPC so webservices are isolated
- Configuring log destinations and CloudWatch monitoring
- Setting up a serverless database in RDS
- Defining an ECS cluster along with the tasks and services required
- Making ready the load balancer, with HTTPS and HTTP listeners

Example Invocations
-------------------

Deploy a fleetingforms webservice at myforms.com::

    deploy.py --domain-name myforms.com

Deploy an instance with email and service customizations::

    deploy.py --domain-name fleetingforms.dev --rollback prompt \
              --code-length 6 --max-retention 180 \
              --max-otp-attempts 1 --min-password-chars 2 \
              --email-host smtp.sendgrid.com --email-user apikey \
              --email-pass "password" --email-from "rosie@fleetingforms.dev"

"""
import os
import sys
import time
import boto3
import random
import string
import logging
import argparse

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%Y%m%dT%H%M%S',)
logger = logging.getLogger(__name__)

ec2 = boto3.resource('ec2')
iam = boto3.resource('iam')
ec2_client = boto3.client('ec2')
elbv2_client = boto3.client('elbv2')
ecs_client = boto3.client('ecs')
sd_client = boto3.client('servicediscovery')
acm_client = boto3.client('acm')
rds_client = boto3.client('rds')
log_client = boto3.client('logs')

ROLLBACKS = ('error', 'prompt', 'always', 'never', )

def parse_args():
    parser = argparse.ArgumentParser(
            description='Deploy fleetingforms in AWS.')

    required = parser.add_argument_group('required arguments')
    required.add_argument('--domain-name', dest='domain_name', action='store',
                          required=True,
                          help='The domain name of the service.')
    required.add_argument('--region', dest='region',
                          action='store', default='us-east-1',
                          help='The AWS region to deploy in.')
    required.add_argument('--rollback', action="store", choices=ROLLBACKS,
                          default='error',
                          help="Rollback after deploying? Choices are: "
                               "error (default), prompt, always, never.")
    required.add_argument('--debug', dest='debug', action='store_true',
                          help='Enable service debugging. Only for testing!')

    network = parser.add_argument_group(
            'network arguments', 'If any one network setting is user defined, '
            'they must all be.')
    network.add_argument('--vpc-cidr', dest='vpc_network', action='store',
                         default='172.16.0.0/20',
                         help='CIDR block for VPC.')
    network.add_argument('--public-subnet', nargs=2, action="append",
                         dest='public_subnets',
                         metavar=('subnet-cidr', 'availability-zone'),
                         help="Public subnets. At least three in different "
                              "availability zones are required.")
    network.add_argument('--private-subnet', nargs=2, action="append",
                         dest='private_subnets',
                         metavar=('subnet-cidr', 'availability-zone'),
                         help="Private subnets. At least three in different "
                              "availability zones are required.")

    ecs = parser.add_argument_group(
            'ecs cluster arguments')
    ecs.add_argument('--task-execution-role', action="store",
                     default="ecsTaskExecutionRole",
                     help="ECS task execution role name.")
    ecs.add_argument('--container-insights', action="store",
                     default='disabled', choices=['enabled', 'disabled'],
                     help="Enable container insights for the ECS cluster?")
    ecs.add_argument('--backend-container', action="store",
                     default="cbinckly/fleetingforms",
                     help="Backend container.")
    ecs.add_argument('--frontend-container', action="store",
                     default="cbinckly/fleetingforms-nginx",
                     help="Frontend container.")
    ecs.add_argument('--backend-container-tag', action="store",
                     default="latest", help="Backend container tag.")
    ecs.add_argument('--frontend-container-tag', action="store",
                     default="latest", help="Frontend container tag.")
    ecs.add_argument('--backend-task-count', action="store", type=int,
                     default=1, help="Backend task count.")
    ecs.add_argument('--frontend-task-count', action="store", type=int,
                     default=1, help="Backend task count.")

    rds = parser.add_argument_group(
            'rds cluster arguments')
    rds.add_argument('--serverless', action="store_true",
                     help='Use a serverless database. Dev/Test only.')
    rds.add_argument('--deletion-protection', action="store_true",
                     help='Enabled deletion protection for the database.')
    rds.add_argument('--db-instance-class', action="store",
                     default='db.t3.medium',
                     help="Database instance class for provisioned clusters.")

    service = parser.add_argument_group(
            'service configuration', 'customize the fleeting forms service.')
    service.add_argument('--code-length', action='store', type=int, default=8,
                         help='The length of the unqiue code for each form.')
    service.add_argument('--url-scheme', action='store', default='https://',
                         help='The URL scheme to use. Only use HTTP in dev!')
    service.add_argument('--max-retention', action='store',
                         default=365, type=int,
                         help='Maximum form retention a user can configure.')
    service.add_argument('--max-otp-attempts', action='store',
                         type=int, default=3,
                         help='Max number of OTP retries per user per form.')
    service.add_argument('--min-password-chars', action='store',
                         type=int, default=3,
                         help='Min number of characters in plain passwords.')

    email = parser.add_argument_group('email')
    email.add_argument('--email-host', action="store", default="",
                       help="The email host to relay mail through.")
    email.add_argument('--email-port', action="store", default=587, type=int,
                       help="The mail relay port.")
    email.add_argument('--email-user', action="store", default="",
                       help="The user to connect to the relay as.")
    email.add_argument('--email-password', action="store", default="",
                       help="The password to authenticate with.")
    email.add_argument('--email-from', action="store", default="",
                       help="The email address to send mail from.")

    return parser.parse_args()

ARG_TO_ENV_MAP = {
            'debug': 'FLEETING_DEBUG',
            'code_length': 'FLEETING_CODE_LENGTH',
            'max_retention': 'FLEETING_MAX_RETENTION',
            'domain_name': 'FLEETING_DOMAIN',
            'url_scheme': 'FLEETING_URL_SCHEME',
            'max_otp_attempts': 'FLEETING_MAX_OTP_ATTEMPTS',
            'min_password_chars': 'FLEETING_PASSWORD_MIN_CHARS',
            'email_host': 'FLEETING_EMAIL_HOST',
            'email_port': 'FLEETING_EMAIL_PORT',
            'email_user': 'FLEETING_EMAIL_USER',
            'email_from': 'FLEETING_EMAIL_FROM',
            'email_pass': 'FLEETING_EMAIL_PASS',
        }

def env_from_args(args):
    """Generate a task environment variable config from script args."""
    envconfig = []
    for name, envname in ARG_TO_ENV_MAP.items():
        if hasattr(args, name):
            val = getattr(args, name)
            if val:
                entry = {'name': envname, 'value': str(val)}
                envconfig.append(entry)
                logger.info('added {} to envconfig.'.format(entry))
    return envconfig

def validate_network_args(args):
    vpc_network = "172.16.0.0/20"
    public_subnets = [
            ('172.16.0.0/24', '{}a'.format(args.region)),
            ('172.16.2.0/24', '{}b'.format(args.region)),
            ('172.16.4.0/24', '{}c'.format(args.region)),
        ]
    private_subnets = [
            ('172.16.1.0/24', '{}a'.format(args.region)),
            ('172.16.3.0/24', '{}b'.format(args.region)),
            ('172.16.5.0/24', '{}c'.format(args.region)),
        ]

    if args.vpc_network or args.public_subnets or args.private_subnets:
        if not args.vpc_network:
            raise ValueError("If any one part of the network is user defined, "
                             "it all must be. No VPC CIDR range provided.")
        if args.public_subnets or args.private_subnets:
            if not args.public_subnets and args.private_subnets:
                raise ValueError("If any one part of the network is user"
                                 "defined, it all must be. Provide three "
                                 "public and private subnets.")
            if not (len(args.public_subnets) > 2 and
                    len(args.private_subnets) > 2):
                raise ValueError("If any one part of the network is user"
                                 "defined, it all must be. Provide three "
                                 "public and private subnets.")
            vpc_network = args.vpc_network
            public_subnets = args.public_subnets
            private_subnets = args.private_subnets
    return (vpc_network, public_subnets, private_subnets)


def main():

    args = parse_args()
    try:
        vpc_network, public_subnets, private_subnets = \
                validate_network_args(args)
    except ValueError as e:
        print("Error validating options: {}.".format(e))
        sys.exit(1)

    logger.info("starting deployment of vpc network {}.".format(vpc_network))
    logger.info("public subnets: {}.".format(public_subnets))
    logger.info("private subnets: {}.".format(private_subnets))

    try:
        # Get after the deployment.
        deployment = FleetingECSDeployment(
                domain_name=args.domain_name,
                region=args.region,
                task_execution_role=args.task_execution_role)

        # Create VPC and setup default routing
        deployment.create_vpc(vpc_network)
        deployment.create_internal_security_group()
        deployment.create_external_security_group()
        deployment.enable_vpc_dns()
        deployment.add_internet_gateway()
        deployment.add_public_routing_table()
        deployment.add_default_route()

        # Add public and private subnets
        for cidr, az in public_subnets:
            deployment.create_public_subnet(cidr, az)
        for cidr, az in private_subnets:
            deployment.create_private_subnet(cidr, az)

        # Add a subnet group for the database that includes the private subnets
        deployment.create_database_subnet_group()
        # Create a serverless database
        deployment.create_rds_db(serverless=args.serverless,
                                 deletion_protection=args.deletion_protection,
                                 db_instance_class=args.db_instance_class)

        # Add a NAT gateway to allow isolated tasks to pull images
        # Make the NAT gateway the default route on all private nets.
        deployment.allocate_elastic_ip()
        deployment.create_nat_gateway(deployment.public_subnets[0])
        deployment.add_private_routing()

        # Create a new ECS cluster
        deployment.create_ecs_cluster()

        # Create a new Application Load Balancer and target group
        deployment.create_application_load_balancer()
        deployment.create_target_group()

        # Create the task definitions, customizing the service with user args
        backend_env = env_from_args(args)
        env = [*deployment.backend_base_environment, *backend_env]
        deployment.create_task_definition(
            deployment.backend_task_name,
            args.backend_container,
            args.backend_container_tag, 8000, env=env)
        deployment.create_task_definition(
            deployment.frontend_task_name,
            args.frontend_container,
            args.frontend_container_tag, 80)

        # Internal DNS namespace is used for the services to find one another.
        deployment.create_internal_dns_namespace()
        deployment.register_namespace_services()

        # Get the certificate to use for HTTPS
        deployment.get_certificate()

        # Create the listeners in the load balancer
        deployment.create_https_listener()
        deployment.create_http_listener()

        # Setup a logging destination for the services
        deployment.create_log_destinations()

        # Create the services from the tasks
        deployment.create_services()

        logger.info("deployment complete.")
        logger.info("access the service at {}".format(
                deployment.load_balancer['DNSName']))

        # Check if the user needs to rollback.
        if args.rollback == 'always':
            raise Exception("user requested rollback.")
        elif args.rollback == 'prompt':
            rollback = input("rollback? Y/n: ").strip().lower()
            if not rollback or rollback[0] != 'n':
                raise Exception("user requested rollback.")

    except Exception as e:
        logger.exception("failed: {}.".format(e))
        if args.rollback in ['error', 'always']:
            deployment.rollback()
        if args.rollback == 'prompt':
            rollback = input("rollback? Y/n: ").strip().lower()
            if not rollback or rollback[0] != 'n':
                deployment.rollback()

    except KeyboardInterrupt:
        logger.error("interrupted.")
        if args.rollback in ['error', 'always']:
            deployment.rollback()
        if args.rollback == 'prompt':
            rollback = input("rollback? Y/n: ").strip().lower()
            if not rollback or rollback[0] != 'n':
                deployment.rollback()

    return deployment

class FleetingECSDeployment():

    DEFAULT_ROUTE = "0.0.0.0/0"

    def __init__(self, domain_name, region='us-east-1',
                 container_insights='disabled',
                 task_execution_role="ecsTaskExecutionRole",):
        self.domain_name = domain_name
        self.name = self.domain_name.replace('.', '-')
        self.region = region
        self.task_execution_role = task_execution_role
        self.vpc = None
        self.internet_gateway = None
        self.public_subnets = []
        self.private_subnets = []
        self.public_routing_table = None
        self.public_routes = {}
        self.private_routing_tables = []
        self.private_routes = {}
        self.internal_security_group = None
        self.external_security_group = None
        self.container_insights_status = container_insights
        self.rollback_callbacks = []
        self.tasks = []
        self.database_cluster = None
        self.database_instance = None
        self.database_user = self.database_name
        self.database_password = self.generate_password()
        self.database_port = 5432

        self.secret_key = self.generate_password(length=64)
        logger.info("started for {} in region {}.".format(domain_name, region))

    def rollback(self):
        logger.warn("rolling back {} operations.".format(
                self.rollback_callbacks))
        while self.rollback_callbacks:
            try:
                delete_function, args = self.rollback_callbacks.pop()
                delete_function(*args)
                time.sleep(0.5)
            except Exception as e:
                logger.error("failed to rollback {}({}): {}.".format(
                        delete_function, args, e))
            except KeyboardInterrupt:
                logger.error("keyboard interrupt, stopped current task.")
        logger.info("rollback complete.")

    def generate_password(self, length=16):
         return ''.join(random.choice(string.ascii_letters)
                        for i in range(length))
    @property
    def task_execution_role_arn(self):
        return iam.Role(self.task_execution_role).arn

    @property
    def default_route(self):
        return self.public_routes.get(self.DEFAULT_ROUTE)

    @property
    def vpc_name(self):
        return "vpc-{}".format(self.name)

    @property
    def database_name(self):
        return self.name.replace("-", "_")

    @property
    def internal_security_group_name(self):
        return "int-{}".format(self.name)

    @property
    def external_security_group_name(self):
        return "ext-{}".format(self.name)

    @property
    def backend_container_name(self):
        return self.name.split('-')[0]

    @property
    def frontend_container_name(self):
        return '{}-nginx'.format(self.backend_container_name)

    @property
    def public_availability_zones(self):
        return {s.availability_zone for s in self.public_subnets}

    @property
    def private_availability_zones(self):
        return {s.availability_zone for s in self.private_subnets}

    def create_vpc(self, cidr_block):
        # Create a new VPC and tag it with the projec name
        logger.info("creating vpc {} wih block {}".format(
                self.vpc_name, cidr_block))
        self.vpc = ec2.create_vpc(CidrBlock=cidr_block)
        self.vpc.create_tags(Tags=[{"Key": "Name",
                                    "Value": self.vpc_name }])
        self.vpc.wait_until_available()
        self.rollback_callbacks.append((self.delete_vpc, [],))
        logger.info("vpc {} created.".format(self.vpc.id))
        return self.vpc

    def delete_vpc(self):
        logger.warn("deleting vpc {}.".format(self.vpc.id))
        self.vpc.delete()

    def enable_vpc_dns(self):
        # Enable DNS within the VPC - required to resolve docker hub
        logger.info("enabling dns in vpc.")
        ec2_client.modify_vpc_attribute(
                VpcId=self.vpc.id, EnableDnsSupport={'Value': True})
        ec2_client.modify_vpc_attribute(
                VpcId=self.vpc.id, EnableDnsHostnames={'Value': True})
        logger.info("dns enabled.")

    def allocate_elastic_ip(self):
        logger.info('allocating elatic ip.')
        self.elastic_ip = ec2_client.allocate_address(
            Domain='vpc',
        )

        logger.info('allocated {} [{}].'.format(
            self.elastic_ip['PublicIp'], self.elastic_ip['AllocationId']))

        self.rollback_callbacks.append((
            self.release_elastic_ip, [self.elastic_ip['AllocationId'], ]))

        return self.elastic_ip

    def release_elastic_ip(self, allocation_id):
        logger.warn("releasing elastic ip {}.".format(allocation_id))
        ec2_client.release_address(AllocationId=allocation_id)

    def create_nat_gateway(self, subnet):
        logger.info("creating nat gateway on {}.".format(subnet.id))
        self.nat_gateway = ec2_client.create_nat_gateway(
            AllocationId=self.elastic_ip['AllocationId'],
            SubnetId=subnet.id,
            TagSpecifications=[
                {
                    'ResourceType': 'natgateway',
                    'Tags': [
                        {
                            'Key': 'Name',
                            'Value': '{}-{}'.format(self.name, subnet.id),
                        },
                    ]
                },
            ]
        )['NatGateway']

        while self.nat_gateway['State'] not in ['available', 'failed', ]:
            time.sleep(15)
            self.nat_gateway = ec2_client.describe_nat_gateways(
                Filter=[
                    {
                        'Name': 'vpc-id',
                        'Values': [
                            self.vpc.id
                        ],
                    },
                ])['NatGateways'][0]
            logger.info("waiting on nat gateway provisioning, state {}.".format(
                    self.nat_gateway['State']))

        logger.info("created nat gateway {}".format(
                self.nat_gateway['NatGatewayId']))

        self.rollback_callbacks.append((
            self.delete_nat_gateway, [self.nat_gateway['NatGatewayId'], ]))

        if self.nat_gateway['State'] != 'available':
            raise Exception('nat gateway not available.')

        return self.nat_gateway

    def delete_nat_gateway(self, nat_gateway_id):
        logger.warn('deleting {}.'.format(nat_gateway_id))
        ec2_client.delete_nat_gateway(NatGatewayId=nat_gateway_id)
        while self.nat_gateway['State'] not in ['deleted', ]:
            time.sleep(5)
            self.nat_gateway = ec2_client.describe_nat_gateways(
                Filter=[
                    {
                        'Name': 'vpc-id',
                        'Values': [
                            self.vpc.id
                        ],
                    },
                ])['NatGateways'][0]
            logger.info("waiting on nat gateway deletion, state {}.".format(
                    self.nat_gateway['State']))

    def add_private_routing(self):
        for subnet in self.private_subnets:
            logger.info("configuring routing for {}.".format(subnet.id))
            subnet_routing_table = self.vpc.create_route_table()
            self.private_routing_tables.append(subnet_routing_table)

            self.rollback_callbacks.append(
                    (self.delete_routing_table,
                     [subnet_routing_table]))

            self.private_routes[
                    subnet.cidr_block] = subnet_routing_table.create_route(
                        DestinationCidrBlock=self.DEFAULT_ROUTE,
                        NatGatewayId=self.nat_gateway['NatGatewayId'])

            association = subnet_routing_table.associate_with_subnet(
                    SubnetId=subnet.id)

            self.rollback_callbacks.append((
                    self.delete_routing_table_association,
                    [association]))

    def delete_routing_table_association(self, association):
        logger.warn("deleting route table association {}.".format(
            association.route_table_association_id))
        association.delete()

    def add_internet_gateway(self):
        # An internet gateway is required to route traffic to the wider world
        #   Note: consumes an elastic IP and you probably don't have many.
        logger.info("creating internet gateway.")
        self.internet_gateway = ec2.create_internet_gateway()
        self.vpc.attach_internet_gateway(
                InternetGatewayId=self.internet_gateway.id)
        self.rollback_callbacks.append((self.delete_internet_gateway, []))
        logger.info("internet gateway {} created.".format(
                self.internet_gateway.id))
        return self.internet_gateway

    def delete_internet_gateway(self):
        logger.warn("deleting internet gateway {}".format(
                self.internet_gateway.id))
        self.vpc.detach_internet_gateway(
                InternetGatewayId=self.internet_gateway.id)
        self.internet_gateway.delete()

    def add_public_routing_table(self):
        logger.info("adding route table and default route.")
        # Add a routing table to the VPC that sends traffic out the internet
        # gateway. This is required for our services to talk to the internet.
        self.public_routing_table = self.vpc.create_route_table()
        self.rollback_callbacks.append(
                (self.delete_routing_table,
                 [self.public_routing_table]))
        logger.info("added route table {}.".format(
                self.public_routing_table.id))
        return self.public_routing_table

    def add_default_route(self):
        self.public_routes[
                self.DEFAULT_ROUTE] = self.public_routing_table.create_route(
                    DestinationCidrBlock=self.DEFAULT_ROUTE,
                    GatewayId=self.internet_gateway.id)

    def delete_routing_table(self, routing_table):
        logger.warn("deleting routing table {}.".format(routing_table.id))
        '''
        for route in routing_table.routes:
            logger.warn('deleting route {}.'.format(route.instance_id))
            route.delete()
        '''
        routing_table.delete()

    def create_public_subnet(self, cidr_block, availability_zone):
        logger.info("creating new private subnet {} in {}".format(
                cidr_block, availability_zone))
        subnet = ec2.create_subnet(AvailabilityZone=availability_zone,
                                   CidrBlock=cidr_block,
                                   VpcId=self.vpc.id)
        self.public_subnets.append(subnet)

        self.rollback_callbacks.append((self.delete_subnet, [subnet, ]))

        association = self.public_routing_table.associate_with_subnet(
                SubnetId=subnet.id)

        self.rollback_callbacks.append((
                self.delete_routing_table_association,
                [association]))

        logger.info("created new subnet {}.".format(subnet.id))
        return subnet

    def create_private_subnet(self, cidr_block, availability_zone):
        # Add a subnet to the VPC for things to live on
        logger.info("creating new private subnet {} in {}".format(
                cidr_block, availability_zone))
        subnet = ec2.create_subnet(AvailabilityZone=availability_zone,
                                   CidrBlock=cidr_block,
                                   VpcId=self.vpc.id)
        self.private_subnets.append(subnet)
        self.rollback_callbacks.append((self.delete_subnet, [subnet, ]))
        logger.info("created new private subnet {}.".format(subnet.id))
        return subnet

    def delete_subnet(self, subnet):
        logger.warn("deleting subnet {}.".format(subnet.id))
        subnet.delete()
        if subnet in self.private_subnets:
            self.private_subnets.remove(subnet)
        elif subnet in self.public_subnets:
            self.public_subnets.remove(subnet)

    def create_external_security_group(self):
        logger.info("creating external security group.")
        self.external_security_group = ec2.create_security_group(
                GroupName=self.external_security_group_name,
                Description='VPC External Communication',
                VpcId=self.vpc.id)
        self.external_security_group.authorize_ingress(
                    IpPermissions=[
                        {
                            'FromPort': 80,
                            'ToPort': 80,
                            'IpProtocol': 'tcp',
                            'IpRanges': [
                                {'CidrIp': '0.0.0.0/0',
                                 'Description': 'Everyone'}
                            ],
                            'Ipv6Ranges': [
                                {'CidrIpv6': '::/0',
                                 'Description': 'Everyone'}
                            ],

                        },
                        {
                            'FromPort': 443,
                            'ToPort': 443,
                            'IpProtocol': 'tcp',
                            'IpRanges': [
                                {'CidrIp': '0.0.0.0/0',
                                 'Description': 'Everyone'}
                            ],
                            'Ipv6Ranges': [
                                {'CidrIpv6': '::/0',
                                 'Description': 'Everyone'}
                            ],

                        }
                    ]
                )
        self.rollback_callbacks.append(
                (self.delete_security_group, [self.external_security_group]))
        logger.info("created security group {}.".format(
                self.external_security_group.id))
        return self.external_security_group

    def create_internal_security_group(self):
        logger.info("creating internal security group.")
        self.internal_security_group = ec2.create_security_group(
                GroupName=self.internal_security_group_name,
                Description='VPC Internal Communication',
                VpcId=self.vpc.id)
        self.rollback_callbacks.append(
                (self.delete_security_group,
                 [self.internal_security_group]))
        self.internal_security_group.authorize_ingress(
                    IpPermissions=[
                        {
                            'FromPort': 1,
                            'ToPort': 65535,
                            'IpProtocol': 'tcp',
                            'IpRanges': [
                                {'CidrIp': '172.16.0.0/20',
                                 'Description': 'VPC Internal'}
                            ],
                        },
                    ]
                )

        logger.info("created security group {}.".format(
                self.internal_security_group.id))
        return self.internal_security_group

    def delete_security_group(self, group):
        logger.warn("deleting security group {}".format(group.id))
        group.delete()

    def create_database_subnet_group(self):
        logger.info("creating db subnet group.")
        subnet_ids = [s.id for s in self.private_subnets]
        self.db_subnet_group = rds_client.create_db_subnet_group(
                DBSubnetGroupName=self.name,
                DBSubnetGroupDescription=self.name,
                SubnetIds=subnet_ids,
                Tags=[
                    {
                        'Key': 'Name',
                        'Value': self.name
                    },
                ]
            )

        self.rollback_callbacks.append((
                self.delete_database_subnet_group, [self.name]))
        logger.info("db subnet group {} created.".format(self.name))

        return self.db_subnet_group

    def delete_database_subnet_group(self, name):
        logger.warn("deleting database subnet group {}.".format(self.name))
        rds_client.delete_db_subnet_group(
            DBSubnetGroupName=name
        )

    def create_rds_db(self,
                      serverless=False,
                      deletion_protection=False,
                      db_instance_class='db.t3.medium'):

        engine_mode = 'provisioned'
        scaling_configuration = {}
        if serverless:
            engine_mode = 'serverless'
            scaling_configuration = {
                "MinCapacity": 2,
                "MaxCapacity": 4,
                "AutoPause": True,
                "SecondsUntilAutoPause": 300,
                "TimeoutAction": "RollbackCapacityChange"
            }
        self.database_cluster = rds_client.create_db_cluster(
            AvailabilityZones=list(self.public_availability_zones),
            BackupRetentionPeriod=7,
            DatabaseName=self.database_name,
            DBClusterIdentifier=self.name,
            DBClusterParameterGroupName='default.aurora-postgresql10',
            DBSubnetGroupName=self.name,
            VpcSecurityGroupIds=[
                self.internal_security_group.id
            ],
            Engine='aurora-postgresql',
            EngineVersion='10.7',
            Port=self.database_port,
            MasterUsername=self.database_user,
            MasterUserPassword=self.database_password,
            Tags=[
                {
                    'Key': 'Name',
                    'Value': self.name
                },
            ],
            StorageEncrypted=True,
            EngineMode=engine_mode,
            ScalingConfiguration=scaling_configuration,
            DeletionProtection=deletion_protection,
            EnableHttpEndpoint=False,
            CopyTagsToSnapshot=True,
        )['DBCluster']

        while self.database_cluster['Status'] != 'available':
            time.sleep(15)
            self.database_cluster = rds_client.describe_db_clusters(
                    DBClusterIdentifier=self.database_cluster[
                            'DBClusterIdentifier']
                    )['DBClusters'][0]
            logger.info("waiting for database cluster to provision, "
                        "status {}.".format(self.database_cluster['Status']))

        self.rollback_callbacks.append((
            self.delete_database_cluster,
            [self.database_cluster['DBClusterIdentifier']]))

        if not serverless:
            self.database_instance = rds_client.create_db_instance(
                DBClusterIdentifier=self.name,
                DBInstanceIdentifier=self.name,
                DBInstanceClass=db_instance_class,
                Engine='aurora-postgresql',
                EngineVersion='10.7',
                Tags=[
                    {
                        'Key': 'Name',
                        'Value': self.name
                    },
                ],
                MultiAZ=False,
                AvailabilityZone=list(self.public_availability_zones)[0],
                AutoMinorVersionUpgrade=True,
                LicenseModel='postgresql-license',
                PubliclyAccessible=False,
            )['DBInstance']
            while self.database_instance['DBInstanceStatus'] != 'available':
                time.sleep(15)
                self.database_instance = rds_client.describe_db_instances(
                        DBInstanceIdentifier=self.database_instance[
                                'DBInstanceIdentifier']
                        )['DBInstances'][0]
                logger.info("waiting for database instance to provision, "
                            "status {}.".format(
                                self.database_instance['DBInstanceStatus']))
            self.rollback_callbacks.append((
                self.delete_database_instance,
                [self.database_instance['DBInstanceIdentifier']]))

        return self.database_cluster


    def delete_database_cluster(self, db_id):
        logger.warn("deleting database cluster {}.".format(db_id))
        rds_client.delete_db_cluster(
            DBClusterIdentifier=db_id,
            SkipFinalSnapshot=True,
            )

        while self.database_cluster['Status'] != 'deleted':
            time.sleep(15)
            self.database_cluster = rds_client.describe_db_clusters(
                    DBClusterIdentifier=self.database_cluster[
                            'DBClusterIdentifier']
                    )['DBClusters'][0]
            logger.info("waiting for database to delete, "
                        "status {}.".format(self.database_cluster['Status']))

    def delete_database_instance(self, db_id):
        logger.warn("deleting database instance {}.".format(db_id))
        rds_client.delete_db_instance(
            DBInstanceIdentifier=db_id,
            SkipFinalSnapshot=True,
            DeleteAutomatedBackups=True,
            )

        while self.database_instance['DBInstanceStatus'] != 'deleted':
            time.sleep(15)
            self.database_instance = rds_client.describe_db_instances(
                    DBInstanceIdentifier=self.database_instance[
                            'DBInstanceIdentifier']
                    )['DBInstances'][0]
            logger.info("waiting for database to delete, "
                        "status {}.".format(
                            self.database_instance['DBInstanceStatus']))

    def create_ecs_cluster(self):
        logger.info("creating ecs cluster {}.".format(self.name))
        self.cluster = ecs_client.create_cluster(
            clusterName=self.name,
            tags=[
                {
                    'key': 'Name',
                    'value': self.name
                },
            ],
            settings=[
                {
                    'name': 'containerInsights',
                    'value': self.container_insights_status,
                },
            ],
            capacityProviders=[
                'FARGATE',
            ],
            defaultCapacityProviderStrategy=[
                {
                    'capacityProvider': 'FARGATE',
                    'weight': 1,
                    'base': 1,
                },
            ]
        )['cluster']

        while self.cluster.get('status') == "PROVISIONING":
            logger.info("waiting for cluser to provision, current status {}.".format(
                    self.cluster.get('status')))
            time.sleep(15)
            self.cluster = ecs_client.describe_clusters(
                    clusters=[self.name, ])['clusters'][0]

        self.rollback_callbacks.append((self.delete_ecs_cluster, [], ))
        logger.info("cluster {} created.".format(self.name))

        return self.cluster

    def delete_ecs_cluster(self):
        logger.warn("deleting ecs cluster {}.".format(self.name))
        ecs_client.delete_cluster(cluster=self.name)

    @property
    def backend_base_environment(self):
        return [
                {'name': "RDS_NAME",
                 'value': self.database_name},
                {'name': "RDS_HOST",
                 'value': self.database_cluster['Endpoint']},
                {'name': "RDS_PASSWORD",
                 'value': self.database_password},
                {'name': "RDS_USER",
                 'value': self.database_user},
                {'name': "RDS_PORT",
                 'value': str(self.database_port)},
                {'name': "FLEETING_SECRET_KEY",
                 'value': self.secret_key },
            ]

    def create_task_definition(
            self, family, container, tag, port,
            cpu="1024", memory="2048", env=[]):

        container_path = ""
        container_name = container
        if len(container.split('/')) > 1:
            container_path, container_name = container.rsplit('/', 1)

        logger.info("creating task {} from {}:{} exposed on {}.".format(
                container_name, container, tag, port))

        task_definition = ecs_client.register_task_definition(
                family=family,
                networkMode='awsvpc',
                requiresCompatibilities=[
                    'FARGATE',
                ],
                executionRoleArn=self.task_execution_role_arn,
                cpu=cpu,
                memory=memory,
                containerDefinitions=[
                    {
                        "name": container_name,
                        "image": ':'.join([container, tag]),
                        "cpu": 0,
                        "memoryReservation": 256,
                        "portMappings": [
                            {
                                "containerPort": port,
                                "hostPort": port,
                                "protocol": "tcp"
                            }
                        ],
                        "essential": True,
                        "environment": [
                            *env
                        ],
                        "logConfiguration": {
                            "logDriver": "awslogs",
                            "options": {
                                "awslogs-group": self.name,
                                "awslogs-region": self.region,
                                "awslogs-stream-prefix": "backend",
                            }
                        }
                    }
                ],
            )['taskDefinition']

        self.rollback_callbacks.append((
                self.delete_task,
                [family, str(task_definition['revision'])]))

        logger.info("created new task definition {}:{}.".format(
                    family, task_definition['revision']))

        self.tasks.append(task_definition)

        return task_definition

    def delete_task(self, family, revision):
        task_definition = ':'.join([family, revision, ])
        logger.warn("deregistering task {}.".format(task_definition))
        ecs_client.deregister_task_definition(taskDefinition=task_definition)

    def create_application_load_balancer(self):
        logger.info("creating new alb.")
        self.load_balancer = elbv2_client.create_load_balancer(
                Name=self.name,
                Subnets=[s.id for s in self.public_subnets],
                SecurityGroups=[
                    self.internal_security_group.id,
                    self.external_security_group.id,
                ],
                Scheme='internet-facing',
                Tags=[
                    {
                        'Key': 'Name',
                        'Value': self.name
                    },
                ],
                Type='application',
                IpAddressType='ipv4'
            )['LoadBalancers'][0]

        self.rollback_callbacks.append(
                (self.delete_application_load_balancer, []))

        logger.info("created alb {}.".format(self.load_balancer['DNSName']))
        return self.load_balancer

    def delete_application_load_balancer(self):
        logger.warn("deleting alb {}.".format(self.load_balancer['DNSName']))
        elbv2_client.delete_load_balancer(
                LoadBalancerArn=self.load_balancer['LoadBalancerArn'])

    @property
    def target_group_name(self):
        return "tg-{}".format(self.name)

    def create_target_group(self):
        logger.info("creating target group.")
        self.target_group = elbv2_client.create_target_group(
                Name=self.target_group_name,
                Protocol='HTTP',
                Port=80,
                VpcId=self.vpc.id,
                HealthCheckProtocol='HTTP',
                HealthCheckPort='80',
                HealthCheckEnabled=True,
                HealthCheckPath='/_health',
                HealthCheckIntervalSeconds=60,
                HealthCheckTimeoutSeconds=5,
                HealthyThresholdCount=3,
                UnhealthyThresholdCount=3,
                Matcher={
                    'HttpCode': '200'
                },
                TargetType='ip'
            )['TargetGroups'][0]

        self.rollback_callbacks.append((self.delete_target_group, []))

        logger.info("created target group {}.".format(
                self.target_group['TargetGroupName']))
        return self.target_group

    def delete_target_group(self):
        logger.warn("deleting target group {}.".format(
                self.target_group['TargetGroupName']))
        elbv2_client.delete_target_group(
                TargetGroupArn=self.target_group['TargetGroupArn'])

    @property
    def internal_dns_namespace_domain(self):
        return 'local'

    def create_internal_dns_namespace(self):
        logger.info("creating internal dns namespace {}.".format(
            self.internal_dns_namespace_domain))
        internal_dns_namespace_op_id = sd_client.create_private_dns_namespace(
            Name=self.internal_dns_namespace_domain,
            Vpc=self.vpc.id
        )['OperationId']

        op = sd_client.get_operation(
                OperationId=internal_dns_namespace_op_id)['Operation']

        while op['Status'] not in ["SUCCESS", "ERROR"]:
            logger.info("waiting for internal namespace to provision, "
                         "status {}.".format(op['Status']))
            op = sd_client.get_operation(
                    OperationId=internal_dns_namespace_op_id)['Operation']
            time.sleep(15)
        self.internal_dns_namespace_id = op['Targets']['NAMESPACE']
        self.internal_dns_namespace = sd_client.get_namespace(
                Id=self.internal_dns_namespace_id)['Namespace']

        self.rollback_callbacks.append(
                (self.delete_internal_dns_namespace, []))

        logger.info("created new namespace {}.".format(
                self.internal_dns_namespace_id))

        return self.internal_dns_namespace

    def delete_internal_dns_namespace(self):
        logger.warn("deleting namespace {}.".format(
                self.internal_dns_namespace_id))
        sd_client.delete_namespace(Id=self.internal_dns_namespace_id)

    def register_namespace_services(self):
        logger.info("creating namespace services.")
        self.internal_dns_backend_service = sd_client.create_service(
            Name='backend',
            NamespaceId=self.internal_dns_namespace['Id'],
            DnsConfig={
                'NamespaceId': self.internal_dns_namespace['Id'],
                'RoutingPolicy': 'WEIGHTED',
                'DnsRecords': [
                    {
                        'Type': 'A',
                        'TTL': 600,
                    },
                ]
            })['Service']

        self.rollback_callbacks.append((
                self.deregister_namespace_service,
                [self.internal_dns_backend_service['Id']]))

        self.internal_dns_frontend_service = sd_client.create_service(
            Name='frontend',
            NamespaceId=self.internal_dns_namespace['Id'],
            DnsConfig={
                'NamespaceId': self.internal_dns_namespace['Id'],
                'RoutingPolicy': 'WEIGHTED',
                'DnsRecords': [
                    {
                        'Type': 'A',
                        'TTL': 600,
                    },
                ]
            })['Service']

        logger.info("created namespace services {}/{}.".format(
                self.internal_dns_backend_service['Id'],
                self.internal_dns_frontend_service['Id'],
            ))

        self.rollback_callbacks.append((
                self.deregister_namespace_service,
                [self.internal_dns_frontend_service['Id']]))

        return (self.internal_dns_backend_service,
                self.internal_dns_frontend_service)

    def deregister_namespace_service(self, id):
        logger.warn("deleting namespace service {}.".format(id))
        sd_client.delete_service(Id=id)
        time.sleep(15)

    @property
    def backend_task_name(self):
        return "{}-backend".format(self.name)

    @property
    def frontend_task_name(self):
        return "{}-frontend".format(self.name)

    def create_services(self):
        logger.info("creating services.")
        self.rollback_callbacks.append((
                self.wait_for_services_inactive,
                [['backend', 'frontend', ], ]))
        self.backend_service = ecs_client.create_service(
                cluster=self.name,
                serviceName='backend',
                taskDefinition=self.backend_task_name,
                desiredCount=1,
                clientToken='request_identifier_string',
                deploymentConfiguration={
                    'maximumPercent': 200,
                    'minimumHealthyPercent': 50
                },
                serviceRegistries=[
                    {
                        'registryArn': self.internal_dns_backend_service['Arn'],
                    },
                ],
                networkConfiguration={
                    "awsvpcConfiguration": {
                        "subnets": [s.id for s in self.private_subnets],
                        "securityGroups": [
                            self.internal_security_group.id
                        ],
                        "assignPublicIp": "DISABLED",
                    },
                },
            )['service']

        self.rollback_callbacks.append(
                (self.delete_service,
                 [self.backend_service['serviceName']]))

        self.frontend_service = ecs_client.create_service(
                cluster=self.name,
                serviceName='frontend',
                taskDefinition=self.frontend_task_name,
                desiredCount=1,
                clientToken='request_identifier_string',
                deploymentConfiguration={
                    'maximumPercent': 200,
                    'minimumHealthyPercent': 50
                },
                serviceRegistries=[
                    {
                        'registryArn': self.internal_dns_frontend_service['Arn'],
                    },
                ],
                loadBalancers=[
                    {
                        'targetGroupArn': self.target_group['TargetGroupArn'],
                        'containerName': 'fleetingforms-nginx',
                        'containerPort': 80,
                    }
                ],
                networkConfiguration={
                    "awsvpcConfiguration": {
                        "subnets": [s.id for s in self.private_subnets],
                        "securityGroups": [
                            self.internal_security_group.id
                        ],
                        "assignPublicIp": "DISABLED",
                    },
                },
            )['service']

        self.rollback_callbacks.append(
                (self.delete_service,
                 [self.frontend_service['serviceName']]))

        logger.info("created services {}/{}.".format(
                self.backend_service['serviceName'],
                self.frontend_service['serviceName'],
                ))

        return self.frontend_service, self.backend_service

    def wait_for_services_inactive(self, services):
        waiter = ecs_client.get_waiter('services_inactive')
        waiter.wait()

    def delete_service(self, service_name):
        logger.warn("deleting services {}.".format(service_name))
        ecs_client.delete_service(cluster=self.name,
                                  service=service_name,
                                  force=True)
        time.sleep(5)

    def get_certificate(self):
        self.certificate_arn = None
        for cert in acm_client.list_certificates().get('CertificateSummaryList'):
            if cert.get('DomainName') == self.domain_name:
                self.certificate_arn = cert.get('CertificateArn')
                break
        if not self.certificate_arn:
            raise Exception("can't find certificate {}.".format(
                    self.domain_name))

    def create_https_listener(self):
        self.https_listener = elbv2_client.create_listener(
            LoadBalancerArn=self.load_balancer['LoadBalancerArn'],
            Protocol='HTTPS',
            Port=443,
            SslPolicy='ELBSecurityPolicy-2016-08',
            Certificates=[
                {
                    'CertificateArn': self.certificate_arn,
                },
            ],
            DefaultActions=[
                {
                    'Type': 'forward',
                    'TargetGroupArn': self.target_group['TargetGroupArn']
                },
            ],
        )['Listeners'][0]

        self.rollback_callbacks.append(
                (self.delete_listener,
                 [self.https_listener['ListenerArn']]))

        return self.https_listener

    def create_http_listener(self):
        self.http_listener = elbv2_client.create_listener(
            LoadBalancerArn=self.load_balancer['LoadBalancerArn'],
            Protocol='HTTP',
            Port=80,
            DefaultActions=[
                {
                    'Type': 'redirect',
                    'RedirectConfig': {
                        'Protocol': 'HTTPS',
                        'Host': '#{host}',
                        'Path': '/#{path}',
                        'Port': '443',
                        'StatusCode': 'HTTP_301',
                    },
                },
            ],
        )['Listeners'][0]

        self.rollback_callbacks.append(
                (self.delete_listener,
                 [self.http_listener['ListenerArn']]))

        return self.http_listener

    def delete_listener(self, listener_arn):
        logger.warning("deleting listener {}.".format(listener_arn))
        elbv2_client.delete_listener(ListenerArn=listener_arn)

    def create_log_destinations(self):
        self.log_group = log_client.create_log_group(
                logGroupName=self.name,
                tags={'Name': self.name})

        self.rollback_callbacks.append((
                self.delete_log_group, [self.name, ]))

        self.backend_log_stream = log_client.create_log_stream(
                logGroupName=self.name,
                logStreamName='backend')

        self.rollback_callbacks.append((
                self.delete_log_stream,
                [self.name, 'backend', ]
            ))

        self.frontend_log_stream = log_client.create_log_stream(
                logGroupName=self.name,
                logStreamName='frontend')

        self.rollback_callbacks.append((
                self.delete_log_stream,
                [self.name, 'frontend', ]
            ))

    def delete_log_group(self, name):
        log_client.delete_log_group(
                logGroupName=self.name)

    def delete_log_stream(self, name, stream):
        log_client.delete_log_stream(
                logGroupName=self.name,
                logStreamName=stream)

if __name__ == '__main__':
    main()
