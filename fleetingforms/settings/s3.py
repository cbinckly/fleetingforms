import os
# S3 Backend
AWS_ACCESS_KEY_ID = os.environ.get("AWS_ACCESS_KEY_ID")
AWS_SECRET_ACCESS_KEY = os.environ.get("AWS_SECRET_ACCESS_KEY")
AWS_STORAGE_BUCKET_NAME = "ffio-static"
AWS_BUCKET_ACL = "private"
AWS_DEFAULT_ACL = "private"
AWS_S3_CUSTOM_DOMAIN = "s3.amazonaws.com/{}".format(AWS_STORAGE_BUCKET_NAME)

DEFAULT_FILE_STORAGE = 'fleetingforms.storage_backends.PublicMediaStorage'
AWS_PUBLIC_MEDIA_LOCATION = 'media/public'

PRIVATE_FILE_STORAGE = 'fleetingforms.storage_backends.PrivateMediaStorage'
AWS_PRIVATE_MEDIA_LOCATION = 'media/private'

AWS_STATIC_LOCATION = 'static'
STATICFILES_STORAGE = 'fleetingforms.storage_backends.StaticStorage'
STATIC_URL = "https://{}/{}/".format(AWS_S3_CUSTOM_DOMAIN, AWS_STATIC_LOCATION)
