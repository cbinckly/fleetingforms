import os
from passlib.hash import (pbkdf2_sha256,
                          pbkdf2_sha512,
                          bcrypt,
                          argon2, )
from .base import DEBUG

# Default domain - used to render friendly namespace strings.
FLEETING_DOMAIN = os.environ.get('FLEETING_DOMAIN',
                                 'fleetingforms.io')

# The URL scheme to use. HTTPS is pretty much required unless you're testing.
FLEETING_URL_SCHEME = os.environ.get("FLEETING_URL_SCHEME", "https://")

# Form one time code length. 6 to 12 recommended.
FLEETING_CODE_LENGTH = int(os.environ.get('FLEETING_CODE_LENGTH', 8))

# Header field for Fleeting Token.
FLEETING_TOKEN_HEADER = "HTTP_X_FLEETING_TOKEN"

# The list, and modules, for the support password hashers.
FLEETING_DEFAULT_HASHER = pbkdf2_sha256
FLEETING_HASHERS = {
        'pbkdf2-sha256': pbkdf2_sha256,
        'pbkdf2-sha512': pbkdf2_sha512,
        'bcrypt': bcrypt,
        'argon2': argon2,
    }

# Limit the maximum number of password hash iterations.
FLEETING_MAX_PASS_ITERS = 100000

# Limit the manimum number of one time passcodes a user can receive per form.
FLEETING_OTP_MAX_ATTEMPTS = int(os.environ.get("FLEETING_OTP_MAX_ATTEMPTS", 3))

PHC_REGEX= (r'^\$(?P<id>[a-z\-0-9]+)'
             '(\$(?P<params>((([a-z0-9]+=[a-z0-9]+)[,]*)+|\d+)))'
             '(\$(?P<salt>[^$]+))(\$(?P<hash>.+))$')

# The minimum number of characters in a plain text password.
FLEETING_PASSWORD_MIN_CHARS = int(os.environ.get("FLEETING_PASSWORD_MIN_CHARS",
                                  3))

# The maximum configurable retention for a namespace.
FLEETING_MAX_RETENTION = int(os.environ.get(
        "FLEETING_MAX_RETENTION", 365))
