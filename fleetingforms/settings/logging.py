from .base import DEBUG

LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'formatter': 'verbose'
                },
            },
        'formatters': {
            'verbose': {
                'format': ('{asctime} {levelname} {module} {process:d} '
                           '{thread:d} {message}'),
                'style': '{',
                'datefmt': '%Y-%m-%d %H:%M:%S'
                },
            'simple': {
                'format': '{levelname} {module} {message}',
                'style': '{',
                },
            },
        'root': {
            'handlers': ['console'],
            'level': 'INFO',
            },
        }

if DEBUG:
    LOGGING['root']['level'] = 'DEBUG'
