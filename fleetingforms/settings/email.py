import os

EMAIL_HOST = os.environ.get("FLEETING_EMAIL_HOST", 'smtp.sendgrid.net')
EMAIL_PORT = os.environ.get("FLEETING_EMAIL_PORT", 587)
EMAIL_HOST_USER = os.environ.get("FLEETING_EMAIL_USER")
EMAIL_HOST_PASSWORD = os.environ.get("FLEETING_EMAIL_PASS")
EMAIL_USE_TLS = True
EMAIL_FROM = os.environ.get("FLEETING_EMAIL_FROM", "rosie@fleetingforms.io")
