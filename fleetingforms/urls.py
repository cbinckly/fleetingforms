"""fleetingforms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from fleetingform.views import (
            FleetingFormListCreateView,
            FleetingFormRetrieveDestroyView,
            FleetingNamespaceListCreateView,
            FleetingNamespaceRetrieveUpdateView,
            FleetingFormLoginView,
            FleetingOTPResetRedirectView,
            UserFormView,
            FleetingIndexView,
        )

urlpatterns = [
    path('admin/', admin.site.urls),
    path('forms/', FleetingFormListCreateView.as_view(), name='form-list'),
    path('forms/<pk>/',
         FleetingFormRetrieveDestroyView.as_view(),
         name='form-retrieve'),
    path('namespaces/',
         FleetingNamespaceListCreateView.as_view(),
         name='namespace-list'),
    path('namespaces/<pk>/',
         FleetingNamespaceRetrieveUpdateView.as_view(),
         name='namespace-retrieve'),
    path('<code>/login/resend',
         FleetingOTPResetRedirectView.as_view(),
         name='otp-resend'),
    path('<code>/login', FleetingFormLoginView.as_view(), name='form-login'),
    path('<code>/', UserFormView.as_view(), name="form-display"),
    path('', FleetingIndexView.as_view(), name="index")
]
