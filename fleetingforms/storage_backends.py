from django.conf import settings
from storages.backends.s3boto3 import S3Boto3Storage

class S3Boto3StorageWithTree(S3Boto3Storage):

    def delete_tree(self, root):
        name = self._normalize_name(self._clean_name(root))
        self.bucket.objects.filter(Prefix=name).delete()

        for key in self._entries:
            if name in key:
                del self._entries[name]

class StaticStorage(S3Boto3Storage):
    default_acl = 'public-read'
    location = settings.AWS_STATIC_LOCATION

class PublicMediaStorage(S3Boto3StorageWithTree):
    default_acl = 'public-read'
    location = settings.AWS_PUBLIC_MEDIA_LOCATION
    file_overwrite = False

class PrivateMediaStorage(S3Boto3StorageWithTree):
    default_acl = 'private'
    location = settings.AWS_PRIVATE_MEDIA_LOCATION
    file_overwrite = True
    custom_domain = False

