import random
import smtplib
from unittest import mock

from django import forms
from django.test import TestCase
from django.conf import settings
from rest_framework import serializers
from twilio.base.exceptions import TwilioRestException

from fleetingform.models import (FleetingFormControl, )
from fleetingform.factories import (FleetingTemplateFormControlFactory,
                                    FleetingFormFactory,
                                    FleetingUserFactory)
from fleetingform.lib.form_generator import generate_fleeting_form_class_for
from fleetingform.lib.form_control_validators import (
        EmptyFormControlParamsValidator,
        DecimalFormControlParamsValidator, )
from fleetingform.lib import send_otp_email, send_otp_sms


class FleetingFormGeneratorTestCase(TestCase):

    def setUp(self):
        """
        user, _ = User.objects.get_or_create(username="test",
                                          email="test@email.com")
        self.namespace, _ = FleetingNamespace.objects.get_or_create(
                subdomain='fftest', user=User.objects.first())
        self.headers = { settings.FLEETING_TOKEN_HEADER: self.namespace.token }
        self.minimal_form = {
                "template": {
                    "type": "generic",
                    "title": "One Button",
                    "actions": [{"label": "Push Me"}]
                    },
            }
        factory = APIRequestFactory()
        self.request = factory.post(reverse('form-list'),
                                self.minimal_form,
                                **self.headers)
        self.request.namespace = self.namespace
        """
        pass

    def test_form_control_text_create(self):
        form_control = FleetingTemplateFormControlFactory(
                type=FleetingFormControl.FIELD_TYPE_TEXT,
                choices=[],
                validations=[])
        form_class = generate_fleeting_form_class_for([form_control, ])
        form = form_class()
        self.assertTrue(form_control.name in form.fields)
        self.assertIsInstance(form.fields[form_control.name], forms.CharField)

    def test_form_control_textarea_create(self):
        form_control = FleetingTemplateFormControlFactory(
                type=FleetingFormControl.FIELD_TYPE_TEXTAREA,
                choices=[],
                validations=[])
        form_class = generate_fleeting_form_class_for([form_control, ])
        form = form_class()
        self.assertTrue(form_control.name in form.fields)
        self.assertIsInstance(form.fields[form_control.name], forms.CharField)
        self.assertIsInstance(
                form.fields[form_control.name].widget, forms.Textarea)

    def test_form_control_integer_create(self):
        form_control = FleetingTemplateFormControlFactory(
                type=FleetingFormControl.FIELD_TYPE_INTEGER,
                choices=[],
                validations=[])
        form_class = generate_fleeting_form_class_for([form_control, ])
        form = form_class()
        self.assertTrue(form_control.name in form.fields)
        self.assertIsInstance(
                form.fields[form_control.name], forms.IntegerField)

    def test_form_control_float_create(self):
        form_control = FleetingTemplateFormControlFactory(
                type=FleetingFormControl.FIELD_TYPE_FLOAT,
                choices=[],
                validations=[])
        form_class = generate_fleeting_form_class_for([form_control, ])
        form = form_class()
        self.assertTrue(form_control.name in form.fields)
        self.assertIsInstance(
                form.fields[form_control.name], forms.FloatField)

    def test_form_control_boolean_create(self):
        form_control = FleetingTemplateFormControlFactory(
                type=FleetingFormControl.FIELD_TYPE_BOOLEAN,
                choices=[],
                validations=[])
        form_class = generate_fleeting_form_class_for([form_control, ])
        form = form_class()
        self.assertTrue(form_control.name in form.fields)
        self.assertIsInstance(
                form.fields[form_control.name], forms.BooleanField)

    def test_form_control_time_create(self):
        form_control = FleetingTemplateFormControlFactory(
                type=FleetingFormControl.FIELD_TYPE_TIME,
                choices=[],
                validations=[])
        form_class = generate_fleeting_form_class_for([form_control, ])
        form = form_class()
        self.assertTrue(form_control.name in form.fields)
        self.assertIsInstance(
                form.fields[form_control.name], forms.TimeField)

    def test_form_control_date_create(self):
        form_control = FleetingTemplateFormControlFactory(
                type=FleetingFormControl.FIELD_TYPE_DATE,
                choices=[],
                validations=[])
        form_class = generate_fleeting_form_class_for([form_control, ])
        form = form_class()
        self.assertTrue(form_control.name in form.fields)
        self.assertIsInstance(
                form.fields[form_control.name], forms.DateField)

    def test_form_control_datetime_create(self):
        form_control = FleetingTemplateFormControlFactory(
                type=FleetingFormControl.FIELD_TYPE_DATETIME,
                choices=[],
                validations=[])
        form_class = generate_fleeting_form_class_for([form_control, ])
        form = form_class()
        self.assertTrue(form_control.name in form.fields)
        self.assertIsInstance(
                form.fields[form_control.name], forms.DateTimeField)

    def test_form_control_url_create(self):
        form_control = FleetingTemplateFormControlFactory(
                type=FleetingFormControl.FIELD_TYPE_URL,
                choices=[],
                validations=[])
        form_class = generate_fleeting_form_class_for([form_control, ])
        form = form_class()
        self.assertTrue(form_control.name in form.fields)
        self.assertIsInstance(
                form.fields[form_control.name], forms.URLField)

    def test_form_control_email_create(self):
        form_control = FleetingTemplateFormControlFactory(
                type=FleetingFormControl.FIELD_TYPE_EMAIL,
                choices=[],
                validations=[])
        form_class = generate_fleeting_form_class_for([form_control, ])
        form = form_class()
        self.assertTrue(form_control.name in form.fields)
        self.assertIsInstance(
                form.fields[form_control.name], forms.EmailField)

    def test_form_control_choice_create(self):
        form_control = FleetingTemplateFormControlFactory(
                type=FleetingFormControl.FIELD_TYPE_CHOICE,
                choices=[(1, 'One', ), (2, 'Two', )],
                validations=[])
        form_class = generate_fleeting_form_class_for([form_control, ])
        form = form_class()
        self.assertTrue(form_control.name in form.fields)
        self.assertIsInstance(
                form.fields[form_control.name], forms.ChoiceField)

    def test_form_control_decimal_create(self):
        form_control = FleetingTemplateFormControlFactory(
                type=FleetingFormControl.FIELD_TYPE_DECIMAL,
                choices=[],
                validations=[])
        form_class = generate_fleeting_form_class_for([form_control, ])
        form = form_class()
        self.assertTrue(form_control.name in form.fields)
        self.assertIsInstance(
                form.fields[form_control.name], forms.DecimalField)

    def test_form_control_decimal_params(self):
        form_control = FleetingTemplateFormControlFactory(
                type=FleetingFormControl.FIELD_TYPE_DECIMAL,
                choices=[],
                validations=[],
                params={'max_digits': 20, 'decimal_places': 4})
        form_class = generate_fleeting_form_class_for([form_control, ])
        form = form_class()
        self.assertEqual(form.fields[form_control.name].max_digits, 20)
        self.assertEqual(form.fields[form_control.name].decimal_places, 4)

    def test_form_control_decimal_default_params(self):
        form_control = FleetingTemplateFormControlFactory(
                type=FleetingFormControl.FIELD_TYPE_DECIMAL,
                choices=[],
                validations=[],
                params={})
        form_class = generate_fleeting_form_class_for([form_control, ])
        form = form_class()
        self.assertEqual(form.fields[form_control.name].max_digits, 21)
        self.assertEqual(form.fields[form_control.name].decimal_places, 5)

    def test_form_control_hidden_create(self):
        form_control = FleetingTemplateFormControlFactory(
                type=FleetingFormControl.FIELD_TYPE_TEXT,
                hidden=True,
                choices=[],
                validations=[])
        form_class = generate_fleeting_form_class_for([form_control, ])
        form = form_class()
        self.assertTrue(form_control.name in form.fields)
        self.assertIsInstance(
                form.fields[form_control.name], forms.CharField)
        self.assertIsInstance(
                form.fields[form_control.name].widget, forms.HiddenInput)



class ExternalAPITestCase(TestCase):

    def setUp(self):
        self.fform = FleetingFormFactory()

    @mock.patch('fleetingform.lib.send_mail')
    def test_send_otp_email_calls_send_email_with_args(self, mock_send):
        user = FleetingUserFactory()
        otp = random.randint(100000,999999)
        send_otp_email(user.email, otp, self.fform)
        mock_send.assert_called_once()
        call_args = mock_send.mock_calls[0].args
        self.assertTrue(self.fform.template.title[:80] in call_args[0])
        self.assertTrue(str(otp) in call_args[1])
        self.assertEqual(settings.EMAIL_FROM, call_args[2])
        self.assertTrue(user.email in call_args[3])


    @mock.patch('fleetingform.lib.send_mail',
                side_effect=smtplib.SMTPException)
    def test_send_otp_email_raises_on_smtp_exception(self, mock_send):
        user = FleetingUserFactory()
        otp = random.randint(100000,999999)
        with self.assertRaises(smtplib.SMTPException):
            send_otp_email(user.email, otp, self.fform)

    @mock.patch('fleetingform.lib.TwilioClient')
    def test_send_otp_sms_calls_send_twilio_with_args(self, mock_send):
        user = FleetingUserFactory()
        otp = random.randint(100000,999999)
        mock_send.return_value.messages.create.return_value = \
                mock.Mock(error_code=None)
        send_otp_sms(str(user.phone), otp, self.fform)
        mock_send.assert_called_once()
        init_call = mock_send.mock_calls[0]
        send_call = mock_send.mock_calls[1]
        self.assertEqual((settings.TWILIO_ACCOUNT_SID,
                          settings.TWILIO_ACCOUNT_TOKEN,),
                          init_call.args)
        self.assertTrue(
                self.fform.template.title[:80] in send_call.kwargs['body'])
        self.assertTrue(str(otp) in send_call.kwargs['body'])
        self.assertEqual(settings.TWILIO_FROM_PHONE, send_call.kwargs['from_'])
        self.assertEqual(str(user.phone), send_call.kwargs['to'])

    @mock.patch('fleetingform.lib.TwilioClient')
    def test_send_otp_phone_returns_false_exception(self, mock_send):
        user = FleetingUserFactory()
        otp = random.randint(100000, 999999)
        mock_send.return_value.messages.create.side_effect = \
            TwilioRestException('failed', 'https://twilio', 'fail', 'bad')
        self.assertFalse(send_otp_sms(str(user.phone), otp, self.fform))

    @mock.patch('fleetingform.lib.TwilioClient')
    def test_send_otp_phone_returns_false_error(self, mock_send):
        user = FleetingUserFactory()
        otp = random.randint(100000, 999999)
        mock_send.return_value.messages.create.return_value = \
            mock.Mock(error_code="error!")
        self.assertFalse(send_otp_sms(str(user.phone), otp, self.fform))


class DecimalFormControlParamsValidatorTestCase(TestCase):

    def test_rejects_non_integer_params(self):
        params = {'max_digits': '22', 'decimal_places': '4'}
        with self.assertRaises(serializers.ValidationError):
            DecimalFormControlParamsValidator(params).validate()

    def test_rejects_bad_params(self):
        params = {'stuff_places': '4'}
        with self.assertRaises(serializers.ValidationError):
            DecimalFormControlParamsValidator(params).validate()

    def test_rejects_overlength_max_digits(self):
        params = {'max_digits': 22}
        with self.assertRaises(serializers.ValidationError):
            DecimalFormControlParamsValidator(params).validate()

    def test_rejects_overlength_decimal_places(self):
        params = {'decimal_places': 22}
        with self.assertRaises(serializers.ValidationError):
            DecimalFormControlParamsValidator(params).validate()

    def test_rejects_decimal_places_gt_max_digits(self):
        params = {'decimal_places': 18, 'max_digits': 8}
        with self.assertRaises(serializers.ValidationError):
            DecimalFormControlParamsValidator(params).validate()

    def test_rejects_negative_max_digits(self):
        params = {'max_digits': -18}
        with self.assertRaises(serializers.ValidationError):
            DecimalFormControlParamsValidator(params).validate()

    def test_rejects_negative_decimal_places(self):
        params = {'decimal_places': -18}
        with self.assertRaises(serializers.ValidationError):
            DecimalFormControlParamsValidator(params).validate()

    def test_accepts_valid_config(self):
        params = {'decimal_places': 4, 'max_digits': 8}
        valid_params = DecimalFormControlParamsValidator(params).validate()
        self.assertEqual(params, valid_params)


class EmptyFormControlParamsValidatorTestCase(TestCase):

    def test_accepts_empty_params(self):
        params = {}
        valid_params = EmptyFormControlParamsValidator(params).validate()
        self.assertEqual(params, valid_params)

    def test_rejects_all_params(self):
        params = {'max_digits': '22', 'empty_places': '4'}
        with self.assertRaises(serializers.ValidationError):
            EmptyFormControlParamsValidator(params).validate()

