from django.urls import reverse
from django.test import TestCase
from django.conf import settings
from django.contrib.auth.models import User
from rest_framework.test import APIRequestFactory
from rest_framework import status
from rest_framework.authtoken.models import Token

from fleetingform.models import (FleetingForm,
                                 FleetingTemplate,
                                 FleetingAction,
                                 FleetingAuth,)
from fleetingform.views import (FleetingFormListCreateView, )
from fleetingform.factories import FleetingNamespaceFactory, UserFactory


class FleetingAPIAuthenticationTestCase(TestCase):

    def setUp(self):
        user, _ = User.objects.get_or_create(username="test",
                                             email="test@email.com")
        self.namespace = FleetingNamespaceFactory(user=user)
        self.headers = {settings.FLEETING_TOKEN_HEADER: self.namespace.token}
        self.minimal_form = {
                "template": {
                    "type": "generic",
                    "title": "One Button",
                    "actions": [{"label": "Push Me"}]
                    },
            }

    def assertObjectCounts(self, class_counts):
        for _class, count in class_counts.items():
            self.assertEqual(_class.objects.count(), count)

    def test_authenticate_requires_token(self):
        """The minimum argument set is correct."""
        factory = APIRequestFactory()
        self.request = factory.post(reverse('form-list'),
                                    self.minimal_form, )
        self.request.namespace = None
        response = FleetingFormListCreateView.as_view()(self.request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertObjectCounts({FleetingForm: 0,
                                 FleetingTemplate: 0,
                                 FleetingAuth: 0})

    def test_template_requires_valid_token(self):
        """The minimum argument set is correct."""
        factory = APIRequestFactory()
        self.minimal_form['template'].pop('title')
        self.request = factory.post(
                reverse('form-list'),
                self.minimal_form,
                **{settings.FLEETING_TOKEN_HEADER:
                   "9c843126-7d26-4ca7-94e6-85fd18c73ff0"})
        response = FleetingFormListCreateView.as_view()(self.request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertObjectCounts({FleetingForm: 0,
                                 FleetingTemplate: 0,
                                 FleetingAction: 0,
                                 FleetingAuth: 0})

    def test_template_requires_valid_uuid(self):
        """The minimum argument set is correct."""
        factory = APIRequestFactory()
        self.minimal_form['template'].pop('title')
        self.request = factory.post(reverse('form-list'),
                                    self.minimal_form,
                                    **{settings.FLEETING_TOKEN_HEADER: "ABCD"})
        self.request.namespace = None
        response = FleetingFormListCreateView.as_view()(self.request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertObjectCounts({FleetingForm: 0,
                                 FleetingTemplate: 0,
                                 FleetingAction: 0,
                                 FleetingAuth: 0})


class UserTokenGenerationTestCase(TestCase):

    def test_token_is_created(self):
        u = UserFactory()
        self.assertEqual(Token.objects.filter(user=u).count(), 1)

    def test_token_is_not_overwritten_on_update(self):
        u = UserFactory()
        token = Token.objects.get(user=u)
        u.first_name = "Joe"
        u.save()
        self.assertEqual(token, Token.objects.get(user=u))
