from django.test import TestCase

from rest_framework.serializers import ValidationError

from fleetingform.template_helpers import (FleetingTemplateHelper,
                                           FleetingAuthValidator, )


class FleetingTemplateHelperTestCase(TestCase):

    def test_defines_and_requires_implementation_of_validate_params(self):
        with self.assertRaises(NotImplementedError):
            FleetingTemplateHelper()

    def test_requires_definition_of_type(self):
        cls = FleetingTemplateHelper
        cls.type = 'something'
        with self.assertRaises(NotImplementedError):
            t = cls()
            t.validate_params({})


class FleetingAuthParamValidatorTestCase(TestCase):

    def setUp(self):
        self.validator = FleetingAuthValidator('none')
        self.users = [
                {"username": "chris",
                 "password": "plain:password",
                 "email": "email@place.com",
                 "phone": "+14164567465"},
                {"username": "fen",
                 "password": "plain:password",
                 "email": "fen@place.com",
                 "phone": "+14154567465"},
        ]

    def test_verify_all_users_have_field(self):
        self.assertEqual(
                self.validator._verify_all_users_have_field(
                    self.users, 'username'), self.users)

    def test_verify_all_users_have_field_raises_when_missing(self):
        self.users.append({"email": "E@G.com"})
        with self.assertRaises(ValidationError):
            self.validator._verify_all_users_have_field(
                self.users, 'username')

    def test_verify_all_users_have_field_raises_when_missing_from_all(self):
        with self.assertRaises(ValidationError):
            self.validator._verify_all_users_have_field(
                self.users, 'fake')

    def test_verify_all_users_have_unique_field(self):
        self.assertEqual(
                self.validator._verify_all_users_have_unique_field(
                    self.users, 'username'), self.users)

    def test_verify_all_users_have_unique_field_raises_when_missing(self):
        self.users.append({"email": "E@G.com"})
        with self.assertRaises(ValidationError):
            self.validator._verify_all_users_have_unique_field(
                self.users, 'username')

    def test_verify_all_users_have_unique_field_raises_when_duplicated(self):
        with self.assertRaises(ValidationError):
            self.validator._verify_all_users_have_unique_field(
                self.users, 'password')

    def test_verify_all_users_have_unique_field_raises_missing_from_all(self):
        with self.assertRaises(ValidationError):
            self.validator._verify_all_users_have_unique_field(
                self.users, 'fake')

    def test_verify_max_user_count(self):
        self.assertEqual(self.validator._verify_max_user_count(self.users, 3),
                         self.users)
        self.assertEqual(self.validator._verify_max_user_count(self.users, 2),
                         self.users)

    def test_verify_max_user_count_raises(self):
        with self.assertRaises(ValidationError):
            self.assertEqual(
                    self.validator._verify_max_user_count(self.users, 1),
                    self.users)

    def test_verify_field_not_set(self):
        self.assertEqual(
                self.validator._verify_field_not_set(
                    self.users, 'fake'), self.users)

    def test_verify_field_not_set_raises_when_set(self):
        self.users[0]["fake"] = "E@G.com"
        with self.assertRaises(ValidationError):
            self.validator._verify_field_not_set(
                self.users, 'fake')

    def test_validate_raises_when_users_provided_but_not_required(self):
        with self.assertRaises(ValidationError):
            self.validator.validate({'type': 'none', 'users': self.users})

    def test_validates_when_no_users_provided_and_none_required(self):
        self.assertEqual(self.validator.validate(
                {'type': 'none', 'users': []}),
                {'type': 'none', 'users': []})

    def test_validate_raises_when_users_required_but_not_provided(self):
        with self.assertRaises(ValidationError):
            self.validator.validate({'type': 'user', 'users': []})

    def test_validates_when_users_required_and_provided(self):
        self.assertEqual(self.validator.validate(
                {'type': 'user', 'users': self.users}),
                {'type': 'user', 'users': self.users},)

    def test_validate_enforces_one_user_required_empty(self):
        with self.assertRaises(ValidationError):
            self.validator.validate({'type': 'pass', 'users': []})

    def test_validate_enforces_one_user_required_overlength(self):
        users = self.users + [{'username': 'joe'}]
        with self.assertRaises(ValidationError):
            self.validator.validate({'type': 'pass', 'users': users})

    def test_validate_enforces_unique_usernames(self):
        users = self.users + [{"username": self.users[0]['username']}]
        with self.assertRaises(ValidationError):
            self.validator.validate({'type': 'user', 'users': users})

    def test_validate_enforces_password_required(self):
        with self.assertRaises(ValidationError):
            self.validator.validate({'type': 'pass',
                                     'users': [{'username': 'chris'}]})

    def test_validate_enforces_email_required(self):
        with self.assertRaises(ValidationError):
            self.validator.validate({'type': 'otp_email',
                                     'users': [{'username': 'chris'}]})

    def test_validate_enforces_phone_required(self):
        with self.assertRaises(ValidationError):
            self.validator.validate({'type': 'otp_phone',
                                     'users': [{'username': 'chris'}]})

    def test_validate_enforces_no_pass(self):
        with self.assertRaises(ValidationError):
            self.validator.validate({'type': 'otp_phone',
                                     'users': [{'phone': '+15551234567',
                                                'password': 'plain:password'}]
                                     })
