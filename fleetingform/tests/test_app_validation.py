import unittest

from django.test import TestCase

from fleetingform.template_helpers import FleetingAppParamsValidator
from fleetingform.errors import FleetingValidationError

class BugAppValidatorTestCase(unittest.TestCase):

    def test_bug_one(self):
        """This app param failed to validate after going live... lame."""
        app =   {
                'wiid': 10,
                'steps': {
                    'Reject': "Approved+",
                    'Approve': "Rejected+"
                    },
                'type': 'workflow_approval',
            }

        FleetingAppParamsValidator().validate(app)

class TestAuthValidator(TestCase):

    def setUp(self):
        self.validator = FleetingAppParamsValidator()
        self.app = {
                'key': 'value',
                'key2:': { 'longer_key': 'v' },
                'list': ['a', 1, ],
            }

    def test_empty_params(self):
        self.assertTrue(self.validator.validate({}))

    def test_valid_params(self):
        self.assertTrue(self.validator.validate(self.app))

    def test_key_overlength(self):
        long_key = "a" * (self.validator.max_key_length + 1)
        self.app[long_key] = 1
        with self.assertRaises(FleetingValidationError):
                self.validator.validate(self.app)

    def test_key_atlength(self):
        long_key = "a" * self.validator.max_key_length
        self.app[long_key] = 1
        self.assertTrue(self.validator.validate(self.app))

    def test_maxkeys_exceeded(self):
        for i in range(0, self.validator.max_keys):
            self.app['key{}'.format(i)] = i
        with self.assertRaises(FleetingValidationError):
                self.validator.validate(self.app)

    def test_value_overlength(self):
        long_value = "a" * (self.validator.max_value_length + 1)
        self.app['testkey'] = long_value
        with self.assertRaises(FleetingValidationError):
                self.validator.validate(self.app)

    def test_value_atlength(self):
        long_value = "a" * self.validator.max_value_length
        self.app['testkey'] = long_value
        self.assertTrue(self.validator.validate(self.app))

    def test_max_levels(self):
        self.app['nested0'] = {}
        self.app['nested0']['nested1'] = {}
        with self.assertRaises(FleetingValidationError):
                self.validator.validate(self.app)

    def test_max_list_length(self):
        items = [i for i in range(0, self.validator.max_list_length + 1)]
        self.app['items'] = items
        with self.assertRaises(FleetingValidationError):
                self.validator.validate(self.app)
