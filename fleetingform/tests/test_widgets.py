from faker import Factory

from django.test import TestCase

from fleetingform.widgets import BootstrapDateTimePickerInput

faker = Factory.create()


class FleetingWidgetsTestCase(TestCase):

    def test_datetime_picker_sets_context(self):
        widget = BootstrapDateTimePickerInput()
        name = faker.word()
        data_target = "#datetimepicker_{}".format(name)
        context = widget.get_context(name=name, value='', attrs=None)
        self.assertEqual(context['widget']['name'], name)
        self.assertEqual(context['widget']['attrs']['data-target'],
                         data_target)
