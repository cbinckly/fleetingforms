from django.test import TestCase
from django.conf import settings

from fleetingform.models import FleetingAuth
from fleetingform.errors import (FleetingFormCompleteError,
                                 FleetingOTPRetriesExceeded,
                                 FleetingAuthOTPError,
                                 FleetingValidationError,
                                 FleetingDeletionError,
                                 )
from fleetingform.factories import (FleetingFormFactory,
                                    FleetingAuthFactory, )


class FleetingErrorsTestCase(TestCase):

    def setUp(self):
        self.fform = FleetingFormFactory()
        self.password = 'password'
        self.username = 'username'
        auth = FleetingAuthFactory(form=self.fform,
                                   type=FleetingAuth.AUTH_TYPE_USER_PASS)
        user = auth.users.first()
        user.username = self.username
        user.password = settings.FLEETING_DEFAULT_HASHER.hash(self.password)
        user.save()

    def test_otp_retries_exceeded_args(self):
        with self.assertRaises(TypeError):
            FleetingOTPRetriesExceeded()
        FleetingOTPRetriesExceeded(self.fform.auth.users.first())

    def test_otp_retries_exceeded_str(self):
        e = FleetingOTPRetriesExceeded(self.fform.auth.users.first())
        self.assertIn(self.username, str(e))
        self.assertIn(self.fform.code, str(e))

    def test_otp_error_args(self):
        # No args
        with self.assertRaises(TypeError):
            FleetingAuthOTPError()

        # Only a user
        with self.assertRaises(TypeError):
            FleetingAuthOTPError(self.fform.auth.users.first())

        # Only an auth type
        with self.assertRaises(TypeError):
            FleetingAuthOTPError('generic')

        FleetingAuthOTPError(self.fform.auth.users.first(), 'generic')

    def test_otp_error_str(self):
        e = FleetingAuthOTPError(self.fform.auth.users.first(), 'generic')
        self.assertIn(self.username, str(e))

    def test_validation_error_args(self):
        field = 'field'
        code = 'object-over-length'
        # No args
        with self.assertRaises(TypeError):
            FleetingValidationError()

        # Only a user
        with self.assertRaises(TypeError):
            FleetingValidationError(field)

        # Only an auth type
        with self.assertRaises(TypeError):
            FleetingValidationError(code)

        FleetingValidationError(field, code)

    def test_validation_error_str(self):
        """The FleetingValidationError has a proper string representation.

        This requires more extensive testing for each message type. Too
        trivial for now.
        """
        field = 'field'
        code = 'object-over-length'
        _max = 10

        e = FleetingValidationError(field, code, max_length=_max)
        self.assertIn('maximum', str(e))
        self.assertIn(str(_max), str(e))

        unknown_code = 'bad-format-test-code'

        e = FleetingValidationError(field, unknown_code)
        self.assertIn(field, str(e))
        self.assertIn(unknown_code, str(e))

    def test_save_fails_when_complete(self):
        self.fform = FleetingFormFactory()
        self.fform.complete({})
        with self.assertRaises(FleetingFormCompleteError):
            self.fform.save()

    def test_deletion_error_str(self):
        klass = 'FleetingFakeClass'
        pk = 12
        e = FleetingDeletionError(klass, pk)
        self.assertIn(str(pk), str(e))
        self.assertIn(klass, str(e))
