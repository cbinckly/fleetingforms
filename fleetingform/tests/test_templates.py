from django.urls import reverse
from django.test import TestCase

from fleetingform.lib.form_generator import generate_fleeting_form_class_for
from fleetingform.factories import (FleetingFormFactory,
                                    FleetingAuthFactory,
                                    FleetingTemplateFactory,
                                    FleetingTemplateFormControlFactory,
                                    FleetingChoiceFactory)


class GenericTemplateTestCase(TestCase):

    def setUp(self):
        self.fform = FleetingFormFactory()
        self.fform.auth = FleetingAuthFactory(type='none')
        self.fform.template = FleetingTemplateFactory(
                                type='generic', form_controls=[], )
        self.fform.save()
        self.form_class = generate_fleeting_form_class_for({})
        self.response = self.client.get(
                reverse('form-display', args=[self.fform.code]))

    def test_template(self):
        self.assertTemplateUsed(self.response,
                                self.fform.template.html_template)

    def test_has_form(self):
        self.assertEqual("GeneratedFleetingForm", self.form_class.__name__)

    def test_html(self):
        self.assertContains(self.response, '<form')

    def test_input_text_accepts_text(self):
        template = self.fform.template
        template.form_controls.add(FleetingTemplateFormControlFactory(
                                        name='text',
                                        type='text'))
        form_class = generate_fleeting_form_class_for(
                                template.form_controls.all())
        form = form_class({"text": "This is some text."})
        self.assertTrue(form.is_valid())

    def test_input_textarea_accepts_text(self):
        template = self.fform.template
        template.form_controls.add(FleetingTemplateFormControlFactory(
                                        name='textarea',
                                        type='textarea'))
        form_class = generate_fleeting_form_class_for(
                                template.form_controls.all())
        form = form_class({"textarea": "This is some text."})
        self.assertTrue(form.is_valid())

    def test_input_integer_accepts_integer(self):
        template = self.fform.template
        template.form_controls.add(FleetingTemplateFormControlFactory(
                                        name='integer',
                                        type='integer'))
        form_class = generate_fleeting_form_class_for(
                                template.form_controls.all())
        form = form_class({"integer": 1})
        self.assertTrue(form.is_valid())

    def test_input_float_accepts_float(self):
        template = self.fform.template
        template.form_controls.add(FleetingTemplateFormControlFactory(
                                        name='float',
                                        type='float'))
        form_class = generate_fleeting_form_class_for(
                                template.form_controls.all())
        form = form_class({"float": 1.0})
        self.assertTrue(form.is_valid())

    def test_input_date_accepts_date(self):
        template = self.fform.template
        template.form_controls.add(FleetingTemplateFormControlFactory(
                                        name='date',
                                        type='date'))
        form_class = generate_fleeting_form_class_for(
                                template.form_controls.all())
        form = form_class({"date": "12/01/2019"})
        self.assertTrue(form.is_valid())

    def test_input_datetime_accepts_datetime(self):
        template = self.fform.template
        template.form_controls.add(FleetingTemplateFormControlFactory(
                                        name='datetime',
                                        type='datetime'))
        form_class = generate_fleeting_form_class_for(
                                template.form_controls.all())
        form = form_class({"datetime": "12/01/2019 14:12"})
        self.assertTrue(form.is_valid())

    def test_input_time_accepts_time(self):
        template = self.fform.template
        template.form_controls.add(FleetingTemplateFormControlFactory(
                                        name='time',
                                        type='time'))
        form_class = generate_fleeting_form_class_for(
                                template.form_controls.all())
        form = form_class({"time": "2:12 PM"})
        self.assertTrue(form.is_valid())

    def test_input_url_accepts_url(self):
        template = self.fform.template
        template.form_controls.add(FleetingTemplateFormControlFactory(
                                        name='url',
                                        type='url'))
        form_class = generate_fleeting_form_class_for(
                                template.form_controls.all())
        form = form_class({"url": "https://fleetingforms.io"})
        self.assertTrue(form.is_valid())

    def test_input_decimal_accepts_decimal(self):
        template = self.fform.template
        template.form_controls.add(FleetingTemplateFormControlFactory(
                                        name='decimal',
                                        type='decimal'))
        form_class = generate_fleeting_form_class_for(
                                template.form_controls.all())
        form = form_class({"decimal": 15.8})
        self.assertTrue(form.is_valid())

    def test_input_email_accepts_email(self):
        template = self.fform.template
        template.form_controls.add(FleetingTemplateFormControlFactory(
                                        name='email',
                                        type='email'))
        form_class = generate_fleeting_form_class_for(
                                template.form_controls.all())
        form = form_class({"email": "rosie@fleetingforms.io"})
        self.assertTrue(form.is_valid())

    def test_input_choice_accepts_choice(self):
        template = self.fform.template
        fc = FleetingTemplateFormControlFactory(
                                        name='choice',
                                        type='choice',
                                        choices=[])
        fc.choices.add(FleetingChoiceFactory(text="text", value="1"))
        template.form_controls.add(fc)
        form_class = generate_fleeting_form_class_for(
                                template.form_controls.all())
        form = form_class({"choice": "1"})
        self.assertTrue(form.is_valid())

    def test_disabled_form_control_returns_initial_value(self):
        template = self.fform.template
        fc = FleetingTemplateFormControlFactory(
                                        name='test',
                                        type='text',
                                        initial='initial',
                                        disabled=True)
        template.form_controls.add(fc)
        form_class = generate_fleeting_form_class_for(
                                template.form_controls.all())
        form = form_class({"test": "edited"})
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['test'], 'initial')
