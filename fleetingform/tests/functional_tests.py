import time
import unittest
import requests
import datetime
from contextlib import contextmanager

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from django.test import LiveServerTestCase
from django.urls import reverse
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from rest_framework.test import APIClient

from fleetingform.factories import (FleetingNamespaceFactory,
                                    FleetingUserFactory,
                                    FleetingFormFactory,
                                    FleetingAuthFactory, )
from fleetingform.models import FleetingNamespace, FleetingUser, FleetingForm

class FleetingFormTest(LiveServerTestCase):
    port = 8001

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.namespace = FleetingNamespaceFactory()
        print(f'Created Namespace {self.namespace} {self.namespace.token}')
        self.requests_client = APIClient(**self.headers())

    def tearDown(self):
        self.browser.quit()

    def headers(self):
        return { settings.FLEETING_TOKEN_HEADER: str(self.namespace.token) }

    @contextmanager
    def wait_for_page_load(self, timeout=30):
        old_page = self.browser.find_element_by_tag_name('html')
        yield
        WebDriverWait(self.browser, timeout).until(
            EC.staleness_of(old_page)
        )

    def wait_for_element_by_name(self, name, timeout=5):
        element_present = EC.presence_of_element_located((By.NAME, name))
        WebDriverWait(self.browser, timeout).until(element_present)

    def test_can_login_to_form_with_username(self):
        _username = 'joe'
        _auth_title = 'Unlock'
        _form_title = 'Form for Joe'

        fform = FleetingFormFactory()
        fform.template.title = _form_title
        fform.template.save()
        fform.auth = FleetingAuthFactory(type='user',
                                         title=_auth_title, )
        user = fform.auth.users.create(username=_username)
        fform.auth.save()
        fform.save()

        # Joe gets a link in his email and follows it.
        form_url = self.live_server_url + reverse('form-display',
                                                  args=[fform.code, ])
        response = self.browser.get(form_url)

        self.assertEqual(_auth_title, self.browser.title)

        # He inputs his username
        username_field = self.browser.find_element_by_name('username')
        username_field.send_keys('joe')

        # He is redirected to the form.
        with self.wait_for_page_load(timeout=5):
            username_field.send_keys(Keys.ENTER)

        self.wait_for_element_by_name(
                            fform.template.form_controls.first().name)

        form_field = self.browser.find_element_by_name(
                            fform.template.form_controls.first().name)

    def test_can_create_and_consume_a_form(self):
        # Sarah has an important approval that needs to be done remotely
        # She needs it to display a specific title and render special content.
        title = 'Approve More Thneeds!'
        content = '''
            Onceler,

            We are running out of Thneeds.  They have been flying off the shelf
            after an article in the international news!

            To ramp up production we need you to tell us what you need.
            '''

        # She needs to create the following form
        form_controls = [
                    {
                        'name': 'text',
                        'type': 'text',
                        'label': 'Truffala tufts',
                    },
                    {
                        'name': 'textarea',
                        'type': 'textarea',
                        'label': 'Barbaloot Suits',
                    },
                    {
                        'name': 'integer',
                        'type': 'integer',
                        'label': 'How many barbaloot suits?',
                    },
                    {
                        'name': 'decimal',
                        'type': 'decimal',
                        'label': 'How high are the tufts?',
                    },
                    {
                        'name': 'float',
                        'type': 'float',
                        'label': 'How many litres in the pond?',
                    },
                    {
                        'name': 'url',
                        'type': 'url',
                        'label': 'Where to send the barbaloots?',
                    },
                    {
                        'name': 'email',
                        'type': 'email',
                        'label': 'Who to email about the humming fish?',
                    },
                    {
                        'name': 'datetime',
                        'type': 'datetime',
                        'label': 'When to add new capacity?',
                    },
                    {
                        'name': 'date',
                        'type': 'date',
                        'label': 'Date Field',
                    },
                    {
                        'name': 'time',
                        'type': 'time',
                        'label': 'Time Field',
                    },
                    {
                        'name': 'boolean',
                        'type': 'boolean',
                        'label': 'Boolean Field',
                    },
                ]
        actions = [{ 'label': 'Approve' }, { 'label': 'Reject' }]
        # So she packages them into a payload without any auth.
        payload = {'template':
                    {   'title': title,
                        'content': content,
                        'form_controls': form_controls,
                        'actions': actions
                    }
                  }


        # And sends them to fleetingforms.
        post_url = self.live_server_url + reverse('form-list')
        response = self.requests_client.post(post_url,
                                 payload,
                                 format='json')
        form = response.json()

        # She sends the short url to the Onceler.
        form_url = self.live_server_url + reverse('form-display',
                                                  args=[form['code'], ])

        # And later checks the status of the form
        get_url = self.live_server_url + reverse('form-retrieve',
                                                 args=[form['id']])
        response = self.requests_client.get(get_url)

        # But finds that it has not been completed, or opened.
        self.assertEqual(form['status'], 'created')
        self.assertIsNone(form['opened_on'])
        self.assertFalse(form['result'])

        # Later, the Onceler visits the page.
        self.browser.get(form_url)

        # And diligently completes it correctly.
        self.assertEqual(title, self.browser.title)

        _text = 'Hello'
        _integer = 5
        _float = 5.5
        _decimal = '15.85555'
        _datetime = datetime.datetime(2020, 5, 17, 22, 50,
                                      tzinfo=datetime.timezone.utc)
        _date = datetime.date(2020, 5, 17)
        _time = datetime.time(22, 50)
        _url = 'https://fleetingforms.io'
        _email = 'rosie@fleetingforms.io'

        text_field = self.browser.find_element_by_name('text')
        text_field.send_keys(_text)
        textarea_field = self.browser.find_element_by_name('textarea')
        textarea_field.send_keys(_text)
        integer_field = self.browser.find_element_by_name('integer')
        integer_field.send_keys(str(_integer))
        float_field = self.browser.find_element_by_name('float')
        float_field.send_keys(str(_float))
        decimal_field = self.browser.find_element_by_name('decimal')
        decimal_field.send_keys(_decimal)
        boolean_field = self.browser.find_element_by_name('boolean')
        boolean_field.click()
        datetime_field = self.browser.find_element_by_name('datetime')
        datetime_field.send_keys(_datetime.strftime('%d/%m/%Y %H:%M'))
        date_field = self.browser.find_element_by_name('date')
        date_field.send_keys(_date.strftime('%d/%m/%Y'))
        time_field = self.browser.find_element_by_name('time')
        time_field.send_keys(_time.strftime('%H:%M'))
        url_field = self.browser.find_element_by_name('url')
        url_field.send_keys(_url)
        email_field = self.browser.find_element_by_name('email')
        email_field.send_keys(_email)
        with self.wait_for_page_load(timeout=5):
            email_field.send_keys(Keys.ENTER)

        self.wait_for_element_by_name(
                form['template']['form_controls'][0]['name'])

        # Later, Sarah checks again.
        response = self.requests_client.get(get_url)
        form = response.json()

        # And finds the form completed.
        self.assertEqual(form['status'], 'completed')

        # She extracts all the information she needs.
        r = form['result']
        self.assertEqual(r['text'], _text)
        self.assertEqual(r['textarea'], _text)
        self.assertEqual(r['integer'], _integer)
        self.assertEqual(r['float'], _float)
        self.assertEqual(r['decimal'], _decimal)
        self.assertEqual(r['datetime'],
                         _datetime.isoformat(timespec='seconds'))
        self.assertEqual(r['date'], _date.isoformat())
        self.assertEqual(r['time'], _time.isoformat())
        self.assertEqual(r['url'], _url)
        self.assertEqual(r['email'], _email)
        self.assertEqual(r['boolean'], True)

        # Confirms the timestamps
        self.assertIsNotNone(form['completed_on'])
        self.assertIsNotNone(form['opened_on'])

        # And deleted the form
        response = self.requests_client.delete(get_url)

        with self.assertRaises(ObjectDoesNotExist):
            fform = FleetingForm.objects.get(pk=form['id'])

    def test_can_create_and_consume_a_form_error_paths(self):
        # Sarah has an important approval that needs to be done remotely
        # She needs it to display a specific title and render special content.
        title = 'Approve More Thneeds!'
        content = '''
            Onceler,

            We are running out of Thneeds.  They have been flying off the shelf
            after an article in the international news!

            To ramp up production we need you to tell us what you need.
            '''

        # She needs to create the following form
        form_controls = [
                    {
                        'name': 'text',
                        'type': 'oopstext',
                        'label': 'Truffala tufts',
                    },
                ]

        # She thinks the default action will do
        actions = []#{ 'label': 'Go' }]

        # So she packages them into a payload without any auth.
        # But forgets the title
        payload = {'template':
                    {   # 'title': None,
                        'content': content,
                        'form_controls': form_controls,
                        'actions': actions
                    }
                  }

        # She decides on username and password authentication
        payload['auth'] = {
                    'type': 'username_and_password',
                }

        # And sends them to fleetingforms.
        # But forgets her token
        bad_client = APIClient()
        # and uses the wrong url
        post_url = self.live_server_url + reverse('form-list')
        wrong_post_url = self.live_server_url + reverse('form-retrieve',
                                                        args=[0])
        response = bad_client.post(wrong_post_url,
                                 payload,
                                 format='json')
        response_json = response.json()
        self.assertEqual(response.status_code, 403)
        self.assertIn('detail', response_json)
        self.assertEqual(response_json.get('detail'),
                         'Authentication credentials were not provided.')

        # She tries again with headers
        response = self.requests_client.post(wrong_post_url,
                                 payload,
                                 format='json')
        response_json = response.json()
        self.assertEqual(response.status_code, 405)
        self.assertIn('detail', response_json)
        self.assertEqual(response_json.get('detail'),
                         'Method "POST" not allowed.' )

        # She tries again with headers and URL
        response = self.requests_client.post(post_url,
                                 payload,
                                 format='json')
        response_json = response.json()
        self.assertEqual(response.status_code, 400)
        self.assertIn('template', response_json)
        t = response_json.get('template')
        self.assertIn('title', t)
        self.assertIn('required', t['title'][0])
        self.assertIn('form_controls', t)
        self.assertIn('is not a valid choice',
                      t['form_controls'][0]['type'][0])
        self.assertIn('actions', t)
        self.assertIn('At least one action is required', t['actions'][0])

        # She fixes the template up, setting the fields properly
        payload['template']['title'] = title
        payload['template']['actions'] = [{ 'label': 'Go'}]
        payload['template']['form_controls'][0]['type'] = 'text'

        # She tries again with headers and URL
        response = self.requests_client.post(post_url,
                                 payload,
                                 format='json')
        response_json = response.json()
        self.assertEqual(response.status_code, 400)
        self.assertIn('auth', response_json)
        self.assertIn('is not a valid choice',
                      response_json['auth']['type'][0])

        # So she looks at the docs, changes it, and adds the users
        payload['auth']['type'] = 'user_pass'
        payload['auth']['users'] = [{'username': 'joe',
                                     'password': 'password'}]

        # She tries again with headers and URL
        response = self.requests_client.post(post_url,
                                 payload,
                                 format='json')
        response_json = response.json()
        self.assertEqual(response.status_code, 400)
        msg = response_json['auth']['users'][0]['password'][0]
        self.assertIn("plain:", msg)
        self.assertIn("modulo crypto", msg)

        payload['auth']['users'][0]['password'] = 'plain:password'

        # She tries again with headers and URL
        response = self.requests_client.post(post_url,
                                 payload,
                                 format='json')
        form = response.json()
        self.assertEqual(response.status_code, 201)

        # And later checks the status of the form
        get_url = self.live_server_url + reverse('form-retrieve',
                                                 args=[form['id']])
        response = self.requests_client.get(get_url)
        form = response.json()

        # But finds that it has not been completed, or opened.
        self.assertEqual(form['status'], 'created')
        self.assertIsNone(form['opened_on'])
        self.assertFalse(form['result'])

        # And that the password is obscured
        self.assertEqual(form['auth']['users'][0]['password'], "encrypted")

        # Later, the Onceler visits the page.
        form_url = self.live_server_url + reverse('form-display',
                                                  args=[form['code'], ])

        # And is presented with the username and password login form.
        self.browser.get(form_url)

        self.assertEqual(self.browser.title, "Unlock Fleeting Form")
        # And leaves it blank!
        username_field = self.browser.find_element_by_name('username')
        username_field.send_keys(Keys.ENTER)

        # And nothing happens, so he fills them in
        username_field.send_keys('joe')
        password_field = self.browser.find_element_by_name('password')
        password_field.send_keys('badpass')

        with self.wait_for_page_load(timeout=5):
            password_field.send_keys(Keys.ENTER)

        alert = self.browser.find_element_by_css_selector(
                'div.alert ul li')
        self.assertIn('This form requires a valid username and password.',
                      alert.text)

        # He relents and uses his password.
        password_field = self.browser.find_element_by_name('password')
        password_field.send_keys('password')
        with self.wait_for_page_load(timeout=5):
            password_field.send_keys(Keys.ENTER)

        self.wait_for_element_by_name(
                form["template"]["form_controls"][0]["name"])

        # And submits it blank again!
        go_button = self.browser.find_element_by_name('action')
        go_button.click()

        # Nothing happens, so he adds some text
        text_field = self.browser.find_element_by_name('text')
        with self.wait_for_page_load(timeout=5):
            text_field.send_keys("Tufts!" + Keys.ENTER)

        self.wait_for_element_by_name(
                form["template"]["form_controls"][0]["name"])

        alert = self.browser.find_element_by_css_selector(
                'div.alert')
        self.assertIn('This form has been completed',
                      alert.text)

        # in the meantime, a new form is created by another user
        fform2 = FleetingFormFactory()
        fform2.save()

        get_url = self.live_server_url + reverse('form-retrieve',
                                                 args=[fform2.id])
        response = self.requests_client.get(get_url)
        self.assertEqual(response.status_code, 404)

        get_url = self.live_server_url + reverse('form-retrieve',
                                                 args=[form['id']])
        response = self.requests_client.get(get_url)
        self.assertEqual(response.status_code, 200)
        form = response.json()

        self.assertEqual(form['status'], 'completed')
        self.assertEqual(form['result']['text'], "Tufts!")

        # And deleted the form
        response = self.requests_client.delete(get_url)

        with self.assertRaises(ObjectDoesNotExist):
            fform = FleetingForm.objects.get(pk=form['id'])
