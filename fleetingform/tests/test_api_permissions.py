from django.test import TestCase, RequestFactory

from fleetingform.permissions import FleetingFormTokenPermission
from fleetingform.factories import (FleetingFormFactory,
                                    FleetingNamespaceFactory)


class FleetingAPIPermissionsTestCase(TestCase):

    def setUp(self):
        self.namespace = FleetingNamespaceFactory()
        self.authorized_form = FleetingFormFactory(namespace=self.namespace)
        self.unauthorized_form = FleetingFormFactory()
        self.permission = FleetingFormTokenPermission()
        self.request = RequestFactory()

    def test_has_permission_no_namespace_in_request(self):
        self.assertFalse(self.permission.has_permission(self.request, {}))

    def test_has_permission_none_namespace_in_request(self):
        self.request.namespace = None
        self.assertFalse(self.permission.has_permission(self.request, {}))

    def test_has_permission_namespace_in_request(self):
        self.request.namespace = self.namespace
        self.assertTrue(self.permission.has_permission(self.request, {}))

    def test_has_object_permission_no_namespace_in_request(self):
        self.assertFalse(self.permission.has_object_permission(
                            self.request, {}, self.authorized_form))

    def test_has_object_permission_none_namespace_in_request(self):
        self.request.namespace = None
        self.assertFalse(self.permission.has_object_permission(
                            self.request, {}, self.authorized_form))

    def test_has_object_permission_namespace_in_request(self):
        self.request.namespace = self.namespace
        self.assertTrue(self.permission.has_object_permission(
                            self.request, {}, self.authorized_form))

    def test_has_object_permission_wrong_namespace_in_request(self):
        self.request.namespace = self.namespace
        self.assertFalse(self.permission.has_object_permission(
                            self.request, {}, self.unauthorized_form))
