import random
from unittest import skip
from datetime import timedelta

from django.test import TestCase, RequestFactory
from django.conf import settings
from django.utils import timezone

from rest_framework.serializers import ValidationError
from rest_framework.authtoken.models import Token

from fleetingform.serializers import (FleetingNamespaceSerializer,
                                      FleetingFormControlSerializer,
                                      FleetingTemplateSerializer,
                                      FleetingUserSerializer,
                                      FleetingAuthSerializer,
                                      FleetingFormSerializer, )

from fleetingform.models import FleetingAuth
from fleetingform.factories import (FleetingFormFactory,
                                    FleetingAuthFactory,
                                    FleetingChoiceFactory,
                                    FleetingTemplateFormControlFactory,
                                    FleetingUserFactory,
                                    FleetingNamespaceFactory)


class FleetingFormControlSerializerTestCase(TestCase):

    def setUp(self):
        self.form = FleetingFormFactory()

    def test_form_control_validates_empty_choices(self):
        form_control = {
                'name': 'choices',
                'type': 'choice',
                }
        s = FleetingFormControlSerializer(data=form_control)
        with self.assertRaises(ValidationError):
            s.is_valid(raise_exception=True)

    def test_form_control_validates_choices_for_other_types(self):
        form_control = {
                'name': 'notchoices',
                'type': 'text',
                'choices': [{'value': 1, 'text': 'One'}]
                }
        s = FleetingFormControlSerializer(data=form_control)
        with self.assertRaises(ValidationError):
            s.is_valid(raise_exception=True)

    def test_form_control_created_with_valid_choices(self):
        form_control = {
                'name': 'choices',
                'type': 'choice',
                'choices': [{'value': 1, 'text': 'One'}]
                }
        s = FleetingFormControlSerializer(data=form_control)
        self.assertTrue(s.is_valid(raise_exception=True))

    def test_form_control_created_without_choices(self):
        form_control = {
                'name': 'field',
                'type': 'text',
                }
        s = FleetingFormControlSerializer(data=form_control)
        self.assertTrue(s.is_valid(raise_exception=True))

    def test_form_control_representation_includes_choices(self):
        form_control = FleetingTemplateFormControlFactory(
                type='choice', choices=[FleetingChoiceFactory()
                                        for i in range(0, 3)])

        s = FleetingFormControlSerializer(instance=form_control)
        self.assertTrue('choices' in s.to_representation(form_control))

    def test_form_control_representation_includes_empty_choices(self):
        form_control = FleetingTemplateFormControlFactory(
                type='choice', choices=[])

        s = FleetingFormControlSerializer(instance=form_control)
        self.assertTrue('choices' in s.to_representation(form_control))

    def test_form_control_representation_excludes_choices(self):
        form_control = FleetingTemplateFormControlFactory(
                type='text', choices=[FleetingChoiceFactory(text=str(i))
                                      for i in range(0, 3)])
        s = FleetingFormControlSerializer(instance=form_control)
        self.assertTrue('choices' not in s.to_representation(form_control))

    def test_form_control_representation_excludes_empty_choices(self):
        form_control = FleetingTemplateFormControlFactory(
                type='text', choices=[])
        s = FleetingFormControlSerializer(instance=form_control)
        self.assertTrue('choices' not in s.to_representation(form_control))

    def test_form_control_accepts_disabled_attribute(self):
        form_control = FleetingTemplateFormControlFactory(
                type='text', disabled=True)
        s = FleetingFormControlSerializer(instance=form_control)
        self.assertTrue('disabled' in s.to_representation(form_control))
        self.assertTrue(s.to_representation(form_control)['disabled'], False)

    def test_form_control_decimal_invalid_params(self):
        form_control = {'type': 'decimal',
                        'name': 'decimal',
                        'params': {'badparam': 1}}
        s = FleetingFormControlSerializer(data=form_control)
        self.assertTrue(not s.is_valid())


class FleetingTemplateSerializerTestCase(TestCase):

    def setUp(self):
        self.form = FleetingFormFactory()

    def test_template_validates_empty_actions(self):
        template = {
                'title': 'Template Title',
                'type': 'generic',
                'actions': []
                }
        s = FleetingTemplateSerializer(data=template)
        with self.assertRaises(ValidationError):
            s.is_valid(raise_exception=True)

    def test_generic_template_rejects_params(self):
        template = {
                'title': 'Template Title',
                'type': 'generic',
                'actions': [{'label': 'One'}],
                'params': {'param1': 'fail'}
                }
        s = FleetingTemplateSerializer(data=template)
        with self.assertRaises(ValidationError):
            s.is_valid(raise_exception=True)

    def test_template_rejects_overlength_cleaned_titles(self):
        template = {
                'title': '&&&&&&' * 20,
                'type': 'generic',
                'actions': [{'label': 'One'}],
                }
        s = FleetingTemplateSerializer(data=template)
        with self.assertRaises(ValidationError):
            s.is_valid(raise_exception=True)


class FleetingUserSerializerTestCase(TestCase):

    def setUp(self):
        self.form = FleetingFormFactory()
        self.serializer = FleetingUserSerializer

    def test_validate_password_noops_on_empty(self):
        self.assertEqual(self.serializer().validate_password(""), "")

    def test_validate_password_accepts_basic_plain(self):
        self.assertTrue(self.serializer().validate_password("plain:password"))

    def test_validate_password_accepts_valid_hash(self):
        self.assertTrue(self.serializer().validate_password(
                            settings.FLEETING_DEFAULT_HASHER.hash("password")))

    def test_validate_password_rejects_empty_plain(self):
        with self.assertRaises(ValidationError):
            self.serializer().validate_password("plain:")

    def test_validate_password_rejects_short_plain(self):
        with self.assertRaises(ValidationError):
            self.serializer().validate_password("plain:")

    def test_validate_password_rejects_bad_format_1(self):
        with self.assertRaises(ValidationError):
            self.serializer().validate_password("abcdefg")

    def test_validate_password_rejects_bad_format_2(self):
        with self.assertRaises(ValidationError):
            self.serializer().validate_password("!!!!!!!")

    def test_validate_password_rejects_bad_format_3(self):
        with self.assertRaises(ValidationError):
            self.serializer().validate_password("$algo$param$salt$hash$")

    def test_validate_password_rejects_bad_algo(self):
        with self.assertRaises(ValidationError):
            self.serializer().validate_password("$algo$param$salt$hash")

    def test_user_representation_replaces_password(self):
        user = FleetingUserFactory(password="password")
        auth = FleetingAuthFactory(type='user_pass')
        user.auth = auth
        user.save()
        self.assertEqual(
                self.serializer().to_representation(user)["password"],
                "encrypted")

    def test_user_representation_removes_password(self):
        user = FleetingUserFactory(password="password")
        auth = FleetingAuthFactory(type='user')
        user.auth = auth
        user.save()
        self.assertTrue("password" not in
                        self.serializer().to_representation(user))

    def test_phone_validates_without_lookup(self):
        settings.TWILIO_LOOKUP_VALIDATION = False
        phone = "+15005550006"
        self.assertEqual(self.serializer().validate_phone(phone), phone)

    @skip
    def test_phone_validates_with_lookup_valid(self):
        """Twilio does not allow testing of lookups with test creds.
        Production credentials are not generally available here, so the test
        skipped by default. Use Production keys in your env run this tests."""
        settings.TWILIO_LOOKUP_VALIDATION = True
        # Magic phone number (https://www.twilio.com/docs/iam/test-credentials)
        phone = "+15005550006"
        self.assertEqual(self.serializer().validate_phone(phone), phone)

    @skip
    def test_phone_raises_validation_error_when_lookup_raises(self):
        """Twilio does not allow testing of lookups with test creds.
        Production credentials are not generally available here, so the test
        skipped by default. Use Production keys in your env run this tests."""
        settings.TWILIO_LOOKUP_VALIDATION = True
        # Magic phone number (https://www.twilio.com/docs/iam/test-credentials)
        phone = "+15005550001"
        with self.assertRaises(ValidationError):
            self.serializer().validate_phone(phone)


class FleetingAuthSerializerTestCase(TestCase):

    def setUp(self):
        self.fform = FleetingFormFactory()
        self.template = self.fform.template
        self.auth = self.fform.auth
        self.serializer = FleetingAuthSerializer

    def test_validates_valid_auth_params(self):
        auth = {
                    'type': 'user_pass',
                    'users': [
                        {"username": "chris", "password": "plain:password"}
                    ]
                }
        s = self.serializer(data=auth)
        self.assertEqual(s.validate(auth), auth)

    def test_validates_invalid_auth_params(self):
        auth = {
                    'type': 'user_pass',
                    'users': [
                        {"password": "plain:password"}
                    ]
                }
        s = self.serializer(data=auth)
        with self.assertRaises(ValidationError):
            s.validate(auth)

    def test_type_none_create(self):
        auth = {'type': 'none'}
        self.assertIsInstance(self.serializer().create(auth),
                              FleetingAuth)

    def test_type_user_create(self):
        auth = {'type': 'user', 'users': [{"username": "chris"}], }
        s = self.serializer(data=auth)
        self.assertTrue(s.is_valid())
        self.assertIsInstance(s.save(), FleetingAuth)

    def test_title_defaults_on_create(self):
        auth = {'type': 'user', 'users': [{"username": "chris"}], }
        s = self.serializer(data=auth)
        self.assertTrue(s.is_valid())
        a = s.save()
        self.assertEqual(a.title, FleetingAuth.AUTH_DEFAULT_TITLE)

    def test_content_defaults_on_create(self):
        auth = {'type': 'user', 'users': [{"username": "chris"}], }
        s = self.serializer(data=auth)
        self.assertTrue(s.is_valid())
        a = s.save()
        self.assertEqual(a.content, FleetingAuth.AUTH_DEFAULT_CONTENT)

    def test_content_doesnt_default_on_create_if_set(self):
        content = "My Great Content"
        auth = {'type': 'user', 'users': [{"username": "chris"}],
                'content': content}
        s = self.serializer(data=auth)
        self.assertTrue(s.is_valid())
        a = s.save()
        self.assertEqual(a.content, content)

    def test_title_doesnt_default_on_create_if_set(self):
        title = "My Great Title"
        auth = {'type': 'user', 'users': [{"username": "chris"}],
                'title': title}
        s = self.serializer(data=auth)
        self.assertTrue(s.is_valid())
        a = s.save()
        self.assertEqual(a.title, title)

    def test_username_doesnt_default_on_create_if_set(self):
        label = "My Great Label"
        auth = {'type': 'user', 'users': [{"username": "chris"}],
                'form_controls': [
                        {"name": "username", "label": label, "type": "text"}
                    ]}
        s = self.serializer(data=auth)
        self.assertTrue(s.is_valid(raise_exception=True))
        a = s.save()
        self.assertEqual(a.form_controls.first().label, label)

    def test_username_defaults_on_create_if_set(self):
        auth = {'type': 'user', 'users': [{"username": "chris"}], }
        s = self.serializer(data=auth)
        self.assertTrue(s.is_valid(raise_exception=True))
        a = s.save()
        self.assertEqual(a.form_controls.first().name, 'username')

    def test_password_defaults_on_create_if_set(self):
        auth = {'type': 'pass', 'users': [{"password": "plain:chris"}], }
        s = self.serializer(data=auth)
        self.assertTrue(s.is_valid(raise_exception=True))
        a = s.save()
        self.assertEqual(a.form_controls.first().name, 'password')

    def test_password_doesnt_default_on_create_if_set(self):
        label = "My Great Label"
        auth = {'type': 'pass', 'users': [{"password": "plain:chris"}],
                'form_controls': [
                        {"name": "password", "label": label, "type": "text"}
                    ]}
        s = self.serializer(data=auth)
        self.assertTrue(s.is_valid(raise_exception=True))
        a = s.save()
        self.assertEqual(a.form_controls.first().label, label)

    def test_to_representation_removes_empty(self):
        auth = FleetingAuthFactory(type='none', title='')
        self.assertTrue(hasattr(auth, 'title'))
        print(auth.title)
        self.assertTrue(auth.title == '')
        self.assertTrue('title' not in
                        self.serializer().to_representation(auth))


class FleetingFormSerializerTestCase(TestCase):

    def setUp(self):
        self.fform = FleetingFormFactory()
        self.template = self.fform.template
        self.auth = self.fform.auth
        self.serializer = FleetingFormSerializer
        self.minimal_form = {
                "template": {
                    "type": "generic",
                    "title": "One Button",
                    "actions": [{"label": "Push Me"}]
                    },
            }

    def test_doesnt_set_existing_namespace(self):
        namespace = FleetingNamespaceFactory()
        fake_namespace = FleetingNamespaceFactory()
        request = RequestFactory()
        request.namespace = fake_namespace
        fs = self.serializer(context={'request': request})
        self.assertEqual(fs.validate_namespace(namespace),
                         namespace)

    def test_injects_namespace(self):
        namespace = FleetingNamespaceFactory()
        request = RequestFactory()
        request.namespace = namespace
        fs = self.serializer(context={'request': request})
        self.assertEqual(fs.validate_namespace(None),
                         namespace)

    def test_injects_none_on_no_namespace(self):
        request = RequestFactory()
        fs = self.serializer(context={'request': request})
        self.assertEqual(fs.validate_namespace(None), None)

    def test_injects_none_on_no_request(self):
        fs = self.serializer()
        self.assertEqual(fs.validate_namespace(None), None)

    def test_injects_auth_none(self):
        fs = self.serializer()
        self.assertEqual(fs.validate_auth(self.minimal_form.get('auth')),
                         {'type': FleetingAuth.AUTH_TYPE_NONE})

    def test_ignores_auth_set(self):
        fs = self.serializer()
        self.assertEqual(fs.validate_auth({'type': 'user'}), {'type': 'user'})

    def test_validates_app_params(self):
        self.assertEqual(self.serializer().validate_app({}), {})

    def test_validates_invalid_app_params(self):
        long_key = "a" * 33
        with self.assertRaises(ValidationError):
            self.serializer().validate_app({long_key: 1})

    def test_doesnt_overwrite_user_provided_auth(self):
        self.minimal_form['auth'] = {'type': 'user',
                                     'users': [{"username": "chris"}]}
        s = self.serializer(data=self.minimal_form)
        self.assertTrue(s.is_valid())
        self.assertEqual(s.save().auth.type, 'user')

    def test_allow_html_form_content(self):
        content = '<a href="https://google.com">google</a>'
        self.minimal_form['template']['content'] = content
        s = self.serializer(data=self.minimal_form)
        self.assertTrue(s.is_valid())
        self.assertEqual(content, s.validated_data['template']['content'])

    def test_disallow_invalid_tags(self):
        content = 'Content <script>console.log("hello")</script>'
        self.minimal_form['template']['content'] = content
        s = self.serializer(data=self.minimal_form)
        self.assertTrue(s.is_valid())
        self.assertNotEqual(content, s.validated_data['template']['content'])

    def test_allow_html_tables_in_form_content(self):
        content = ('<table><thead><tr><th>A</th><th>B</th></tr></thead>'
                   '<tbody><tr><td>A</td><td>B</td></tr></tbody>'
                   '<tfoot></tfoot></table>')
        self.minimal_form['template']['content'] = content
        s = self.serializer(data=self.minimal_form)
        self.assertTrue(s.is_valid())
        self.assertEqual(content, s.validated_data['template']['content'])

    def test_allow_html_whitespace_in_form_content(self):
        content = '<p>This is a para</p><br>With break.'
        self.minimal_form['template']['content'] = content
        s = self.serializer(data=self.minimal_form)
        self.assertTrue(s.is_valid())
        self.assertEqual(content, s.validated_data['template']['content'])


class FleetingNamespaceSerializerTestCase(TestCase):

    def test_get_usage(self):
        namespace = FleetingNamespaceFactory()
        serializer = FleetingNamespaceSerializer(instance=namespace)
        representation = serializer.to_representation(namespace)
        this_month = timezone.now()
        last_month = this_month.replace(day=1) - timedelta(days=1)
        self.assertIn('usage', representation)
        self.assertIn(this_month.strftime("%Y-%m"), representation['usage'])
        self.assertIn(last_month.strftime("%Y-%m"), representation['usage'])

    def test_get_usage_counts(self):
        namespace = FleetingNamespaceFactory()
        this_month = timezone.now()
        last_month = this_month.replace(day=1) - timedelta(days=1)
        last_month_count = random.randint(3, 10)
        this_month_count = random.randint(3, 10)
        for i in range(0, last_month_count):
            fform = FleetingFormFactory(namespace=namespace)
            audit = fform.audit_entry
            fform.created_on = last_month - timedelta(days=i)
            audit.created_on = last_month - timedelta(days=i)
            fform.save()
            audit.save()
        for i in range(0, this_month_count):
            fform = FleetingFormFactory(namespace=namespace)

        serializer = FleetingNamespaceSerializer(instance=namespace)
        representation = serializer.to_representation(namespace)
        self.assertEqual(representation['usage'][this_month.strftime("%Y-%m")],
                         this_month_count)
        self.assertEqual(representation['usage'][last_month.strftime("%Y-%m")],
                         last_month_count)

    def test_hides_token_in_representation(self):
        namespace = FleetingNamespaceFactory()
        request = RequestFactory()
        request.namespace = namespace
        request.user = namespace.user
        request.auth = str(namespace.token)
        s = FleetingNamespaceSerializer(context={'request': request})
        self.assertNotIn('token', s.to_representation(namespace))

    def test_shows_token_in_representation_for_user(self):
        namespace = FleetingNamespaceFactory()
        request = RequestFactory()
        request.namespace = namespace
        request.user = namespace.user
        request.auth = Token.objects.get(user=namespace.user).key
        s = FleetingNamespaceSerializer(context={'request': request})
        self.assertIn('token', s.to_representation(namespace))
