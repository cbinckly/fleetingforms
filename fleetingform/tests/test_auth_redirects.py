from unittest import skip
from django.urls import reverse
from django.test import TestCase
from django.conf import settings
from django.contrib.auth.models import User
from rest_framework.test import APIRequestFactory
from rest_framework import status

from fleetingform.models import (FleetingNamespace,
                                 FleetingForm,
                                 FleetingTemplate,
                                 FleetingAction,
                                 FleetingAuth,)
from fleetingform.views import (FleetingFormListCreateView, )
from fleetingform.factories import (FleetingFormFactory,
                                    FleetingAuthFactory, )

class AuthFormRedirectTestCase(TestCase):

    def setUp(self):
        self.fform = FleetingFormFactory()

    def test_auth_user_redirects(self):
         self.fform = self._form_with_auth_type(
                                FleetingAuth.AUTH_TYPE_USER)
         self.url = reverse('form-display', kwargs={ 'code': self.fform.code })
         response = self.client.get(self.url)
         self.assertRedirects(response, '/{}/login'.format(self.fform.code))

    def test_auth_pass_redirects(self):
         self.fform = self._form_with_auth_type(
                                FleetingAuth.AUTH_TYPE_PASS)
         self.url = reverse('form-display', kwargs={ 'code': self.fform.code })
         response = self.client.get(self.url)
         self.assertRedirects(response, '/{}/login'.format(self.fform.code))

    def test_auth_user_pass_redirects(self):
         self.fform = self._form_with_auth_type(
                                FleetingAuth.AUTH_TYPE_USER_PASS)
         self.url = reverse('form-display', kwargs={ 'code': self.fform.code })
         response = self.client.get(self.url)
         self.assertRedirects(response, '/{}/login'.format(self.fform.code))

    def test_auth_pass_code_email_redirects(self):
         self.fform = self._form_with_auth_type(
                                FleetingAuth.AUTH_TYPE_PASS_OTP_EMAIL)
         self.url = reverse('form-display', kwargs={ 'code': self.fform.code })
         response = self.client.get(self.url)
         self.assertRedirects(response, '/{}/login'.format(self.fform.code))

    def test_auth_pass_code_phone_redirects(self):
         self.fform = self._form_with_auth_type(
                                FleetingAuth.AUTH_TYPE_PASS_OTP_PHONE)
         self.url = reverse('form-display', kwargs={ 'code': self.fform.code })
         response = self.client.get(self.url)
         self.assertRedirects(response, '/{}/login'.format(self.fform.code))

    def test_auth_user_pass_code_email_redirects(self):
         self.fform = self._form_with_auth_type(
                                FleetingAuth.AUTH_TYPE_USER_PASS_OTP_EMAIL)
         self.url = reverse('form-display', kwargs={ 'code': self.fform.code })
         response = self.client.get(self.url)
         self.assertRedirects(response, '/{}/login'.format(self.fform.code))

    def test_auth_pass_code_phone_redirects(self):
         self.fform = self._form_with_auth_type(
                                FleetingAuth.AUTH_TYPE_USER_PASS_OTP_PHONE)
         self.url = reverse('form-display', kwargs={ 'code': self.fform.code })
         response = self.client.get(self.url)
         self.assertRedirects(response, '/{}/login'.format(self.fform.code))

    def test_auth_none_doesnt_redirect(self):
         self.fform = self._form_with_auth_type(FleetingAuth.AUTH_TYPE_NONE)
         self.url = reverse('form-display', kwargs={ 'code': self.fform.code })
         response = self.client.get(self.url)
         self.assertEqual(response.status_code, 200)

    def _form_with_auth_type(self, auth_type):
        form = FleetingFormFactory()
        auth = FleetingAuthFactory(type=auth_type)
        auth.form = form
        form.auth = auth
        auth.save()
        form.save()
        return form








