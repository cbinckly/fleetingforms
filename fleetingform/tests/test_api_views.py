import uuid
import base64
import datetime
from unittest import mock
from decimal import Decimal

from django.urls import reverse
from django.test import TestCase, RequestFactory
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.sessions.middleware import SessionMiddleware

from rest_framework.test import APITestCase, APIClient, APIRequestFactory
from rest_framework.authtoken.models import Token
from rest_framework import status

from fleetingform.errors import (FleetingOTPRetriesExceeded,
                                 FleetingAuthOTPError,
                                 FleetingFormGenerationError, )
from fleetingform.models import (FleetingForm,
                                 FleetingNamespace,
                                 FleetingTemplate,
                                 FleetingAction,
                                 FleetingAuth,
                                 FleetingAuditEntry, )
from fleetingform.forms import (UserAuthenticationForm,
                                PasswordAuthenticationForm,
                                UserPasswordAuthenticationForm,)
from fleetingform.factories import (FleetingNamespaceFactory,
                                    FleetingFormFactory,
                                    FleetingAuthFactory,
                                    UserFactory, )
from fleetingform.views import (send_otp_and_set_messages,
                                FleetingFormListCreateView,
                                FleetingFormLoginView,
                                UserFormView,
                                FleetingOTPResetRedirectView, )
from fleetingform.lib import auth_token_field, auth_username_field


class FleetingFormAPIMinimalCreateTestCase(TestCase):

    def setUp(self):
        user, _ = User.objects.get_or_create(username="test",
                                             email="test@email.com")
        self.namespace = FleetingNamespaceFactory(user=user)
        self.headers = {settings.FLEETING_TOKEN_HEADER: self.namespace.token}
        self.minimal_form = {
                "template": {
                    "type": "generic",
                    "title": "One Button",
                    "actions": [{"label": "Push Me"}]
                    },
            }
        factory = APIRequestFactory()
        self.request = factory.post(reverse('form-list'),
                                    self.minimal_form,
                                    **self.headers)
        self.request.namespace = self.namespace

    def test_minimal_form_create(self):
        """The minimum argument set is correct."""
        response = FleetingFormListCreateView.as_view()(self.request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(FleetingForm.objects.count(), 1)
        self.assertEqual(FleetingTemplate.objects.count(), 1)
        self.assertEqual(FleetingAction.objects.count(), 1)
        self.assertEqual(FleetingAuth.objects.count(), 1)
        self.assertEqual(FleetingAuditEntry.objects.count(), 1)

    def test_minimal_template(self):
        """The template fields are input correctly."""
        FleetingFormListCreateView.as_view()(self.request)
        fform = FleetingForm.objects.get()
        self.assertEqual(fform.template.title, 'One Button')
        self.assertEqual(fform.template.content, '')
        self.assertEqual(fform.template.type, 'generic')
        self.assertEqual(fform.template.actions.first().label, 'Push Me')

    def test_minimal_auth(self):
        """The auth object is auto-created and set to type none."""
        FleetingFormListCreateView.as_view()(self.request)
        fform = FleetingForm.objects.get()
        self.assertIsNotNone(fform.auth)
        self.assertEqual(fform.auth.type, FleetingAuth.AUTH_TYPE_NONE)

    def test_minimal_app(self):
        """The app object default is an empty dict."""
        FleetingFormListCreateView.as_view()(self.request)
        fform = FleetingForm.objects.get()
        self.assertIsInstance(fform.app, dict)

    def test_minimal_code_created(self):
        """The code has been assigned."""
        FleetingFormListCreateView.as_view()(self.request)
        fform = FleetingForm.objects.get()
        self.assertIsNotNone(fform.code)

    def test_minimal_audit_entry_created(self):
        """The code has been assigned."""
        FleetingFormListCreateView.as_view()(self.request)
        fform = FleetingForm.objects.get()
        self.assertIsNotNone(fform.code)
        ae = FleetingAuditEntry.objects.get(code=fform.code)
        self.assertEqual(ae.status, fform.status)
        self.assertIsNone(ae.opened_on)
        self.assertIsNone(ae.completed_on)

    def test_create_denied_when_over_hard_limit(self):
        self.namespace.hard_limit = 1
        response = FleetingFormListCreateView.as_view()(self.request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = FleetingFormListCreateView.as_view()(self.request)
        self.assertEqual(response.status_code,
                         status.HTTP_429_TOO_MANY_REQUESTS)


class FleetingFormAPITemplateCreateTestCase(TestCase):

    def setUp(self):
        user, _ = User.objects.get_or_create(username="test",
                                             email="test@email.com")
        self.namespace = FleetingNamespaceFactory(user=user)
        self.headers = {settings.FLEETING_TOKEN_HEADER: self.namespace.token}
        self.minimal_form = {
                "template": {
                    "type": "generic",
                    "title": "One Button",
                    "actions": [{"label": "Push Me"}]
                    },
            }

    def assertObjectCounts(self, class_counts):
        for _class, count in class_counts.items():
            self.assertEqual(_class.objects.count(), count)

    def test_template_defaults_type(self):
        """The minimum argument set is correct."""
        factory = APIRequestFactory()
        self.minimal_form['template'].pop('type')
        self.request = factory.post(reverse('form-list'),
                                    self.minimal_form,
                                    **self.headers)
        self.request.namespace = self.namespace
        response = FleetingFormListCreateView.as_view()(self.request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(FleetingForm.objects.first().template.type, 'generic')
        self.assertObjectCounts({FleetingForm: 1,
                                 FleetingTemplate: 1,
                                 FleetingAuth: 1})

    def test_template_requires_title(self):
        """The minimum argument set is correct."""
        factory = APIRequestFactory()
        self.minimal_form['template'].pop('title')
        self.request = factory.post(reverse('form-list'),
                                    self.minimal_form,
                                    **self.headers)
        self.request.namespace = self.namespace
        response = FleetingFormListCreateView.as_view()(self.request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertObjectCounts({FleetingForm: 0,
                                 FleetingTemplate: 0,
                                 FleetingAction: 0,
                                 FleetingAuth: 0})

    def test_template_requires_actions(self):
        """The minimum argument set is correct."""
        factory = APIRequestFactory()
        self.minimal_form['template'].pop('actions')
        self.request = factory.post(reverse('form-list'),
                                    self.minimal_form,
                                    **self.headers)
        self.request.namespace = self.namespace
        response = FleetingFormListCreateView.as_view()(self.request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertObjectCounts({FleetingForm: 0,
                                 FleetingTemplate: 0,
                                 FleetingAction: 0,
                                 FleetingAuth: 0})

    def test_template_action_requires_label(self):
        """Actions need labels."""
        factory = APIRequestFactory()
        self.minimal_form['template']['actions'] = [{}]
        self.request = factory.post(reverse('form-list'),
                                    self.minimal_form,
                                    **self.headers)
        self.request.namespace = self.namespace
        response = FleetingFormListCreateView.as_view()(self.request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertObjectCounts({FleetingForm: 0,
                                 FleetingTemplate: 0,
                                 FleetingAction: 0,

                                 FleetingAuth: 0})


class FleetingFormAPIListTestCase(APITestCase):

    def setUp(self):
        self.user, _ = User.objects.get_or_create(username="test",
                                                  email="test@email.com")
        self.namespace = FleetingNamespaceFactory(user=self.user)
        self.headers = {settings.FLEETING_TOKEN_HEADER: self.namespace.token}
        self.client = APIClient(**self.headers)

    def test_list_queryset_limited_to_user(self):
        fform1 = FleetingFormFactory(namespace=self.namespace)
        fform2 = FleetingFormFactory()
        response = self.client.get(reverse('form-list'), **self.headers)
        ids = [f['id'] for f in response.data]
        self.assertNotIn(fform2.id, ids)
        self.assertIn(fform1.id, ids)


class FleetingFormAPIRetrieveTestCase(APITestCase):

    def setUp(self):
        self.user, _ = User.objects.get_or_create(username="test",
                                                  email="test@email.com")
        self.namespace = FleetingNamespaceFactory(user=self.user)
        self.headers = {settings.FLEETING_TOKEN_HEADER: self.namespace.token}
        self.client = APIClient(**self.headers)

    def test_retrieve(self):
        fform1 = FleetingFormFactory(namespace=self.namespace)
        response = self.client.get(reverse('form-retrieve', args=[fform1.id]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_queryset_limited_to_user(self):
        fform2 = FleetingFormFactory()
        response = self.client.get(reverse('form-retrieve', args=[fform2.id]))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class FleetingFormLoginUnitViewCase(TestCase):

    def setUp(self):
        self.fform = FleetingFormFactory()

    def test_authenticated(self):
        self.fform.auth_token = uuid.uuid4()

        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {}
        request.session[
                auth_token_field(self.fform)] = str(self.fform.auth_token)
        self.assertTrue(view._authenticated(self.fform, request))

    def test_authenticated_fails_without_token(self):
        self.fform.auth_token = None

        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {}
        request.session[
                auth_token_field(self.fform)] = str(self.fform.auth_token)
        self.assertFalse(view._authenticated(self.fform, request))

    def test_authenticated_fails_unmatching_token(self):
        self.fform.auth_token = uuid.uuid4()

        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {}
        request.session[auth_token_field(self.fform)] = str(uuid.uuid4)
        self.assertFalse(view._authenticated(self.fform, request))

    def test_get_current_user_returns_first_user_if_not_in_session(self):
        self.fform.auth = FleetingAuthFactory(type='user')
        user = self.fform.auth.users.first()
        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {}
        self.assertEqual(view._get_current_user(self.fform, request),
                         user)

    def test_get_current_user_returns_user_if_in_session(self):
        self.fform.auth = FleetingAuth(type='user_pass')
        self.fform.auth.save()
        user2 = self.fform.auth.users.create(username='joe')
        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {}
        request.session[auth_username_field(self.fform)] = user2.username
        self.assertEqual(view._get_current_user(self.fform, request),
                         user2)

    def test_get_current_user_returns_first_if_user_not_required(self):
        self.fform.auth = FleetingAuthFactory(type='none')
        for user in self.fform.auth.users.all():
            user.save()
        user = self.fform.auth.users.create(username='joe')
        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {}
        self.assertEqual(view._get_current_user(self.fform, request),
                         self.fform.auth.users.first())
        self.assertNotEqual(view._get_current_user(self.fform, request),
                            user)

    def test_get_template_vars_without_otp(self):
        self.fform.auth = FleetingAuthFactory(type='none')
        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {}
        self.assertEqual(view._get_template_vars(self.fform, request),
                         {'title': self.fform.auth.title,
                          'content': self.fform.auth.content,
                          'action': self.fform.auth.action})

    def test_get_template_vars_with_otp(self):
        self.fform.auth = FleetingAuthFactory(type='otp_email')
        user = self.fform.auth.users.first()
        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {}
        remaining = settings.FLEETING_OTP_MAX_ATTEMPTS + 1 - user.attempts
        self.assertEqual(view._get_template_vars(self.fform, request),
                         {'title': self.fform.auth.title,
                          'content': self.fform.auth.content,
                          'action': self.fform.auth.action,
                          'fform_code': self.fform.code,
                          'otp': True,
                          'remaining_codes': remaining,
                          'otp_destination': user.otp_contact_obscured})

    def test_get_form_for_auth_type_user(self):
        self.fform.auth = FleetingAuthFactory(type='user')
        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {}
        self.assertIsInstance(view._get_form(self.fform, request)[0],
                              UserAuthenticationForm,)

    def test_get_form_for_auth_type_user_pass(self):
        self.fform.auth = FleetingAuthFactory(type='user_pass')
        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {}
        self.assertIsInstance(view._get_form(self.fform, request)[0],
                              UserPasswordAuthenticationForm)

    def test_get_form_for_auth_type_pass(self):
        self.fform.auth = FleetingAuthFactory(type='pass')
        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {}
        self.assertIsInstance(view._get_form(self.fform, request)[0],
                              PasswordAuthenticationForm)

    def test_get_form_for_auth_type_otp_email(self):
        self.fform.auth = FleetingAuthFactory(type='otp_email')
        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {}
        self.assertIsInstance(view._get_form(self.fform, request)[0],
                              PasswordAuthenticationForm)

    def test_get_form_for_auth_type_otp_phone(self):
        self.fform.auth = FleetingAuthFactory(type='otp_phone')
        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {}
        self.assertIsInstance(view._get_form(self.fform, request)[0],
                              PasswordAuthenticationForm)

    @mock.patch('fleetingform.models.send_otp_sms')
    def test_get_form_for_auth_type_otp_phone_sets_password(self, mock_send):
        self.fform.auth = FleetingAuthFactory(type='otp_phone')
        self.fform.auth.save()
        user = self.fform.auth.users.first()
        user.password = ''
        user.save()
        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {}
        self.assertIsInstance(view._get_form(self.fform, request)[0],
                              PasswordAuthenticationForm)
        user.refresh_from_db()
        self.assertTrue(user.password)

    @mock.patch('fleetingform.models.send_otp_sms')
    def test_get_form_for_auth_type_otp_phone_sends_otp(self, mock_send):
        self.fform.auth = FleetingAuthFactory(type='otp_phone')
        self.fform.auth.save()
        user = self.fform.auth.users.first()
        user.password = ""
        user.save()
        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {}
        self.assertIsInstance(view._get_form(self.fform, request)[0],
                              PasswordAuthenticationForm)
        mock_send.assert_called_once()

    @mock.patch('fleetingform.models.send_otp_email')
    def test_get_form_for_auth_type_otp_email_sets_password(self, mock_send):
        self.fform.auth = FleetingAuthFactory(type='otp_email')
        self.fform.auth.save()
        user = self.fform.auth.users.first()
        user.password = ''
        user.save()
        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {}
        self.assertIsInstance(view._get_form(self.fform, request)[0],
                              PasswordAuthenticationForm)
        user.refresh_from_db()
        self.assertTrue(user.password)

    @mock.patch('fleetingform.models.send_otp_email')
    def test_get_form_for_auth_type_otp_email_sends_otp(self, mock_send):
        self.fform.auth = FleetingAuthFactory(type='otp_email')
        self.fform.auth.save()
        user = self.fform.auth.users.first()
        user.password = ""
        user.save()
        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {}
        self.assertIsInstance(view._get_form(self.fform, request)[0],
                              PasswordAuthenticationForm)
        mock_send.assert_called_once()

    def test_get_form_for_auth_type_user_otp_shows_user_form(self):
        self.fform.auth = FleetingAuthFactory(type='user_otp_email')
        self.fform.auth.save()
        user = self.fform.auth.users.first()
        user.password = ""
        user.save()
        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {}
        self.assertIsInstance(view._get_form(self.fform, request)[0],
                              UserAuthenticationForm)

    def test_get_form_for_auth_type_user_otp_shows_password_form(self):
        self.fform.auth = FleetingAuthFactory(type='user_otp_email')
        self.fform.auth.save()
        user = self.fform.auth.users.first()
        user.password = ""
        user.save()
        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {auth_username_field(self.fform): user.username}
        self.assertIsInstance(view._get_form(self.fform, request)[0],
                              PasswordAuthenticationForm)

    @mock.patch('fleetingform.models.send_otp_email')
    def test_get_form_for_auth_type_user_otp_email_keeps_pass(self, mock_send):
        self.fform.auth = FleetingAuthFactory(type='user_otp_email')
        self.fform.auth.save()

        password = "password"
        user = self.fform.auth.users.first()
        user.password = password
        user.save()

        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))
        request.session = {auth_username_field(self.fform): user.username}
        self.assertIsInstance(view._get_form(self.fform, request)[0],
                              PasswordAuthenticationForm)

        # Did the form try to send an otp? it shouldn't have.
        user.refresh_from_db()
        self.assertEqual(user.password, password)
        mock_send.assert_not_called()

    def test_get_form_for_auth_type_fake_fails(self):
        self.fform.auth = FleetingAuthFactory(type='fake')
        self.fform.auth.save()
        user = self.fform.auth.users.first()

        view = FleetingFormLoginView()
        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))

        request.session = {auth_username_field(self.fform): user.username}
        with self.assertRaises(FleetingFormGenerationError):
            view._get_form(self.fform, request)


class SendOTPAndSetMessagesTestCase(TestCase):

    def setUp(self):
        self.fform = FleetingFormFactory()

    def test_send_otp_and_set_messages(self):
        self.fform.auth = FleetingAuthFactory(type='otp_email')
        self.fform.auth.save()
        user = self.fform.auth.users.first()

        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))

        request.session = {auth_username_field(self.fform): user.username}
        user.generate_and_send_otp = mock.MagicMock(
                                            method='generate_and_send_otp',)
        self.assertTrue(send_otp_and_set_messages(user, self.fform, request))
        user.generate_and_send_otp.assert_called()

    def test_send_otp_and_set_messages_retries_exceeded(self):
        self.fform.auth = FleetingAuthFactory(type='otp_email')
        self.fform.auth.save()
        user = self.fform.auth.users.first()

        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))

        for mw in [SessionMiddleware(), MessageMiddleware()]:
            mw.process_request(request)
            request.session.save()
        session = request.session
        session[auth_username_field(self.fform)] = user.username
        session.save()

        user.generate_and_send_otp = mock.MagicMock(
                                method='generate_and_send_otp',
                                side_effect=FleetingOTPRetriesExceeded(user))
        self.assertFalse(send_otp_and_set_messages(user, self.fform, request))
        user.generate_and_send_otp.assert_called_once()

    def test_send_otp_and_set_messages_otp_error(self):
        self.fform.auth = FleetingAuthFactory(type='otp_email')
        self.fform.auth.save()
        user = self.fform.auth.users.first()

        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))

        for mw in [SessionMiddleware(), MessageMiddleware()]:
            mw.process_request(request)
            request.session.save()
        session = request.session
        session[auth_username_field(self.fform)] = user.username
        session.save()

        user.generate_and_send_otp = mock.MagicMock(
                                method='generate_and_send_otp',
                                side_effect=FleetingAuthOTPError(
                                            user.username, 'otp_email'))
        self.assertFalse(send_otp_and_set_messages(user, self.fform, request))
        user.generate_and_send_otp.assert_called_once()

    def test_send_otp_and_set_messages_exception(self):
        self.fform.auth = FleetingAuthFactory(type='otp_email')
        self.fform.auth.save()
        user = self.fform.auth.users.first()

        request = RequestFactory().get(reverse('form-login',
                                               args=[self.fform.code]))

        for mw in [SessionMiddleware(), MessageMiddleware()]:
            mw.process_request(request)
            request.session.save()
        session = request.session
        session[auth_username_field(self.fform)] = user.username
        session.save()

        user.generate_and_send_otp = mock.MagicMock(
                                method='generate_and_send_otp',
                                side_effect=Exception)
        self.assertFalse(send_otp_and_set_messages(user, self.fform, request))
        user.generate_and_send_otp.assert_called_once()


class FleetingFormLoginViewCase(TestCase):

    def setUp(self):
        self.fform = FleetingFormFactory()

    def test_get_renders_form(self):
        self.fform.auth = FleetingAuthFactory(type='user')
        self.fform.auth.save()
        self.fform.save()
        response = self.client.get(reverse(
                        'form-login', args=[self.fform.code]))

        self.assertIsInstance(response.context.get('form'),
                              UserAuthenticationForm)

        self.assertTrue(b"<form" in response.content)

    def test_get_redirects_if_auth_not_required(self):
        self.fform.auth = FleetingAuthFactory(type='none')
        self.fform.auth.save()
        self.fform.save()
        response = self.client.get(reverse(
                        'form-login', args=[self.fform.code]))
        self.assertRedirects(response,
                             reverse('form-display', args=[self.fform.code, ]))

    def test_get_redirects_if_authenticated(self):
        self.fform.auth = FleetingAuthFactory(type='none')
        self.fform.auth.save()
        self.fform.save()
        session = self.client.session
        session[auth_token_field(self.fform)] = str(self.fform.auth_token)
        response = self.client.get(reverse(
                        'form-login', args=[self.fform.code]))
        self.assertRedirects(response,
                             reverse('form-display', args=[self.fform.code, ]))

    def test_post_renders_form_on_error(self):
        self.fform.auth = FleetingAuthFactory(type='user')
        self.fform.auth.save()
        self.fform.save()
        response = self.client.post(reverse(
                        'form-login', args=[self.fform.code]),
                        data={'username': 'bogus'})

        self.assertIsInstance(response.context.get('form'),
                              UserAuthenticationForm)

        self.assertTrue(b"<form" in response.content)

    def test_post_redirects_on_success(self):
        self.fform.auth = FleetingAuthFactory(type='user')
        self.fform.auth.save()
        self.fform.save()
        user = self.fform.auth.users.create(username='joe')
        response = self.client.post(reverse(
                        'form-login', args=[self.fform.code]),
                        data={'username': user.username})
        self.assertRedirects(response,
                             reverse('form-display', args=[self.fform.code, ]))

    def test_post_redirects_if_auth_not_required(self):
        self.fform.auth = FleetingAuthFactory(type='none')
        self.fform.auth.save()
        self.fform.save()
        response = self.client.get(reverse(
                        'form-login', args=[self.fform.code]))
        self.assertRedirects(response,
                             reverse('form-display', args=[self.fform.code, ]))

    def test_post_redirects_if_authenticated(self):
        self.fform.auth = FleetingAuthFactory(type='none')
        self.fform.auth.save()
        self.fform.save()
        session = self.client.session
        session[auth_token_field(self.fform)] = str(self.fform.auth_token)
        response = self.client.get(reverse(
                        'form-login', args=[self.fform.code]))
        self.assertRedirects(response,
                             reverse('form-display', args=[self.fform.code, ]))

    def test_post_renders_password_if_username_two_stage(self):
        self.fform.auth = FleetingAuthFactory(type='user_otp_email')
        self.fform.auth.save()
        self.fform.save()
        user = self.fform.auth.users.create(username='joe',
                                            email='joe@test.com')
        response = self.client.post(reverse(
                        'form-login', args=[self.fform.code]),
                        data={'username': user.username})
        self.assertEqual(self.client.session.get(
                                auth_username_field(self.fform)),
                         user.username)
        self.assertIsInstance(response.context.get('form'),
                              PasswordAuthenticationForm)

    def test_post_redirects_two_stage(self):
        self.fform.auth = FleetingAuthFactory(type='user_otp_email')
        self.fform.auth.save()
        self.fform.save()
        password = settings.FLEETING_DEFAULT_HASHER.hash('password')
        user = self.fform.auth.users.create(username='joe',
                                            email='joe@test.com',
                                            password=password)
        session = self.client.session
        session[auth_username_field(self.fform)] = user.username
        session.save()
        response = self.client.post(reverse(
                        'form-login', args=[self.fform.code]),
                        data={'password': 'password'})
        self.assertEqual(self.client.session.get(
                                auth_token_field(self.fform)),
                         self.fform.auth_token)
        self.assertRedirects(response,
                             reverse('form-display', args=[self.fform.code, ]))


class UserFormViewTestCase(TestCase):

    def setUp(self):
        self.fform = FleetingFormFactory()

    def test_get_renders_form(self):
        self.fform.auth = FleetingAuthFactory(type='none')
        self.fform.template.form_controls.create(
                name="test_form_control", type="integer")
        self.fform.auth.save()
        self.fform.save()
        response = self.client.get(reverse(
                        'form-display', args=[self.fform.code]))
        self.assertTrue(b'<input type="number"' in response.content)
        self.assertTrue(b'name="test_form_control"' in response.content)

    def test_get_redirects_if_not_authenticated(self):
        self.fform.auth = FleetingAuthFactory(type='user')
        self.fform.auth.save()
        self.fform.save()
        response = self.client.get(reverse(
                        'form-display', args=[self.fform.code]))
        self.assertRedirects(response,
                             reverse('form-login', args=[self.fform.code, ]))

    def test_get_renders_if_authenticated(self):
        self.fform.auth = FleetingAuthFactory(type='user')
        self.fform.template.form_controls.create(
                name="test_form_control", type="integer")
        self.fform.auth.save()
        self.fform.save()
        session = self.client.session
        session[auth_token_field(self.fform)] = self.fform.auth_token
        session.save()
        response = self.client.get(reverse(
                        'form-display', args=[self.fform.code]))
        self.assertTrue(b'<input type="number"' in response.content)
        self.assertTrue(b'name="test_form_control"' in response.content)

    def test_get_renders_readonly_if_completed(self):
        self.fform.auth = FleetingAuthFactory(type='user')
        self.fform.template.form_controls.create(
                name="test_form_control", type="integer")
        self.fform.auth.save()
        self.fform.status = 'completed'
        self.fform.save()
        session = self.client.session
        session[auth_token_field(self.fform)] = self.fform.auth_token
        session.save()
        response = self.client.get(reverse(
                        'form-display', args=[self.fform.code]))
        self.assertTrue(b'<fieldset disabled="disabled">' in response.content)

    @mock.patch('fleetingform.views.generate_fleeting_form_class_for',
                side_effect=FleetingFormGenerationError)
    def test_get_sets_error_on_exception(self, mock_generate):
        self.fform.auth = FleetingAuthFactory(type='none')
        self.fform.template.form_controls.create(
                name="test_form_control", type="integer")
        self.fform.auth.save()
        self.fform.save()
        try:
            self.client.get(reverse(
                            'form-display', args=[self.fform.code]))
        except Exception:
            pass
        self.fform.refresh_from_db()
        self.assertEqual(self.fform.status, 'error')

    def test_post_renders_form_on_success(self):
        self.fform.auth = FleetingAuthFactory(type='none')
        self.fform.auth.save()
        self.fform.template.form_controls.all().delete()
        self.fform.template.form_controls.create(
                name="test_form_control", type="integer")
        self.fform.save()
        response = self.client.post(reverse(
                        'form-display', args=[self.fform.code]),
                        data={'test_form_control': 1})

        self.assertTrue(response.context.get('completed'))
        self.assertTrue(b"<form" in response.content)

    def test_post_renders_form_on_error(self):
        self.fform.auth = FleetingAuthFactory(type='none')
        self.fform.auth.save()
        self.fform.template.form_controls.all().delete()
        self.fform.template.form_controls.create(
                name="test_form_control", type="integer")
        self.fform.save()
        response = self.client.post(reverse(
                        'form-display', args=[self.fform.code]),
                        data={'test_form_control': "one hundred"})

        self.assertTrue(b"<form" in response.content)

    @mock.patch('fleetingform.views.generate_fleeting_form_class_for',
                side_effect=FleetingFormGenerationError)
    def test_post_sets_error_on_exception(self, mock_generate):
        self.fform.auth = FleetingAuthFactory(type='none')
        self.fform.auth.save()
        self.fform.save()
        try:
            self.client.post(reverse(
                'form-display', args=[self.fform.code]),
                data={'test_form_control': "one hundred"})
        except Exception:
            pass
        self.fform.refresh_from_db()
        self.assertEqual(self.fform.status, 'error')

    def test_post_redirects_if_not_authenticated(self):
        self.fform.auth = FleetingAuthFactory(type='user')
        self.fform.auth.save()
        self.fform.save()
        response = self.client.post(reverse(
                        'form-display', args=[self.fform.code]))
        self.assertRedirects(response,
                             reverse('form-login', args=[self.fform.code, ]))

    def test_post_renders_if_authenticated(self):
        self.fform.auth = FleetingAuthFactory(type='user')
        self.fform.template.form_controls.all().delete()
        self.fform.template.form_controls.create(
                name="test_form_control", type="integer")
        self.fform.auth.save()
        self.fform.save()
        session = self.client.session
        session[auth_token_field(self.fform)] = self.fform.auth_token
        session.save()
        response = self.client.post(reverse(
                        'form-display', args=[self.fform.code]),
                        data={'test_form_control': 1})
        self.assertTrue(response.context.get('completed'))
        self.assertTrue(b'<input type="number"' in response.content)
        self.assertTrue(b'name="test_form_control"' in response.content)

    def test_jsonify_result_handles_datetime(self):
        view = UserFormView()
        dt = datetime.datetime(2020, 1, 1, 12, 0, 0)
        self.assertEqual(view._jsonify_result({'datetime': dt}),
                         {'datetime': dt.isoformat(timespec='seconds')})

    def test_jsonify_result_handles_date(self):
        view = UserFormView()
        dt = datetime.date(2020, 1, 1)
        self.assertEqual(view._jsonify_result({'date': dt}),
                         {'date': dt.isoformat()})

    def test_jsonify_result_handles_time(self):
        view = UserFormView()
        dt = datetime.time(14, 1, 1)
        self.assertEqual(view._jsonify_result({'time': dt}),
                         {'time': dt.isoformat()})

    def test_jsonify_result_handles_dict(self):
        view = UserFormView()
        dt = datetime.datetime(2020, 1, 1)
        d = {'key': 'hello', 'day': {'date': dt}}
        d_result = {'key': 'hello', 'day': {'date': dt.isoformat()}}
        self.assertEqual(view._jsonify_result(d), d_result)

    def test_jsonify_result_handles_decimal(self):
        view = UserFormView()
        d = "2.345"
        dec = Decimal(d)
        result = {'key': dec}
        dec_result = {'key': d}
        self.assertEqual(view._jsonify_result(result), dec_result)

    def test_decodes_base64_url_params(self):
        self.fform.auth = FleetingAuthFactory(type='none')
        self.fform.template.form_controls.all().delete()
        self.fform.template.form_controls.create(
                name="test_form_control", type="text")
        self.fform.auth.save()
        self.fform.save()
        session = self.client.session
        session[auth_token_field(self.fform)] = self.fform.auth_token
        session.save()
        superword = "hello"
        b64superword = base64.urlsafe_b64encode(superword.encode()).decode()
        response = self.client.get(reverse(
                        'form-display', args=[self.fform.code]),
                        data={'test_form_control': f'b64:{b64superword}'})
        self.assertTrue(f'value="{superword}"'.encode() in response.content)

    def test_decode_fails_gracefully_base64_url_params(self):
        self.fform.auth = FleetingAuthFactory(type='none')
        self.fform.template.form_controls.all().delete()
        self.fform.template.form_controls.create(
                name="test_form_control", type="text")
        self.fform.auth.save()
        self.fform.save()
        session = self.client.session
        session[auth_token_field(self.fform)] = self.fform.auth_token
        session.save()
        response = self.client.get(reverse(
                        'form-display', args=[self.fform.code]),
                        data={'test_form_control': 'b64:FAILS'})
        self.assertTrue('name="test_form_control" value='.encode() not
                        in response.content)

    def test_decode_ignores_normal_url_params(self):
        self.fform.auth = FleetingAuthFactory(type='none')
        self.fform.template.form_controls.all().delete()
        self.fform.template.form_controls.create(
                name="test_form_control", type="text")
        self.fform.auth.save()
        self.fform.save()
        session = self.client.session
        session[auth_token_field(self.fform)] = self.fform.auth_token
        session.save()
        response = self.client.get(reverse(
                        'form-display', args=[self.fform.code]),
                        data={'test_form_control': 'Succeeds'})
        self.assertTrue('name="test_form_control" value="Succeeds"'.encode()
                        in response.content)


class FleetingOTPResetRedirectViewTestCase(TestCase):

    def setUp(self):
        self.fform = FleetingFormFactory()

    def test_redirect_view_pattern_name(self):
        self.assertEqual(FleetingOTPResetRedirectView().pattern_name,
                         'form-login')

    def test_redirect_view_permanent(self):
        self.assertFalse(FleetingOTPResetRedirectView().permanent)

    def test_get_redirect_url_no_otp(self):
        self.fform.auth = FleetingAuthFactory(type='user')
        self.fform.auth.save()
        self.fform.save()

        response = self.client.get(reverse(
                        'otp-resend', args=[self.fform.code]))
        self.assertRedirects(response,
                             reverse('form-login', args=[self.fform.code]))

    def test_get_redirect_url_otp_no_user(self):
        self.fform.auth = FleetingAuthFactory(type='otp_email')
        self.fform.auth.save()
        self.fform.save()

        response = self.client.get(reverse(
                        'otp-resend', args=[self.fform.code]))
        self.assertRedirects(response,
                             reverse('form-login', args=[self.fform.code]))

    def test_get_redirect_url_user_otp_no_user(self):
        self.fform.auth = FleetingAuthFactory(type='user_otp_email')
        self.fform.auth.save()
        self.fform.save()

        response = self.client.get(reverse(
                        'otp-resend', args=[self.fform.code]))
        self.assertRedirects(response,
                             reverse('form-login', args=[self.fform.code]))

    def test_get_redirect_url_user_otp(self):
        self.fform.auth = FleetingAuthFactory(type='user_otp_email')
        self.fform.auth.save()
        user = self.fform.auth.users.first()
        self.fform.save()

        session = self.client.session
        session[auth_username_field(self.fform)] = user.username
        session.save()

        response = self.client.get(reverse(
                        'otp-resend', args=[self.fform.code]))
        self.assertRedirects(response,
                             reverse('form-login', args=[self.fform.code]))

    def test_get_redirect_url_retries_exceeded(self):
        self.fform.auth = FleetingAuthFactory(type='user_otp_email')
        self.fform.auth.save()
        user = self.fform.auth.users.first()
        self.fform.save()

        session = self.client.session
        session[auth_username_field(self.fform)] = user.username
        session.save()

        view = FleetingOTPResetRedirectView()
        view._get_session_user = mock.MagicMock(method='_get_session_user',
                                                return_value=mock.MagicMock)

        response = self.client.get(reverse(
                        'otp-resend', args=[self.fform.code]))
        self.assertRedirects(response,
                             reverse('form-login', args=[self.fform.code]))


class FleetingNamespaceAPIListTestCase(APITestCase):

    def setUp(self):
        self.user, _ = User.objects.get_or_create(username="test",
                                                  email="test@email.com")
        self.namespace = FleetingNamespaceFactory(user=self.user)
        self.headers = {settings.FLEETING_TOKEN_HEADER: self.namespace.token}
        self.client = APIClient()
        token = Token.objects.get(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

    def test_list_queryset_limited_to_user(self):
        namespace = FleetingNamespaceFactory()
        response = self.client.get(reverse('namespace-list'))
        ids = [ns['id'] for ns in response.data]
        self.assertNotIn(namespace.id, ids)
        self.assertIn(self.namespace.id, ids)

    def test_list_queryset_limited_to_namespace(self):
        namespace = FleetingNamespaceFactory()
        response = self.client.get(reverse('namespace-list'), **self.headers)
        self.assertEqual(len(response.data), 1)
        self.assertNotEqual(namespace.id, response.data[0]['id'])
        self.assertEqual(self.namespace.id, response.data[0]['id'])

    def test_list_queryset_fails_for_invalid_namespace(self):
        client = APIClient()
        headers = {settings.FLEETING_TOKEN_HEADER: ""}
        response = client.get(reverse('namespace-list'), **headers)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list_queryset_fails_for_invalid_user(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token faketoken')
        response = client.get(reverse('namespace-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_fails_for_valid_namespace_token(self):
        ns_data = {'subdomain': 'api-test',
                   'support_email': 'support@fakedomain.com'}
        client = APIClient()
        headers = {settings.FLEETING_TOKEN_HEADER: self.namespace.token}
        response = client.post(reverse('namespace-list'),
                               data=ns_data, **headers)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_perform_create_adds_user(self):
        ns_data = {'subdomain': 'api-test',
                   'support_email': 'support@fakedomain.com'}
        response = self.client.post(reverse('namespace-list'), data=ns_data)
        namespace = FleetingNamespace.objects.last()
        rj = response.json()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(namespace.id, rj['id'])
        self.assertEqual(namespace.user, self.user)
        self.assertEqual(self.user.username, rj['user'])

    def test_perform_create_returns_token(self):
        ns_data = {'subdomain': 'api-test',
                   'support_email': 'support@fakedomain.com'}
        response = self.client.post(reverse('namespace-list'), data=ns_data)
        namespace = FleetingNamespace.objects.last()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(str(namespace.token), response.json()['token'])


class FleetingNamespaceAPIRetrieveTestCase(APITestCase):

    def setUp(self):
        self.user, _ = User.objects.get_or_create(username="test",
                                                  email="test@email.com")
        self.namespace = FleetingNamespaceFactory(user=self.user)
        self.headers = {settings.FLEETING_TOKEN_HEADER: self.namespace.token}
        self.client = APIClient()
        token = Token.objects.get(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

    def test_retrieve(self):
        response = self.client.get(reverse('namespace-retrieve',
                                           args=[self.namespace.id]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_queryset_limited_to_user(self):
        namespace = FleetingFormFactory()
        response = self.client.get(reverse('namespace-retrieve',
                                           args=[namespace.id]))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_retrieve_queryset_limited_to_namespace(self):
        namespace = FleetingFormFactory()
        response = self.client.get(reverse('namespace-retrieve',
                                           args=[namespace.id]),
                                   **self.headers)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_retrieve_queryset_by_namespace(self):
        response = self.client.get(reverse('namespace-retrieve',
                                           args=[self.namespace.id]),
                                   **self.headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_without_credentials(self):
        response = APIClient().get(reverse('namespace-retrieve',
                                           args=[self.namespace.id]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_retrieve_wrong_namespace_credentials(self):
        namespace = FleetingNamespaceFactory()
        headers = {settings.FLEETING_TOKEN_HEADER: namespace.token}
        response = APIClient().get(reverse('namespace-retrieve',
                                           args=[self.namespace.id]),
                                   **headers)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_retrieve_invalid_namespace_credentials(self):
        headers = {settings.FLEETING_TOKEN_HEADER: 'fake'}
        response = APIClient().get(reverse('namespace-retrieve',
                                           args=[self.namespace.id]),
                                   **headers)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_retrieve_wrong_user_credentials(self):
        user = UserFactory()
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' +
                           Token.objects.get(user=user).key)
        namespace = FleetingNamespaceFactory()
        headers = {settings.FLEETING_TOKEN_HEADER: namespace.token}
        response = APIClient().get(reverse('namespace-retrieve',
                                           args=[self.namespace.id]),
                                   **headers)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
