from django.test import TestCase
from django.conf import settings

from fleetingform.models import FleetingAuth
from fleetingform.forms import (UserAuthenticationForm,
                                PasswordAuthenticationForm,
                                UserPasswordAuthenticationForm)
from fleetingform.errors import (FleetingFormCompleteError, )
from fleetingform.factories import (FleetingFormFactory,
                                    FleetingAuthFactory, )

class FleetingFormSaveTestCase(TestCase):

    def setUp(self):
        pass

    def test_save_fails_when_complete(self):
        self.fform = FleetingFormFactory()
        self.fform.complete({})
        with self.assertRaises(FleetingFormCompleteError):
            self.fform.save()

class FleetingFormErrorsTestCase(TestCase):

    def test_complete_error_requires_code(self):
        with self.assertRaises(TypeError):
            e = FleetingFormCompleteError()

    def test_string_contains_code(self):
        code = "ABCDEFG"
        e = FleetingFormCompleteError(code)
        self.assertIn(code, str(e))


class AuthenticationFormTestCase(TestCase):

    def setUp(self):
        self.fform = FleetingFormFactory()

    def test_user_auth_form_shows_username_field(self):
        self.fform.auth = FleetingAuthFactory(
                type=FleetingAuth.AUTH_TYPE_USER)
        userform = UserAuthenticationForm(self.fform)
        self.assertTrue('username' in userform.fields)

    def test_pass_auth_form_shows_password_field(self):
        self.fform.auth = FleetingAuthFactory(
                type=FleetingAuth.AUTH_TYPE_PASS)
        userform = PasswordAuthenticationForm(self.fform)
        self.assertTrue('password' in userform.fields)

    def test_userpass_auth_form_shows_fields(self):
        self.fform.auth = FleetingAuthFactory(
                type=FleetingAuth.AUTH_TYPE_USER_PASS)
        userform = UserPasswordAuthenticationForm(self.fform)
        self.assertTrue('password' in userform.fields)
        self.assertTrue('username' in userform.fields)

    def test_username_field_label_customized(self):
        label = 'test_label'
        self.fform.auth = FleetingAuthFactory(
                type=FleetingAuth.AUTH_TYPE_USER)
        self.fform.auth.form_controls.create(name='username', label=label)
        userform = UserAuthenticationForm(self.fform)
        username_field = userform.fields['username']
        self.assertEqual(label, username_field.label)

    def test_username_field_help_text_customized(self):
        help_text = 'helpful_text'
        self.fform.auth = FleetingAuthFactory(
                type=FleetingAuth.AUTH_TYPE_USER)
        self.fform.auth.form_controls.create(name='username',
                                             help_text=help_text)
        userform = UserAuthenticationForm(self.fform)
        username_field = userform.fields['username']
        self.assertEqual(help_text, username_field.help_text)

    def test_password_field_label_customized(self):
        label = 'test_label'
        self.fform.auth = FleetingAuthFactory(
                type=FleetingAuth.AUTH_TYPE_PASS)
        self.fform.auth.form_controls.create(name='password', label=label)
        userform = PasswordAuthenticationForm(self.fform)
        password_field = userform.fields['password']
        self.assertEqual(label, password_field.label)

    def test_password_field_help_text_customized(self):
        help_text = 'helpful_text'
        self.fform.auth = FleetingAuthFactory(
                type=FleetingAuth.AUTH_TYPE_PASS)
        self.fform.auth.form_controls.create(name='password',
                                             help_text=help_text)
        userform = PasswordAuthenticationForm(self.fform)
        password_field = userform.fields['password']
        self.assertEqual(help_text, password_field.help_text)

    def test_user_form_cleans_valid_username(self):
        username = 'testuser'
        self.fform.auth = FleetingAuthFactory(
                type=FleetingAuth.AUTH_TYPE_USER)
        self.fform.auth.users.create(username=username)
        userform = UserAuthenticationForm(self.fform, {'username': username})
        self.assertTrue(userform.is_valid())

    def test_user_form_cleans_invalid_username(self):
        username = 'testuser'
        self.fform.auth = FleetingAuthFactory(
                type=FleetingAuth.AUTH_TYPE_USER)
        self.fform.auth.users.create(username=username)
        userform = UserAuthenticationForm(self.fform, {'username': 'fake'})
        self.assertTrue(not userform.is_valid())

    def test_user_form_cleans_empty_username(self):
        username = 'testuser'
        self.fform.auth = FleetingAuthFactory(
                type=FleetingAuth.AUTH_TYPE_USER)
        self.fform.auth.users.create(username=username)
        userform = UserAuthenticationForm(self.fform, {'username': ''})
        self.assertTrue(not userform.is_valid())

    def test_password_form_cleans_valid_password(self):
        password = "testpass"
        password_hash = settings.FLEETING_DEFAULT_HASHER.hash(password)
        self.fform.auth = FleetingAuthFactory(
                type=FleetingAuth.AUTH_TYPE_PASS)
        user = self.fform.auth.users.first()
        user.username = ''
        user.password = password_hash
        user.phone = ''
        user.save()
        userform = PasswordAuthenticationForm(
                            self.fform,
                            user.username,
                            {'password': password})
        v = userform.is_valid()
        print(userform.errors.as_data())
        self.assertTrue(userform.is_valid())

    def test_password_form_cleans_invalid_password(self):
        password = "testpass"
        password_hash = settings.FLEETING_DEFAULT_HASHER.hash(password)
        self.fform.auth = FleetingAuthFactory(
                type=FleetingAuth.AUTH_TYPE_PASS)
        user = self.fform.auth.users.first()
        user.password = password_hash
        user.save()
        userform = PasswordAuthenticationForm(
                            self.fform, user.username, {'password': 'badpass'})
        self.assertTrue(not userform.is_valid())

    def test_password_form_cleans_empty_password(self):
        password = "testpass"
        password_hash = settings.FLEETING_DEFAULT_HASHER.hash(password)
        self.fform.auth = FleetingAuthFactory(
                type=FleetingAuth.AUTH_TYPE_PASS)
        user = self.fform.auth.users.first()
        userform = PasswordAuthenticationForm(
                            self.fform, user.username, {'password': ''})
        self.assertTrue(not userform.is_valid())

    def test_user_pass_form_cleans_valid(self):
        username = "testuser"
        password = "testpass"
        password_hash = settings.FLEETING_DEFAULT_HASHER.hash(password)
        self.fform.auth = FleetingAuthFactory(
                type=FleetingAuth.AUTH_TYPE_PASS)
        user = self.fform.auth.users.first()
        user.username = username
        user.password = password_hash
        user.save()
        userform = UserPasswordAuthenticationForm(
                            self.fform,
                            {'password': password, 'username': username, })
        self.assertTrue(userform.is_valid())

    def test_user_pass_form_cleans_empty(self):
        username = "testuser"
        password = "testpass"
        password_hash = settings.FLEETING_DEFAULT_HASHER.hash(password)
        self.fform.auth = FleetingAuthFactory(
                type=FleetingAuth.AUTH_TYPE_PASS)
        user = self.fform.auth.users.first()
        user.username = username
        user.password = password_hash
        user.save()
        userform = UserPasswordAuthenticationForm(
                            self.fform,
                            {'password': '', 'username': username, })
        self.assertTrue(not userform.is_valid())
        userform = UserPasswordAuthenticationForm(
                            self.fform,
                            {'password': password, 'username': '', })
        self.assertTrue(not userform.is_valid())
        userform = UserPasswordAuthenticationForm(
                            self.fform,
                            {'password': '', 'username': '', })
        self.assertTrue(not userform.is_valid())

    def test_user_pass_form_cleans_invalid(self):
        username = "testuser"
        password = "testpass"
        password_hash = settings.FLEETING_DEFAULT_HASHER.hash(password)
        self.fform.auth = FleetingAuthFactory(
                type=FleetingAuth.AUTH_TYPE_PASS)
        user = self.fform.auth.users.first()
        user.username = username
        user.password = password_hash
        user.save()
        userform = UserPasswordAuthenticationForm(
                            self.fform,
                            {'password': 'badpass', 'username': username, })
        self.assertTrue(not userform.is_valid())
