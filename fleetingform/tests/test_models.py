import random
from datetime import timedelta
from unittest import mock

from django.db.models.fields import URLField
from django.test import TestCase
from django.test.client import RequestFactory
from django.conf import settings
from django.utils import timezone
from django.core.exceptions import ValidationError, ObjectDoesNotExist

from fleetingform.errors import (FleetingFormCompleteError,
                                 FleetingAuthOTPError,
                                 FleetingOTPRetriesExceeded,
                                 FleetingDeletionError, )
from fleetingform.models import (FleetingNamespace,
                                 FleetingForm,
                                 FleetingTemplate,
                                 FleetingAuth,
                                 FleetingAuditEntry, )
from fleetingform.factories import (FleetingNamespaceFactory,
                                    FleetingWebhookFactory,
                                    FleetingFormFactory,
                                    FleetingAuthFactory,
                                    FleetingChoiceFactory,
                                    FleetingValidationFactory,
                                    FleetingAuditEntryFactory, )
from fleetingform.template_helpers import FleetingTemplateHelper


class FleetingNamespaceTestCase(TestCase):

    def test_namespace_from_request_missing(self):
        factory = RequestFactory()
        request = factory.get('/')
        self.assertIsNone(FleetingNamespace.from_request(request))

    def test_namespace_from_request_empty(self):
        headers = {settings.FLEETING_TOKEN_HEADER: ''}
        factory = RequestFactory(**headers)
        request = factory.get('/')
        self.assertIsNone(FleetingNamespace.from_request(request))

    def test_namespace_from_request_invalid(self):
        headers = {settings.FLEETING_TOKEN_HEADER: 'invalid'}
        factory = RequestFactory(**headers)
        request = factory.get('/')
        self.assertIsNone(FleetingNamespace.from_request(request))

    def test_namespace_from_request_unknown_uuid(self):
        headers = {settings.FLEETING_TOKEN_HEADER:
                   'c429cf55-934f-4846-8cd8-c9157d7aa541'}
        factory = RequestFactory(**headers)
        request = factory.get('/')
        self.assertIsNone(FleetingNamespace.from_request(request))

    def test_namespace_from_request_valid(self):
        ns = FleetingNamespaceFactory()
        headers = {settings.FLEETING_TOKEN_HEADER: ns.token}
        factory = RequestFactory(**headers)
        request = factory.get('/')
        self.assertEqual(ns, FleetingNamespace.from_request(request))

    def test_namespace_logo_field_is_type_url(self):
        logo_field = [f for f in FleetingNamespace._meta.fields
                      if f.name == 'logo']
        logo_field = logo_field[0]
        self.assertTrue(type(logo_field) is URLField)

    def test_namespace_logo_field_rejects_ftp_url(self):
        logo = "ftp://fleetingforms.io/images/logo.png"
        with self.assertRaises(ValidationError):
            FleetingNamespaceFactory(logo=logo)

    def test_namespace_logo_field_rejects_without_scheme(self):
        logo = "fleetingforms.io/images/logo.png"
        with self.assertRaises(ValidationError):
            FleetingNamespaceFactory(logo=logo)

    def test_namespace_logo_field_accepts_http_url(self):
        logo = "http://fleetingforms.io/images/logo.png"
        ns = FleetingNamespaceFactory(logo=logo)
        ns.save()
        self.assertEqual(ns.logo, logo)

    def test_namespace_logo_field_accepts_url(self):
        logo = "https://fleetingforms.io/images/logo.png"
        ns = FleetingNamespaceFactory(logo=logo)
        ns.save()
        self.assertEqual(ns.logo, logo)

    def test_namespace_style_field_is_type_url(self):
        style_field = [f for f in FleetingNamespace._meta.fields
                       if f.name == 'style']
        style_field = style_field[0]
        self.assertTrue(type(style_field) is URLField)

    def test_namespace_style_field_rejects_ftp_url(self):
        style = "ftp://fleetingforms.io/images/style.css"
        with self.assertRaises(ValidationError):
            FleetingNamespaceFactory(style=style)

    def test_namespace_style_field_rejects_without_scheme(self):
        style = "fleetingforms.io/images/style.css"
        with self.assertRaises(ValidationError):
            FleetingNamespaceFactory(style=style)

    def test_namespace_style_field_accepts_http_url(self):
        style = "http://fleetingforms.io/images/style.css"
        ns = FleetingNamespaceFactory(style=style)
        ns.save()
        self.assertEqual(ns.style, style)

    def test_namespace_style_field_accepts_url(self):
        style = "https://fleetingforms.io/images/style.css"
        ns = FleetingNamespaceFactory(style=style)
        ns.save()
        self.assertEqual(ns.style, style)

    def test_namespace_str(self):
        ns = FleetingNamespaceFactory()
        self.assertIn(ns.subdomain, str(ns))

    def test_namespace_audit_entries_count(self):
        """Namespace form count is taken by looking at audit entries"""
        upto = random.randint(5, 10)
        ns = FleetingNamespaceFactory()
        for i in range(0, upto):
            FleetingFormFactory(namespace=ns)

        self.assertEqual(upto, ns.audit_entries.count())

    def test_cannot_be_deleted(self):
        """Namespaces are the place we group the stuff we bill. They cannot
        be deleted."""
        ns = FleetingNamespaceFactory()
        with self.assertRaises(FleetingDeletionError):
            ns.delete()

    def test_total_forms_since(self):
        upto = random.randint(5, 10)
        ns = FleetingNamespaceFactory()
        since_start = timezone.now() - timedelta(days=31)
        for i in range(0, upto):
            created_on = timezone.now() - timedelta(days=i)
            FleetingFormFactory(namespace=ns, created_on=created_on)

        self.assertEqual(ns.total_forms_since(since_start), upto)

    def test_total_forms_between(self):
        upto = random.randint(5, 10)
        ns = FleetingNamespaceFactory()
        since_start = timezone.now() - timedelta(days=31)
        for i in range(0, upto):
            created_on = timezone.now() - timedelta(days=i)
            FleetingFormFactory(namespace=ns, created_on=created_on)

        self.assertEqual(ns.total_forms_between(
            since_start, timezone.now()), upto)

    def test_total_forms(self):
        upto = random.randint(5, 10)
        ns = FleetingNamespaceFactory()
        for i in range(0, upto):
            created_on = timezone.now() - timedelta(days=i*10)
            FleetingFormFactory(namespace=ns, created_on=created_on)
        self.assertEqual(ns.total_forms, upto)

    def test_forms_this_month(self):
        upto = random.randint(5, 10)
        month_start = timezone.now().replace(day=1)
        ns = FleetingNamespaceFactory()
        for i in range(0, upto):
            created_on = month_start
            FleetingFormFactory(namespace=ns, created_on=created_on)
        self.assertEqual(ns.forms_this_month, upto)

    def test_forms_last_month(self):
        upto = random.randint(5, 10)
        month_start = timezone.now().replace(day=1, hour=0, minute=0, second=0,
                                             microsecond=0)
        last_month_end = (month_start - timedelta(days=1)).replace(
                            hour=23, minute=59, second=59, microsecond=999999)
        last_month_start = last_month_end.replace(
                day=1, hour=0, minute=0, second=0, microsecond=0)
        ns = FleetingNamespaceFactory()
        for i in range(0, upto):
            created_on = last_month_start + timedelta(days=i)
            f = FleetingFormFactory(namespace=ns)
            f.created_on = created_on
            ae = f.audit_entry
            ae.created_on = created_on
            ae.save()
            f.save()
        self.assertEqual(ns.forms_last_month, upto)

    def test_active_forms(self):
        upto = random.randint(5, 10)
        ns = FleetingNamespaceFactory()
        for i in range(0, upto):
            FleetingFormFactory(namespace=ns)
        self.assertEqual(ns.active_forms, upto)

    def test_retention_range(self):
        ns = FleetingNamespaceFactory()
        with self.assertRaises(ValidationError):
            ns.retention = 0
            ns.save()

        with self.assertRaises(ValidationError):
            ns.retention = settings.FLEETING_MAX_RETENTION
            ns.retention += 1
            ns.save()

        ns.retention = settings.FLEETING_MAX_RETENTION
        ns.save()
        self.assertEqual(ns.retention,
                         settings.FLEETING_MAX_RETENTION)

    def test_subdomain_regex_validation(self):
        ns = FleetingNamespaceFactory()
        for char in FleetingNamespace.CH_URL_UNSAFE:
            ns.subdomain = "name{}".format(char)
            with self.assertRaises(ValidationError):
                ns.save()


class FleetingWebhookTestCase(TestCase):

    def test_webhook_str(self):
        wh = FleetingWebhookFactory()
        self.assertIn(wh.event, str(wh))


class FleetingAuditEntryTestCase(TestCase):

    def test_audit_entry_str(self):
        au = FleetingAuditEntryFactory()
        self.assertIn(au.code, str(au))

    def test_audit_entry_created_post_save(self):
        fform = FleetingFormFactory()
        self.assertIsNotNone(fform.audit_entry)
        self.assertEqual(fform.audit_entry.status, fform.status)
        self.assertEqual(fform.audit_entry.namespace,
                         fform.namespace)

    def test_audit_entry_updated_on_open(self):
        fform = FleetingFormFactory(status='created')
        self.assertIsNotNone(fform.audit_entry)
        self.assertEqual(fform.audit_entry.status, fform.status)
        fform.open()
        self.assertEqual(fform.audit_entry.status, 'opened')
        self.assertEqual(fform.audit_entry.opened_on, fform.opened_on)

    def test_audit_entry_updated_on_completed(self):
        fform = FleetingFormFactory(status='opened')
        self.assertIsNotNone(fform.audit_entry)
        self.assertEqual(fform.audit_entry.status, fform.status)
        fform.complete()
        self.assertEqual(fform.audit_entry.status, 'completed')
        self.assertEqual(fform.audit_entry.completed_on, fform.completed_on)

    def test_audit_entry_updated_on_error(self):
        fform = FleetingFormFactory(status='opened')
        self.assertIsNotNone(fform.audit_entry)
        self.assertEqual(fform.audit_entry.status, fform.status)
        fform.error("failed", "failed")
        self.assertEqual(fform.audit_entry.status, 'error')
        self.assertEqual(fform.audit_entry.completed_on, fform.completed_on)

    def test_audit_entry_not_updated_without_status_change(self):
        fform = FleetingFormFactory(status='created')
        self.assertIsNotNone(fform.audit_entry)
        self.assertEqual(fform.audit_entry.status, fform.status)
        fform.created_on = timezone.now()
        fform.save()
        self.assertEqual(fform.audit_entry.status, 'created')
        self.assertEqual(fform.audit_entry.opened_on, None)

    def test_audit_entry_not_deleted_when_form_deleted(self):
        ns = FleetingNamespaceFactory()
        forms = []
        upto = random.randint(5, 10)
        for i in range(0, upto):
            forms.append(FleetingFormFactory(namespace=ns))
        for i in range(0, int(upto/2)):
            forms[i].delete()
        self.assertEqual(FleetingAuditEntry.objects.count(), upto)

    def test_audit_entry_cannot_be_deleted(self):
        fform = FleetingFormFactory(status='created')
        entry = fform.audit_entry
        fform.delete()
        self.assertTrue(FleetingAuditEntry.objects.filter(
                                            pk=entry.pk).exists())
        with self.assertRaises(FleetingDeletionError):
            entry.delete()


class FleetingFormTestCase(TestCase):

    def setUp(self):
        self.fform = FleetingFormFactory()
        self.fform.save()

    @mock.patch('fleetingform.models.get_random_string')
    def test__next_code(self, mock_get_random_string):
        mock_get_random_string.side_effect = ['abcdefg', 'abcdefg', 'hijklmno']
        f1 = FleetingForm()
        f1.save()
        self.assertEqual(f1.code, 'abcdefg')
        f2 = FleetingForm()
        self.assertEqual(f2.code, 'hijklmno')

    def test_error_required_arguments(self):
        with self.assertRaises(TypeError):
            self.fform.error()
        with self.assertRaises(TypeError):
            self.fform.error('error-code')
        with self.assertRaises(TypeError):
            self.fform.error('message')
        self.fform.error('error-code', 'message')

    def test_error_sets_status(self):
        self.assertTrue(
                self.fform.status != FleetingForm.FORM_STATUS_ERROR)
        self.fform.error('error-code', 'message')
        self.assertTrue(
                self.fform.status == FleetingForm.FORM_STATUS_ERROR)

    def test_error_sets_completed_on(self):
        self.fform.completed_on = None
        self.fform.error('error-code', 'message')
        self.assertIsNotNone(self.fform.completed_on)

    def test_error_sets_result(self):
        self.fform.error('error-code', 'message')
        self.assertEqual(self.fform.result['error'],
                         {'code': 'error-code', 'message': 'message'})

    def test_error_force_status(self):
        self.fform.status = FleetingForm.FORM_STATUS_COMPLETED
        self.fform.save()
        with self.assertRaises(FleetingFormCompleteError):
            self.fform.error('error-code', 'message', force_status=False)
        self.fform.error('error-code', 'message', force_status=True)

    def test_complete_sets_status(self):
        self.assertTrue(
                self.fform.status != FleetingForm.FORM_STATUS_COMPLETED)
        self.fform.complete()
        self.assertTrue(
                self.fform.status == FleetingForm.FORM_STATUS_COMPLETED)

    def test_complete_sets_completed_on(self):
        self.fform.completed_on = None
        self.fform.complete()
        self.assertIsNotNone(self.fform.completed_on)

    def test_complete_sets_default_result(self):
        self.fform.complete()
        self.assertEqual(self.fform.result, {})

    def test_complete_sets_result(self):
        result = {'answer': 'yes'}
        self.fform.complete(result)
        self.assertEqual(self.fform.result, result)

    def test_open_sets_status(self):
        self.assertTrue(self.fform.status != FleetingForm.FORM_STATUS_OPENED)
        self.fform.open()
        self.assertTrue(self.fform.status == FleetingForm.FORM_STATUS_OPENED)

    def test_open_sets_opened_on(self):
        self.fform.opened_on = None
        self.fform.open()
        self.assertIsNotNone(self.fform.opened_on)

    def test_open_noops_when_already_opened(self):
        initial_open = timezone.now() - timedelta(days=7)
        initial_status = FleetingForm.FORM_STATUS_OPENED

        self.fform.status = initial_status
        self.fform.opened_on = initial_open
        self.fform.save()

        self.fform.open()
        self.assertEqual(self.fform.opened_on, initial_open)
        self.assertEqual(self.fform.status, initial_status)

    def test_open_noops_when_form_complete(self):
        initial_open = timezone.now() - timedelta(days=7)
        self.fform.opened_on = initial_open
        self.fform.complete()
        initial_status = self.fform.status
        self.fform.open()
        self.assertEqual(self.fform.opened_on, initial_open)
        self.assertEqual(self.fform.status, initial_status)

    def test_open_noops_for_user_noauth(self):
        self.fform.auth = None
        self.fform.opened_on = None
        self.fform.open()
        self.assertIsNotNone(self.fform.opened_on)

    def test_open_calls_open_for_user(self):
        self.fform.auth = FleetingAuthFactory(type='user')
        user = self.fform.auth.users.first()
        self.assertIsNone(user.opened_on)
        self.fform.open(user.username)
        form_time = self.fform.opened_on.isoformat(timespec='seconds')
        user_time = self.fform.auth.users.first().opened_on.isoformat(
                timespec='seconds')
        self.assertEqual(form_time, user_time)

    def test_expires_on(self):
        self.assertEqual(
                self.fform.expires_on,
                self.fform.created_on + timedelta(
                    self.fform.namespace.retention))

    def test_form_expired(self):
        self.fform.created_on = timezone.now()
        self.assertTrue(not self.fform.expired)
        self.fform.created_on = timezone.now() - timedelta(
                days=self.fform.namespace.retention + 1)
        self.assertTrue(self.fform.expired)

    def test_form_str(self):
        self.assertIn(self.fform.code, str(self.fform))

    def test_audit_entry(self):
        self.assertIsNotNone(self.fform.audit_entry)
        self.assertEqual(self.fform.audit_entry.code, self.fform.code)

    def test_child_objects_deleted(self):
        auth = self.fform.auth
        template = self.fform.template
        self.fform.delete()
        with self.assertRaises(ObjectDoesNotExist):
            auth.refresh_from_db()
        with self.assertRaises(ObjectDoesNotExist):
            template.refresh_from_db()

    def test_child_objects_delete_fails_gracefully(self):
        auth = self.fform.auth
        template = self.fform.template
        auth.delete()
        template.delete()
        self.fform.delete()
        with self.assertRaises(ObjectDoesNotExist):
            auth.refresh_from_db()
        with self.assertRaises(ObjectDoesNotExist):
            template.refresh_from_db()


class FleetingTemplateTestCase(TestCase):

    def setUp(self):
        self.fform = FleetingFormFactory()
        self.fform.save()
        self.template = self.fform.template

    def test_helper(self):
        self.assertEqual(self.template.helper,
                         FleetingTemplate.TEMPLATE_HELPERS[self.template.type])

    def test_supported_templates(self):
        self.assertEqual(self.template.supported_templates,
                         [t[0] for t in FleetingTemplate.TEMPLATE_TYPES])

    def test_html_template(self):
        self.assertEqual(self.template.html_template,
                         "fleetingform/{}.html".format(self.template.type))

    def test_helper_class(self):
        helper = self.template.helper_class()()
        self.assertIsInstance(helper,
                              FleetingTemplateHelper)

    def test_str(self):
        self.assertIn(self.template.title, str(self.template))


class FleetingAuthTestCase(TestCase):

    def setUp(self):
        self.fform = FleetingFormFactory()
        self.fform.save()
        self.auth = self.fform.auth
        self.username = 'username'
        self.password = 'password'
        self.password_hash = settings.FLEETING_DEFAULT_HASHER.hash(
                self.password)
        self.user = self.auth.users.first()
        self.user.username = self.username
        self.user.password = self.password_hash
        self.user.save()

    def test_requires_password(self):
        self.fform.auth.type = FleetingAuth.AUTH_TYPE_USER_PASS
        self.assertTrue(self.fform.auth.requires_password)
        self.fform.auth.type = FleetingAuth.AUTH_TYPE_NONE
        self.assertTrue(not self.fform.auth.requires_password)
        self.fform.auth.type = FleetingAuth.AUTH_TYPE_USER
        self.assertTrue(not self.fform.auth.requires_password)

    def test_authenticate_none_is_true(self):
        self.fform.auth.type = FleetingAuth.AUTH_TYPE_NONE
        self.assertTrue(self.fform.auth.authenticate())
        self.assertTrue(self.fform.auth.authenticate(username='user'))
        self.assertTrue(self.fform.auth.authenticate(password='pass'))
        self.assertTrue(self.fform.auth.authenticate(
                            username='user', password='pass'))

    def test_authenticate_not_none_requires_users(self):
        self.fform.auth.type = FleetingAuth.AUTH_TYPE_USER
        self.fform.auth.users.all().delete()
        self.assertTrue(not self.fform.auth.authenticate())
        self.assertTrue(not self.fform.auth.authenticate(username='user'))
        self.assertTrue(not self.fform.auth.authenticate(password='pass'))
        self.assertTrue(not self.fform.auth.authenticate(
                            username='user', password='pass'))

    def test_authenticate_user(self):
        self.fform.auth.type = FleetingAuth.AUTH_TYPE_USER
        self.assertTrue(not self.fform.auth.authenticate(username='fakeuser'))
        self.assertTrue(not self.fform.auth.authenticate(username=''))
        self.assertTrue(not self.fform.auth.authenticate())
        self.assertTrue(not self.fform.auth.authenticate(
                            username='fakeuser', password='pass'))
        self.assertTrue(self.fform.auth.authenticate(
                            username=self.user.username))

    def test_authenticate_user_pass(self):
        self.fform.auth.type = FleetingAuth.AUTH_TYPE_USER_PASS
        self.assertTrue(not self.fform.auth.authenticate(username='fakeuser'))
        self.assertTrue(not self.fform.auth.authenticate(username=''))
        self.assertTrue(not self.fform.auth.authenticate())
        self.assertTrue(not self.fform.auth.authenticate(
                            username='fakeuser', password='pass'))
        self.assertTrue(self.fform.auth.authenticate(
                        username=self.username, password=self.password))

    def test_authenticate_pass(self):
        self.fform.auth.type = FleetingAuth.AUTH_TYPE_PASS
        self.assertTrue(not self.fform.auth.authenticate(username='fakeuser'))
        self.assertTrue(not self.fform.auth.authenticate(username=''))
        self.assertTrue(not self.fform.auth.authenticate())
        self.assertTrue(not self.fform.auth.authenticate(
                            username='fakeuser', password='pass'))
        self.assertTrue(self.fform.auth.authenticate(password=self.password))

    def test_bad_hash_format_raises(self):
        with self.assertRaises(ValueError):
            self.auth.verify_password('fake', 'garbage')

    def test_opened_by_noops_on_repeat(self):
        self.assertIsNone(self.user.opened_on)
        self.auth.opened_by(self.user.username)
        self.user.refresh_from_db()
        self.assertIsNotNone(self.user.opened_on)
        opened_on = self.user.opened_on
        self.auth.opened_by(self.user.username)
        self.assertEqual(opened_on, self.user.opened_on)

    def test_auth_str(self):
        self.assertIn(self.auth.get_type_display(), str(self.auth))


class FleetingUserTestCase(TestCase):

    def setUp(self):
        self.fform = FleetingFormFactory()
        self.fform.save()
        self.auth = self.fform.auth
        self.username = 'username'
        self.password = 'password'
        self.email = "username@domain.com"
        self.phone = "+16047796890"
        self.password_hash = settings.FLEETING_DEFAULT_HASHER.hash(
                self.password)
        self.user = self.auth.users.first()
        self.user.username = self.username
        self.user.password = self.password_hash
        self.user.email = self.email
        self.user.phone = self.phone
        self.user.save()

    def test_obscure_email(self):
        self.auth.type = FleetingAuth.AUTH_TYPE_PASS_OTP_EMAIL
        self.assertEqual('userxxxx@xxxmain.com',
                         self.user.otp_contact_obscured)

    def test_obscure_phone(self):
        self.auth.type = FleetingAuth.AUTH_TYPE_PASS_OTP_PHONE
        self.assertEqual('+160xxxx6890', self.user.otp_contact_obscured)

    def test_obscure_non_otp_type(self):
        self.auth.type = FleetingAuth.AUTH_TYPE_NONE
        self.assertEqual('', self.user.otp_contact_obscured)

    @mock.patch('fleetingform.models.send_otp_email')
    def test_generate_and_send_otp_calls_send_email(self, mock_send_otp_email):
        self.auth.type = FleetingAuth.AUTH_TYPE_PASS_OTP_EMAIL
        attempts = self.user.attempts
        self.user.generate_and_send_otp()
        self.assertEqual(attempts + 1, self.user.attempts)
        mock_send_otp_email.assert_called_once()

    @mock.patch('fleetingform.models.send_otp_sms')
    def test_generate_and_send_otp_calls_send_sms(self, mock_send_otp_sms):
        self.auth.type = FleetingAuth.AUTH_TYPE_PASS_OTP_PHONE
        attempts = self.user.attempts
        self.user.generate_and_send_otp()
        self.assertEqual(attempts + 1, self.user.attempts)
        mock_send_otp_sms.assert_called_once()

    def test_generate_and_send_otp_raises_on_attempts(self):
        self.user.attempts = settings.FLEETING_OTP_MAX_ATTEMPTS + 1
        with self.assertRaises(FleetingOTPRetriesExceeded):
            self.user.generate_and_send_otp()

    def test_generate_and_send_otp_raises_on_non_otp_type(self):
        self.auth.type = FleetingAuth.AUTH_TYPE_NONE
        with self.assertRaises(FleetingAuthOTPError):
            self.user.generate_and_send_otp()

        self.user.attempts = settings.FLEETING_OTP_MAX_ATTEMPTS + 1
        with self.assertRaises(FleetingOTPRetriesExceeded):
            self.user.generate_and_send_otp()

    def test_authenticate(self):
        self.auth.type = FleetingAuth.AUTH_TYPE_USER_PASS
        self.assertTrue(self.user.authenticate(self.password))
        self.assertTrue(not self.user.authenticate(''))
        self.assertTrue(not self.user.authenticate('badpass'))


class FleetingActionTestCase(TestCase):

    def setUp(self):
        self.fform = FleetingFormFactory()
        self.fform.save()
        self.action = self.fform.template.actions.first()

    def test_str(self):
        self.assertIn(self.action.label, str(self.action))


class FleetingChoiceTestCase(TestCase):

    def setUp(self):
        self.fform = FleetingFormFactory()
        self.fform.save()
        fc = self.fform.template.form_controls.create(
                    type='choice', name='choices')
        self.choice = FleetingChoiceFactory(form_control=fc)

    def test_str(self):
        self.assertIn(self.choice.text, str(self.choice))


class FleetingValidationTestCase(TestCase):

    def setUp(self):
        self.fform = FleetingFormFactory()
        self.fform.save()
        fc = self.fform.template.form_controls.create(
                    type='text', name='name')
        self.validation = FleetingValidationFactory(form_control=fc)

    def test_str(self):
        self.assertIn(self.validation.type, str(self.validation))
