import logging

from django import forms
from django.contrib.auth.forms import UsernameField

logger = logging.getLogger(__name__)

class FleetingAuthenticationForm(forms.Form):
    """Base authentication form that sets the field labels and help text.

    This abstract base class looks after the customization of the stock
    authentication form (auto-generated) with the configuration in a
    Fleeting Form's authentication configuration.

    :param fform: FleetingForm the authentication page is for.
    :type fform: FleetingForm
    """
    def __init__(self, fform, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fform = fform
        if 'password' in self.fields:
            password_field = fform.auth.form_controls.filter(name='password').first()
            if password_field:
                self.fields['password'].label = password_field.label
                self.fields['password'].help_text = password_field.help_text
        if 'username' in self.fields:
            username_field = fform.auth.form_controls.filter(name='username').first()
            if username_field:
                self.fields['username'].label = username_field.label
                self.fields['username'].help_text = username_field.help_text

class UserAuthenticationForm(FleetingAuthenticationForm):
    """Authenticate based on username only.

    :param fform: FleetingForm the authentication page is for.
    :type fform: FleetingForm
    """
    username = UsernameField(widget=forms.TextInput(attrs={'autofocus': True}))

    def __init__(self, fform, *args, **kwargs):
        super().__init__(fform, *args, **kwargs)

    def clean(self):
        username = self.cleaned_data.get('username')
        if not self.fform.auth.verify_username(username=username):
            raise forms.ValidationError(
                    "The user {} cannot complete this form.".format(
                        username),
                    code='invalid_username',
                    params={'username': username})
        return self.cleaned_data

class PasswordAuthenticationForm(FleetingAuthenticationForm):
    """Authenticate based on password only.

    This form may be used either for static password or otp auth
    types.

    :param fform: FleetingForm the authentication page is for.
    :type fform: FleetingForm
    """
    password = forms.CharField(
        strip=False,
        widget=forms.PasswordInput(attrs={'autocomplete': 'current-password'}),
    )

    def __init__(self, fform, username='', *args, **kwargs):
        logger.debug("New password auth for form {}".format(username))
        super().__init__(fform, *args, **kwargs)
        self.username = username

    def clean(self):
        password = self.cleaned_data.get('password')
        if not self.fform.auth.authenticate(username=self.username,
                                            password=password):
            raise forms.ValidationError(
                    "This form requires a valid password.",
                    code='invalid_login',
                    params={'password': 'invalid', })

        return self.cleaned_data

class UserPasswordAuthenticationForm(FleetingAuthenticationForm):
    """Authenticate based on username and password.

    This form may be used either for static username and password or
    user and otp auth types.

    :param fform: FleetingForm the authentication page is for.
    :type fform: FleetingForm
    """

    username = UsernameField(widget=forms.TextInput(attrs={'autofocus': True}))
    password = forms.CharField(
        strip=False,
        widget=forms.PasswordInput(attrs={'autocomplete': 'current-password'}),
    )

    def __init__(self, fform, *args, **kwargs):
        super().__init__(fform, *args, **kwargs)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if not self.fform.auth.authenticate(username, password):
            raise forms.ValidationError(
                    "This form requires a valid username and password.",
                    code='invalid_login',
                    params={'username': username, })

        return self.cleaned_data
