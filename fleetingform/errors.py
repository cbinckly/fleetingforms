class FleetingDeletionError(Exception):
    """Raised when you try to delete an object that cannot be deleted.

    :param klass: object class
    :type klass: str
    :param pk: object primary key
    :type pk: object
    """

    def __init__(self, klass, pk, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.klass = klass
        self.pk = pk

    def __str__(self):
        return "Object {}({}) cannot be deleted.".format(self.klass, self.pk)

class FleetingOTPRetriesExceeded(Exception):
    """Raised when the max number of OTP resends is exceeded.

    :param user: the user for which the retries were exceeded.
    :type user: :class:`fleetingform.models.FleetingUser`
    """

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user

    def __str__(self):
        return "Maximum OTP retries exceeded for {} on {}.".format(
                    self.user, self.user.auth.form.code)

class FleetingFormGenerationError(Exception):
    """Raised when for generation fails."""
    pass

class FleetingAuthOTPError(Exception):
    """Raised when creating, saving, or sending a one time passcode fails.

    :param user: User the OTP was being generated for.
    :type user: :class:`fleetingform.models.FleetingUser`
    :param auth_type: Authentication type of the form.
    :type auth_type: str from FleetingAuth.AUTH_TYPES
    """

    def __init__(self, user, auth_type, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        self.auth_type = auth_type

    def __str__(self):
        return "Failed to generate or send one time code to {}.".format(
                    self.user)

class FleetingFormCompleteError(Exception):
    """Raised when an attempt is made to save a form that is already complete.

    :param code: The form's one time code.
    :type code: str
    """

    def __init__(self, code, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.code = code

    def __str__(self):
        return "Form {} is already complete.".format(self.code)


class FleetingValidationError(Exception):
    """Raised while validating requests.

    Using a message map in place of handwritten Exception messages provides a
    more consistent user experience when creating forms.

    :param field: the field with the value that failed validation.
    :type field: str
    :param code: the code of the form.
    :type code: str
    """
    code_to_message = {
            "object-over-length": ("object has too many keys, maximum keys "
                                   "{max_length}."),
            "field-over-length": ("name is too long, maximum length "
                                  "{max_length}."),
            "value-over-length": ("value is too long, maximum length "
                                  "{max_length}."),
            "list-under-length": ("list must contain at least {min_length} "
                                  "items."),
            "list-over-length": ("list is too long, maximum length "
                                 "{max_length}."),
            "mutually-exclusive": "is mutually exclusive with {_with}.",
            "required-fields": "one or more of {fields} are missing.",
            "incorrect-type": "must be of type {type}.",
            "invalid-field": "{fields} not supported here.",
            "value-format": "must be formatted as {format}.",
            "empty-value": "cannot be empty.",
            "value-range": "must be between {min} and {max}.",
            }

    def __init__(self, field, code, *args, **kwargs):
        # super().__init__(*args, **kwargs)
        self.field = field
        self.code = code
        self.kwargs = kwargs

    def __str__(self):
        if self.code in self.code_to_message:
            return self.code_to_message[self.code].format(field=self.field,
                                                          **self.kwargs)
        return " ".join([self.field, self.code])

