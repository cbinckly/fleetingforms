import logging
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist, ValidationError

from rest_framework import authentication
from rest_framework import exceptions

from fleetingform.models import FleetingNamespace

logger = logging.getLogger(__name__)

class FleetingAuthentication(authentication.BaseAuthentication):
    """ReST Framework Authentication handler for Namespace Token."""

    def authenticate(self, request):
        """Authenticate a request, returns the user and namespace."""
        token = request.META.get(settings.FLEETING_TOKEN_HEADER)
        if not token:
            return None

        try:
            namespace = FleetingNamespace.objects.get(token=token)
            user = namespace.user
        except ObjectDoesNotExist:
            logger.warn(f"namespace or user do not exist for token {token}")
            raise exceptions.AuthenticationFailed('Authentication failed.')
        except ValidationError:
            logger.warn(f"token malformed ({token})")
            raise exceptions.AuthenticationFailed('Authentication failed.')

        return (user, token)
