import logging
import smtplib
from passlib.hash import (bcrypt_sha256,
                          pbkdf2_sha256,
                          pbkdf2_sha512,
                          argon2, )

from django.conf import settings
from django.core.mail import send_mail

from twilio.rest import Client as TwilioClient
from twilio.base.exceptions import TwilioRestException

logger = logging.getLogger(__name__)

def send_otp_sms(phone, otp, fform):
    """Send an SMS one time passcode.

    :param phone: phone number to send to
    :type phone: str (E.164 formatting)
    :param otp: the one time passcode to send
    :type otp: str
    :returns: True if sent, else False
    :rtype: bool
    """

    client = TwilioClient(settings.TWILIO_ACCOUNT_SID,
                          settings.TWILIO_ACCOUNT_TOKEN)
    body = "Your verification code for {} is {}.".format(
                fform.template.title[:80], otp)
    logger.debug(f"{fform.code}: sending sms otp to {phone}.")
    try:
        message = client.messages.create(body=body,
                                         from_=settings.TWILIO_FROM_PHONE,
                                         to=phone)
    except TwilioRestException as e:
        logger.error(f"{fform.code}: twilio send message failed {e}")
        return False

    if message.error_code:
        logger.error(f"{fform.code}: twilio send message failed "
                      "{message.error_code}")
        return False

    return True

def send_otp_email(email, otp, fform):
    """Send an email one time passcode.

    :param email: email to send to
    :type email: str
    :param otp: the one time passcode to send
    :type otp: str
    :returns: True if sent, else False
    :rtype: bool
    """
    subject = "Verification code for '{}'".format(
                fform.template.title[:80])
    body = "Your verification code for {} is {}.".format(
                fform.template.title[:80], otp)
    logger.debug(f"{fform.code}: sending email otp to {email}.")
    try:
        send_mail(subject, body, settings.EMAIL_FROM, [email, ], )
    except smtplib.SMTPException as e:
        logger.debug(f"{fform.code}: email send failed - {e}")
        raise

    return True

def auth_token_field(fform):
    """Session field housing the auth_token for a fform."""
    return 'fform_auth_token_{}'.format(fform.code)

def auth_username_field(fform):
    """Session field housing the verified username for a fform."""
    return 'fform_auth_user_{}'.format(fform.code)

