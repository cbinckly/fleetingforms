from rest_framework import serializers


class EmptyFormControlParamsValidator():

    allowed_fields = tuple()

    def __init__(self, params):
        self.params = params

    def validate(self):
        if self.params:
            raise serializers.ValidationError({
                'params': "this form control type doesn't accept params."
                })
        return self.params


class DecimalFormControlParamsValidator():

    allowed_fields = ('max_digits', 'decimal_places')

    def __init__(self, params):
        self.params = params

    def validate(self):
        extras = set(self.params.keys()) - set(self.allowed_fields)
        if extras:
            raise serializers.ValidationError({
                'params': "unsupported keys for decimal field: {}".format(
                    ", ".join(extras))
                })

        for field in self.allowed_fields:
            val = self.params.get(field)
            if val:
                if not isinstance(val, int):
                    raise serializers.ValidationError({
                            'params': "{} must be an integer.".format(field)
                        })
                if val < 0 or val > 20:
                    raise serializers.ValidationError({
                            'params': "{} must be between 0 and 20.".format(
                                field)
                        })

        max_digits = self.params.get('max_digits', 21)
        decimal_places = self.params.get('decimal_places', 5)

        if decimal_places > max_digits:
            raise serializers.ValidationError({
                    'params': "decimal places must be less than or equal to "
                              "max digits."
                })

        return self.params
