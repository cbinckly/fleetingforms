import logging

from rest_framework import permissions
from rest_framework.exceptions import Throttled

logger = logging.getLogger(__name__)


class FleetingFormTokenPermission(permissions.BasePermission):
    """Require the presence of the correct namespace token."""

    message = "A valid token and namespace are required."

    def has_permission(self, request, view):
        has_permission = hasattr(request, 'namespace') and request.namespace
        if not has_permission:
            logger.debug(f"has_permission False for {request}")
        return has_permission

    def has_object_permission(self, request, view, obj):
        if self.has_permission(request, view) and \
                request.namespace == obj.namespace:
            return True
        logger.warn("object permission rejected for {} {} != {}".format(
            obj,
            request.namespace if hasattr(request, 'namespace') else 'none',
            obj.namespace))
        return False


class FleetingNamespaceTokenPermission(permissions.BasePermission):
    """Require the presence of the correct namespace token or user token."""

    message = "A valid token is required."

    def has_permission(self, request, view):
        """Users can create new namespaces, namespace tokens cannot.
        Users and namespace tokens can both list."""
        has_permission = False
        if request.user and request.user.is_authenticated:
            has_permission = True
        else:
            if request.method == 'GET':
                has_permission = (hasattr(request, 'namespace')
                                  and request.namespace)

        if not has_permission:
            logger.debug(f"has_permission False for {request}")

        return has_permission

    def has_object_permission(self, request, view, obj):
        if hasattr(request, 'namespace') and request.namespace:
            if request.namespace == obj:  # pragma: no branch - qs
                return True
        elif request.user and request.user.is_authenticated:
            if obj in request.user.namespaces.all():  # pragma: no branch - qs
                return True
        else:  # pragma: no cover - covered by queryset filtering
            logger.warn("object permission rejected for {} {} != {}".format(
                obj,
                request.namespace if hasattr(request, 'namespace') else 'none',
                obj.namespace))
            return False


class FleetingFormHardLimitPermission(permissions.BasePermission):
    """Do not allow creates for users over their hard limit."""

    message = "Namespace hard limit exceeded."

    def has_permission(self, request, view):
        if request.method == 'POST':
            namespace = request.namespace
            if namespace.forms_this_month >= namespace.hard_limit:
                raise Throttled(detail=self.message)
        return True
