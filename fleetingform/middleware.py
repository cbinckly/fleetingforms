import logging

from fleetingform.models import FleetingNamespace

logger = logging.getLogger(__name__)

class FleetingNamespaceMiddleware():
    """Manage the FleetingNamespace in the request.

    Implements the Django Middleware pattern.  It should be in the
    middleware stack somewhere after the AuthenticationMiddleware.

    Adds the ``.namespace`` property to the request object.
    Set to None if no token in header.
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.namespace = FleetingNamespace.from_request(request)
        logger.debug(f"namespace middleware identified {request.namespace}")
        return self.get_response(request)
