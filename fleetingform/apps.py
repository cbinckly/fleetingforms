from django.apps import AppConfig


class FleetingFormConfig(AppConfig):    # pragma: no cover - trivial built-in
    name = 'fleetingform'
    verbose_name = 'fleetingforms'

    def ready(self):
        import fleetingform.signals
