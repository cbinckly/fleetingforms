import logging
from random import randint

from django.conf import settings
from django.utils.crypto import get_random_string
from rest_framework.serializers import ValidationError

from fleetingform.models import (FleetingAuth, )
from fleetingform.errors import FleetingValidationError

logger = logging.getLogger(__name__)

class FleetingTemplateHelper():
    """Abstract base class for all template helpers."""

    type = None
    """The type must match a FleetingTemplate.TEMPLATE_TYPE."""

    def __init__(self):
        if not self.type:
            raise NotImplementedError("Child classes must define a type.")

    def validate_params(self, params):
        """Validate the template specific parameters.

        :param params: the parameters to validate.
        :type params: dict
        :returns: validated parameters
        :rtype: dict
        :raises: rest_framework.serializers.ValidationError
        """
        raise NotImplementedError("Child classes must implement "
                                  "param validation.")
class FleetingAuthValidator():
    """Validate that the authentication definition is coherent.

    The serializer checks the validity of each value.  This validator
    checks that the options specified are all valid together,
    and that any required values for the auth type are included.

    :params auth_type: the authentication type for the form.
    :type auth_type: str from FleetingAuth.AUTH_TYPES
    """

    requires_email = (FleetingAuth.AUTH_TYPE_PASS_OTP_EMAIL,
                      FleetingAuth.AUTH_TYPE_USER_PASS_OTP_EMAIL, )
    requires_phone = (FleetingAuth.AUTH_TYPE_PASS_OTP_PHONE,
                      FleetingAuth.AUTH_TYPE_USER_PASS_OTP_PHONE, )
    requires_user =  (FleetingAuth.AUTH_TYPE_USER,
                      FleetingAuth.AUTH_TYPE_USER_PASS,
                      FleetingAuth.AUTH_TYPE_USER_PASS_OTP_EMAIL,
                      FleetingAuth.AUTH_TYPE_USER_PASS_OTP_PHONE, )
    requires_pass =  (FleetingAuth.AUTH_TYPE_PASS,
                      FleetingAuth.AUTH_TYPE_USER_PASS, )
    requires_users = (FleetingAuth.AUTH_TYPE_USER,
                      FleetingAuth.AUTH_TYPE_PASS,
                      FleetingAuth.AUTH_TYPE_USER_PASS,
                      FleetingAuth.AUTH_TYPE_PASS_OTP_EMAIL,
                      FleetingAuth.AUTH_TYPE_USER_PASS_OTP_EMAIL,
                      FleetingAuth.AUTH_TYPE_PASS_OTP_PHONE,
                      FleetingAuth.AUTH_TYPE_USER_PASS_OTP_PHONE, )
    requires_no_users = (FleetingAuth.AUTH_TYPE_NONE, )
    requires_one_user = (FleetingAuth.AUTH_TYPE_PASS,
                         FleetingAuth.AUTH_TYPE_PASS_OTP_EMAIL,
                         FleetingAuth.AUTH_TYPE_PASS_OTP_PHONE, )
    requires_no_pass = (FleetingAuth.AUTH_TYPE_PASS_OTP_EMAIL,
                        FleetingAuth.AUTH_TYPE_PASS_OTP_PHONE,
                        FleetingAuth.AUTH_TYPE_USER_PASS_OTP_EMAIL,
                        FleetingAuth.AUTH_TYPE_USER_PASS_OTP_PHONE, )

    def __init__(self, auth_type):
        self.auth_type = auth_type

    def _verify_all_users_have_field(self, users, field):
        """Verify that a field is present for all users.

        :param users: users for this form
        :type users: list(dict()) - [{"username": ...}, ]
        :param field: the field that must be present
        :type field: str
        :returns: validated users
        :rtype: list
        :raises: rest_framework.serializers.ValidationError
        """
        for user in users:
            if not user.get(field):
                raise ValidationError(
                        { field: ("Auth type {} requires {} "
                                  "for all users.").format(
                                      self.auth_type, field)})
        return users

    def _verify_max_user_count(self, users, count):
        """Verify that no more than ``count`` users are defined.

        :param users: users for this form
        :type users: list(dict()) - [{"username": ...}, ]
        :param count: the maximim number of users
        :type count: int
        :returns: validated users
        :rtype: list
        :raises: rest_framework.serializers.ValidationError
        """
        if len(users) > count:
            raise ValidationError(
                    { 'users' : ("Auth type {} supports "
                                 "{} or fewer users.").format(
                                     self.auth_type, count)})
        return users

    def _verify_all_users_have_unique_field(self, users, field):
        """Verify that all users have a unique value for a field.

        :param users: users for this form
        :type users: list(dict()) - [{"username": ...}, ]
        :param field: the field that must contain unique values
        :type field: str
        :returns: validated users
        :rtype: list
        :raises: rest_framework.serializers.ValidationError
        """
        seen = []
        for user in users:
            value = user.get(field)
            if not value:
                raise ValidationError(
                        "Auth type {} requires {} "
                        "for all users.".format(self.auth_type, field))
            if value in seen:
                raise ValidationError(
                        { field: "Field {} must be unique for "
                                 "all users.".format(field)})
            seen.append(value)
        return users

    def _verify_field_not_set(self, users, field):
        """Verify that a field is not set for any user.

        :param users: users for this form
        :type users: list(dict()) - [{"username": ...}, ]
        :param field: the field that must not be set.
        :type field: str
        :returns: validated users
        :rtype: list
        :raises: rest_framework.serializers.ValidationError
        """
        for user in users:
            value = user.get(field)
            if value:
                raise ValidationError(
                        { field: "{} cannot be set for auth type {}.".format(
                            field, self.auth_type) })
        return users

    def validate(self, data):
        """Perform a full validation of all users' authentication options.

        :param data: the individually validated data.
        :type data: dict
        :returns: validated users
        :rtype: list
        :raises: rest_framework.serializers.ValidationError
        """
        users = data.get('users')
        if data['type'] in self.requires_no_users:
            if users:
                raise ValidationError(
                        { 'users' : ("Auth type {} doesn't accept "
                                     "any users.").format(
                                            data['type'])})
        if data['type'] in self.requires_users:
            if not users:
                raise ValidationError(
                        { 'users': "Auth type {} requires at least "
                                   "one user.".format(data['type'])})
        if data['type'] in self.requires_one_user:
            users = self._verify_max_user_count(data['users'], 1)
        if data['type'] in self.requires_user:
            users = self._verify_all_users_have_unique_field(
                    data['users'], 'username')
        if data['type'] in self.requires_pass:
            users = self._verify_all_users_have_field(
                    data['users'], 'password')
        if data['type'] in self.requires_email:
            users = self._verify_all_users_have_field(
                    data['users'], 'email')
        if data['type'] in self.requires_phone:
            users = self._verify_all_users_have_field(
                    data['users'], 'phone')
        if data['type'] in self.requires_no_pass:
            users = self._verify_field_not_set(
                    data['users'], 'password')

        data['users'] = users

        return data

class FleetingAppParamsValidator():
    """Validate app params.

    App params have a few restrictions:
        - One level of nesting only
        - No more than 32 keys total
        - No lists longer than 32 elements
    """
    max_keys = 16
    max_levels = 1
    max_key_length = 32
    max_value_length = 512
    max_list_length = 32

    def validate(self, app={}):
        """Validate that the application params meet requirements.

        :param app: the application params to validate.
        :type app: dict
        :returns: validated application params
        :rtype: dict
        :raises: fleetingform.errors.FleetingValidationError
        :returns: True
        """
        self._keys_and_depth(app)
        return True

    def _keys_and_depth(self, obj, level=0):
        """Check key count and nesting depth for an dict.

        :param obj: the dict to check
        :type obj: dict
        :param level: the maximum dict nesting depth
        :type level: 0
        :returns: None
        :raises: fleetingform.errors.FleetingValidationError
        """
        count = 0
        for key, value in obj.items():
            count += 1
            if len(key) > self.max_key_length:
                raise FleetingValidationError(
                        "app",
                        "field-over-length",
                        max_length=self.max_key_length)
            if isinstance(value, dict):
                level += 1
                if level > self.max_levels:
                    raise FleetingValidationError(
                            "app",
                            "nesting-depth",
                            max_depth=self.max_levels)
                count += self._keys_and_depth(value, level)
            elif isinstance(value, list):
                if len(value) > self.max_list_length:
                    raise FleetingValidationError(
                            "app",
                            "list-over-length",
                            max_length=self.max_list_length)
            else:
                try:
                    if len(value) > self.max_value_length:
                        raise FleetingValidationError(
                            "app",
                            "value-over-length",
                            max_length=self.max_value_length)
                except TypeError:
                    pass

        if count > self.max_keys:
            raise FleetingValidationError(
                    "app",
                    "object-over-length",
                    max_length=self.max_keys)
        return count
