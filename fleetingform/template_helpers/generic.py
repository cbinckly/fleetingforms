from rest_framework.serializers import ValidationError

from fleetingform.template_helpers import FleetingTemplateHelper

class GenericTemplateHelper(FleetingTemplateHelper):
    """The generic template helper."""

    type = 'generic'

    def validate_params(self, params):
        """Validate the template specific parameters.

        There shouldn't be any!

        :param params: the parameters to validate.
        :type params: dict
        :returns: validated parameters
        :rtype: dict
        :raises: rest_framework.serializers.ValidationError
        """
        if params and len(params):
            raise ValidationError(
                    {'params': "{} templates don't accept params.".format(
                        self.type)})
        return {}
