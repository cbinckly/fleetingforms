import factory
import factory.fuzzy
from faker import Factory

from django.utils import timezone
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from fleetingform.models import (FleetingNamespace,
                                 FleetingWebhook,
                                 FleetingForm,
                                 FleetingTemplate,
                                 FleetingFormControl,
                                 FleetingAction,
                                 FleetingChoice,
                                 FleetingValidation,
                                 FleetingAuth,
                                 FleetingUser,
                                 FleetingAuditEntry,
                                 )
faker = Factory.create()


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Sequence(lambda n: "user_%d" % n)
    email = faker.email()


class FleetingNamespaceFactory(factory.DjangoModelFactory):
    class Meta:
        model = FleetingNamespace

    user = factory.SubFactory(UserFactory)
    subdomain = factory.Sequence(
            lambda n: "{}_{}".format(faker.domain_word(), n))
    url_shortener = factory.fuzzy.FuzzyChoice(FleetingNamespace.URL_SHORTENERS,
                                              getter=lambda s: s[0])
    support_email = faker.email()
    logo = faker.url(schemes=['https', 'http'])
    hard_limit = 100
    soft_limit = 100

class FleetingWebhookFactory(factory.DjangoModelFactory):
    class Meta:
        model = FleetingWebhook

    namespace = factory.SubFactory(FleetingNamespaceFactory)
    name = faker.domain_word()
    event = factory.fuzzy.FuzzyChoice(FleetingWebhook.WEBHOOK_EVENTS,
                                      getter=lambda s: s[0])
    url = faker.url(schemes=['https', ])
    token = faker.uuid4()

class FleetingAuditEntryFactory(factory.DjangoModelFactory):
    class Meta:
        model = FleetingAuditEntry

    code = factory.LazyFunction(lambda: faker.pystr(min_chars=8, max_chars=8))
    namespace = factory.SubFactory(FleetingNamespaceFactory)
    auth = FleetingAuth.AUTH_TYPE_NONE
    template = FleetingTemplate.TEMPLATE_TYPE_GENERIC
    status = FleetingForm.FORM_STATUS_CREATED
    created_on = faker.date_time(tzinfo=timezone.get_current_timezone())
    opened_on = None
    completed_on = None

class FleetingActionFactory(factory.DjangoModelFactory):
    class Meta:
        model = FleetingAction

    label = faker.word()
    template = factory.SubFactory(
                    'fleetingform.factories.FleetingTemplateFactory')

class FleetingChoiceFactory(factory.DjangoModelFactory):
    class Meta:
        model = FleetingChoice

    value = factory.Sequence(lambda n: n)
    text = factory.LazyFunction(lambda: faker.word())
    form_control = factory.SubFactory(
                    'fleetingform.factories.FleetingTemplateFormControlFactory')

class FleetingValidationFactory(factory.DjangoModelFactory):
    class Meta:
        model = FleetingValidation

    type = factory.fuzzy.FuzzyChoice(FleetingValidation.VALIDATION_TYPES,
                                       getter=lambda s: s[0])
    params = {}
    message = faker.sentence()
    form_control = factory.SubFactory(
                    'fleetingform.factories.FleetingFormControlFactory')

class FleetingFormControlFactory(factory.django.DjangoModelFactory):
    object_id = factory.SelfAttribute('form.id')
    content_type = factory.LazyAttribute(
            lambda o: ContentType.objects.get_for_model(o.form))
    name = factory.Sequence(lambda n: "{} {}".format(faker.word(), n))
    label = faker.word()
    type = factory.fuzzy.FuzzyChoice(FleetingFormControl.FIELD_TYPES,
                                       getter=lambda s: s[0])
    help_text = faker.sentence(nb_words=4)
    initial = ''
    disabled = False
    required = faker.pybool()
    choices = factory.RelatedFactory(FleetingChoiceFactory,
                                     factory_related_name='form_control')
    validations = factory.RelatedFactory(FleetingValidationFactory,
                                         factory_related_name='form_control')
    class Meta:
        exclude = ['form']
        abstract = True

class FleetingTemplateFormControlFactory(FleetingFormControlFactory):
    class Meta:
        model = FleetingFormControl

    form = factory.SubFactory('fleetingform.factories.FleetingTemplateFactory')

class FleetingAuthFormControlFactory(FleetingFormControlFactory):
    class Meta:
        model = FleetingFormControl

    form = factory.SubFactory('fleetingform.factories.FleetingAuthFactory')

class FleetingTemplateFactory(factory.DjangoModelFactory):
    class Meta:
        model = FleetingTemplate

    '''
    type = factory.fuzzy.FuzzyChoice(FleetingTemplate.TEMPLATE_TYPES,
                                     getter=lambda s: s[0])
    '''
    type = FleetingTemplate.TEMPLATE_TYPE_GENERIC
    title = faker.sentence(nb_words=4)
    content = faker.paragraph()
    form_controls = factory.RelatedFactory(
                        FleetingTemplateFormControlFactory,
                        factory_related_name='form',)
    actions = factory.RelatedFactory(
                        FleetingActionFactory,
                        factory_related_name='template',)
    content_type = 'txt'
    params = {}
    form = factory.SubFactory('fleetingform.factories.FleetingFormFactory')

class FleetingUserFactory(factory.DjangoModelFactory):
    class Meta:
        model = FleetingUser

    username = factory.Sequence(lambda n: "user_%d" % n)
    email = faker.email()
    phone = faker.phone_number()
    password = settings.FLEETING_DEFAULT_HASHER.hash("password")
    attempts = 0
    auth = factory.SubFactory('fleetingform.factories.FleetingAuthFactory')

class FleetingAuthFactory(factory.DjangoModelFactory):
    class Meta:
        model = FleetingAuth

    type = factory.fuzzy.FuzzyChoice(FleetingAuth.AUTH_TYPES,
                                       getter=lambda s: s[0])
    title = faker.sentence(nb_words=4)
    content = faker.paragraph()
    users = factory.RelatedFactory(FleetingUserFactory,
                                   factory_related_name='auth')
    form_controls = factory.RelatedFactory(FleetingAuthFormControlFactory,
                                           factory_related_name='form')
    action = factory.LazyFunction(lambda: faker.word())
    form = factory.SubFactory('fleetingform.factories.FleetingFormFactory')

class FleetingFormFactory(factory.DjangoModelFactory):
    class Meta:
        model = FleetingForm
    code = factory.LazyFunction(lambda: faker.pystr(min_chars=8, max_chars=8))
    namespace = factory.SubFactory(FleetingNamespaceFactory)
    app = {}
    status = FleetingForm.FORM_STATUS_CREATED
    result = {}
    created_on = faker.date_time(tzinfo=timezone.get_current_timezone())
    opened_on = None
    completed_on = None
    auth_token = faker.uuid4()
    template = factory.RelatedFactory(FleetingTemplateFactory,
                                      factory_related_name='form')
    auth = factory.RelatedFactory(FleetingAuthFactory,
                                      factory_related_name='form')
