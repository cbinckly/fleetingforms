# Generated by Django 3.0.5 on 2020-05-25 21:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fleetingform', '0009_url_shortener_to_char'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fleetingnamespace',
            name='subdomain',
            field=models.CharField(max_length=64, unique=True),
        ),
    ]
