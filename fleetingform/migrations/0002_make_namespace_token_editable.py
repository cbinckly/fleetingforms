# Generated by Django 3.0.5 on 2020-05-13 02:22

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('fleetingform', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fleetingnamespace',
            name='token',
            field=models.UUIDField(default=uuid.uuid4),
        ),
    ]
