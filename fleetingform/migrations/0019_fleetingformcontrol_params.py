# Generated by Django 3.0.5 on 2020-06-13 20:54

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fleetingform', '0018_form_control_type_decimal'),
    ]

    operations = [
        migrations.AddField(
            model_name='fleetingformcontrol',
            name='params',
            field=django.contrib.postgres.fields.jsonb.JSONField(default=dict),
        ),
    ]
