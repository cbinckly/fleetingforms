# Generated by Django 3.0.5 on 2020-05-15 11:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fleetingform', '0002_make_namespace_token_editable'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='fleetingaction',
            options={'ordering': ('id',)},
        ),
        migrations.AlterModelOptions(
            name='fleetingformcontrol',
            options={'ordering': ('id',)},
        ),
        migrations.AlterUniqueTogether(
            name='fleetingaction',
            unique_together={('template', 'label')},
        ),
    ]
