from django.contrib import admin
from fleetingform.models import (FleetingNamespace,
                                 FleetingTemplate,
                                 FleetingForm,
                                 FleetingAuth)

admin.site.site_header = "Fleeting Forms Admin"
admin.site.site_title = "Fleeting Forms Admin Portal"
admin.site.index_title = "Welcome to Fleeting Forms"


@admin.register(FleetingNamespace)
class FleetingNamespaceAdmin(admin.ModelAdmin):
    """Use a custom Admin class to force display of non-editable fields."""
    fields = ('subdomain', 'user', 'token', 'retention', 'support_email',
              'url_shortener', 'style', 'logo', 'soft_limit', 'hard_limit', )
    list_display = ('subdomain', 'retention', 'soft_limit',
                    'forms_this_month', 'forms_last_month',
                    'total_forms', 'active_forms', )

    def get_queryset(self, request, *args, **kwargs):
        if not (request.user and request.user.is_authenticated):
            return FleetingNamespace.objects.none()

        if request.user.is_superuser:
            return FleetingNamespace.objects.all()

        return request.user.namespaces.all()

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def has_delete_permission(self, request, obj=None):
        return False

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        super().save_model(request, obj, form, change)

    def get_changeform_initial_data(self, request):
        return {'user': request.user}

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = []
        if not (request.user and request.user.is_superuser):
            readonly_fields += ['user', 'token', ]
        return readonly_fields


@admin.register(FleetingForm)
class FleetingFormAdmin(admin.ModelAdmin):
    list_display = ('code', 'namespace', 'created_on', '__str__', )

    def get_queryset(self, request, *args, **kwargs):
        if not (request.user and request.user.is_authenticated):
            return FleetingForm.objects.none()

        if request.user.is_superuser:
            return FleetingForm.objects.all()

        return FleetingForm.objects.filter(
                namespace__in=request.user.namespaces.all())

    def has_add_permission(self, request, obj=None):
        return False


@admin.register(FleetingTemplate)
class FleetingTemplateAdmin(admin.ModelAdmin):

    def get_queryset(self, request, *args, **kwargs):
        if not (request.user and request.user.is_authenticated):
            return FleetingTemplate.objects.none()

        if request.user.is_superuser:
            return FleetingTemplate.objects.all()

        return FleetingTemplate.objects.filter(
                form__namespace__in=request.user.namespaces.all())

    def has_add_permission(self, request, obj=None):
        return False


@admin.register(FleetingAuth)
class FleetingAuthAdmin(admin.ModelAdmin):

    def get_queryset(self, request, *args, **kwargs):
        if not (request.user and request.user.is_authenticated):
            return FleetingAuth.objects.none()

        if request.user.is_superuser:
            return FleetingAuth.objects.all()

        return FleetingAuth.objects.filter(
                form__namespace__in=request.user.namespaces.all())

    def has_add_permission(self, request, obj=None):
        return False
