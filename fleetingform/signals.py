import logging

from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

from fleetingform.models import FleetingForm, FleetingAuditEntry

logger = logging.getLogger(__name__)


@receiver(post_save, sender=FleetingForm)
def create_fleeting_audit_entry(sender, instance, created, **kwargs):
    if created and instance.namespace:
        FleetingAuditEntry.objects.create(
                code=instance.code,
                namespace=instance.namespace,
                template=instance.template.type if instance.template else '',
                auth=instance.auth.type if instance.auth else '',
                created_on=instance.created_on,
                status=instance.status,
                )
    else:
        if instance.audit_entry and \
                instance.status != instance.audit_entry.status:
            audit_entry = instance.audit_entry
            if instance.status == FleetingForm.FORM_STATUS_OPENED:
                audit_entry.opened_on = instance.opened_on
            else:
                audit_entry.completed_on = instance.completed_on
            audit_entry.status = instance.status
            audit_entry.save()


@receiver(post_delete, sender=FleetingForm)
def delete_template_and_auth(sender, instance, **kwargs):
    try:
        instance.auth.delete()
        instance.template.delete()
    except Exception as e:
        logger.error("Failed to delete {} auth/template: {}".format(
            instance, e))


@receiver(post_save, sender=User)
def create_authtoken(sender, instance, created, **kwargs):
    if created:
        Token.objects.create(user=instance)
