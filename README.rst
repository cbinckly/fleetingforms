Fleeting Forms
==============

.. image:: https://readthedocs.org/projects/fleetingforms/badge/?version=latest
    :target: https://fleetingforms.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

.. image:: https://circleci.com/bb/cbinckly/fleetingforms/tree/master.svg?style=svg
    :target: https://circleci.com/bb/cbinckly/fleetingforms/tree/master

.. image:: https://img.shields.io/docker/cloud/build/cbinckly/fleetingforms   
    :alt: Docker Cloud Build Status

Quick, secure, validated feedback.
----------------------------------

Fleeting Forms is a web service that makes it easy and quick to securely
collect feedback from users.

Before writing that email parser you've been dreading, try a Fleeting Form.
